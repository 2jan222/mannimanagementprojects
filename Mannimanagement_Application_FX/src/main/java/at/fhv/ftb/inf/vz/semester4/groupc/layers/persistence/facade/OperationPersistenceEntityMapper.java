package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.OperationEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class OperationPersistenceEntityMapper extends PersistenceEntityMapper<OperationEntity> {
    @Override
    public OperationEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(OperationEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("OperationEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<OperationEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<OperationEntity> query = session.createQuery("from OperationEntity ", OperationEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<OperationEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<OperationEntity> query = session.createQuery("from OperationEntity " + whereQueryFragment, OperationEntity.class);
            return query.getResultList();
        }
    }
}
