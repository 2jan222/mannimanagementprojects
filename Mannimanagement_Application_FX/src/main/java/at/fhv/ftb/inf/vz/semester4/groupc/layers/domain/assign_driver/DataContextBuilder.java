package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver.exceptions.ValidationException;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver.exceptions.ValidationType;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Driver;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.OperationFlat;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.ShiftEntry;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.StationConnector;
import javafx.beans.property.SimpleBooleanProperty;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;

public class DataContextBuilder {
    private LocalDate date;
    private HashMap<Integer, OperationFlat> operationById = new HashMap<>();
    private HashMap<Integer, LinkedList<ShiftEntry>> shiftsByOperationId = new HashMap<>();
    private HashMap<Integer, LinkedList<ShiftEntry>> shiftsByDriverId = new HashMap<>();
    private HashMap<Integer, ShiftEntry> shiftEntryById = new HashMap<>();
    private HashMap<Integer, Driver> driverByDriverId = new HashMap<>();
    private HashMap<StationIDPair, Integer> stationConnectors = new HashMap<>();
    private SimpleBooleanProperty isSaved;

    @Contract(pure = true)
    public DataContextBuilder(LocalDate date) {
        this.date = date;
        isSaved = new SimpleBooleanProperty(false);
    }

    public DataContextBuilder addOperation(OperationFlat operation) throws ValidationException {
        RawDataValidator.validateOperation(operation);
        operationById.putIfAbsent(operation.getOperationId(), operation);
        shiftsByOperationId.putIfAbsent(operation.getOperationId(), new LinkedList<>());
        return this;
    }

    public DataContextBuilder addDriverOnDuty(Driver driver) throws ValidationException {
        RawDataValidator.validateDriver(driver, date);
        shiftsByDriverId.putIfAbsent(driver.getDriverId(), new LinkedList<>());
        driverByDriverId.putIfAbsent(driver.getDriverId(), driver);
        return this;
    }


    public DataContext buildContext() {
        return new DataContext(date, operationById, shiftsByOperationId, shiftsByDriverId, shiftEntryById, driverByDriverId, isSaved, stationConnectors);
    }

    public DataContextBuilder addShift(@NotNull ShiftEntry shiftEntry) throws ValidationException {
        RawDataValidator.validateShift(shiftEntry);
        shiftEntryById.put(shiftEntry.getShiftEntryId(), shiftEntry);
        shiftsByOperationId.computeIfPresent(shiftEntry.getOperationId(), (key, value) -> {
            value.add(shiftEntry);
            return value;
        });
        shiftsByDriverId.computeIfPresent(shiftEntry.getDriverId(), (key, value) -> {
            value.add(shiftEntry);
            return value;
        });
        return this;
    }

    @SuppressWarnings("RedundantThrows")
    public static class RawDataValidator {
        static void hasOperationBus(@NotNull OperationFlat operation) throws ValidationException {
            LinkedList<ValidationType> errors = new LinkedList<>();
            if (operation.getBusId() == null) {
                errors.add(ValidationType.BUS_ID_NULL);
            }
            if (!errors.isEmpty()) {
                throw new ValidationException(ValidationType.OPERATION_HAS_NO_BUS,
                        new ValidationException(errors.toArray(new ValidationType[0])));
            }
        }

        @Contract("null -> fail")
        public static void hasOperationId(@Nullable OperationFlat operation) throws ValidationException {
            if (operation == null) {
                throw new ValidationException(ValidationType.OPERATION_ID_NULL,
                        new ValidationException(ValidationType.OPERATION_NULL));
            } else {
                if (operation.getOperationId() == null) {
                    throw new ValidationException(ValidationType.OPERATION_ID_NULL);
                }
            }
        }

        public static void validateOperation(@Nullable OperationFlat operation) throws ValidationException {
            hasOperationId(operation);
            hasOperationBus(operation);
        }

        public static void validateDriver(Driver driver, LocalDate date) throws ValidationException {
            //To be implemented.
        }

        public static void validateShift(ShiftEntry shiftEntry) throws ValidationException {
            //To be implemented.
        }
    }

    public DataContextBuilder addStationConnector(StationConnector statCon) {
        //System.out.println("statCon = " + statCon);
        Integer startId = null;
        if(statCon.getStart() != null) {
            startId = statCon.getStart().getStationId();
        }
        stationConnectors.putIfAbsent(new StationIDPair(startId, statCon.getEnd().getStationId()), statCon.getTimeFromPrevious());
        return this;
    }
}
