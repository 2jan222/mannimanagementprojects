package at.fhv.ftb.inf.vz.semester4.groupd.persistance;

import at.fhv.ftb.inf.vz.semester4.groupd.domain.Operation;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.security.IRoute;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public interface CreateOperationFacade {

	List<? extends IRoute> getValidRoutesByMonth(int month);

	List<? extends IRoute> getValidRoutesByDay(LocalDate date);

	List<Operation> getOperationsByDate(Date valueOf);

	List<Operation> getOperationsByMonth(int month);

	void saveOperations(List<Operation> newOperations);

	void deleteOperation(Operation oper);

	void deleteOperations(List<Operation> deleteOperation);

	void saveOperation(Operation oper);

}
