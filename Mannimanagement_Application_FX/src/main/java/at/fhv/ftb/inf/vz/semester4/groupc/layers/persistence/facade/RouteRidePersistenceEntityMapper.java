package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteRideEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class RouteRidePersistenceEntityMapper extends PersistenceEntityMapper<RouteRideEntity> {
    @Override
    public RouteRideEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(RouteRideEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("RouteRideEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<RouteRideEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<RouteRideEntity> query = session.createQuery("from RouteRideEntity ", RouteRideEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<RouteRideEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<RouteRideEntity> query = session.createQuery("from RouteRideEntity " + whereQueryFragment, RouteRideEntity.class);
            return query.getResultList();
        }
    }
}