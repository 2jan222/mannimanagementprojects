package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos;

/**
 * BusDTO.
 */
public class BusDTO {
    private Integer busId;
    private String licenceNumber;
    private String make;
    private String model;
    private Integer seatPlaces;
    private Integer standPlaces;
    private String note;


    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    public String getLicenceNumber() {
        return licenceNumber;
    }

    public void setLicenceNumber(String licenceNumber) {
        this.licenceNumber = licenceNumber;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getSeatPlaces() {
        return seatPlaces;
    }

    public void setSeatPlaces(Integer seatPlaces) {
        this.seatPlaces = seatPlaces;
    }

    public Integer getStandPlaces() {
        return standPlaces;
    }

    public void setStandPlaces(Integer standPlaces) {
        this.standPlaces = standPlaces;
    }

    public BusDTO(String licenceNumber, String make, String model, String note, Integer seatPlaces, Integer standPlaces) {
        this.licenceNumber = licenceNumber;
        this.make = make;
        this.model = model;
        this.seatPlaces = seatPlaces;
        this.standPlaces = standPlaces;
    }

    public BusDTO() {
    }

    public Integer getPlaces() {
        return getSeatPlaces() + getStandPlaces();
    }

    @Override
    public String toString() {
        return "BusDTO{"
                + "\n\tbusId=" + busId
                + ",\n\t licenceNumber='" + licenceNumber + '\''
                + ",\n\t make='" + make + '\''
                + ",\n\t domain='" + model + '\''
                + ",\n\t seatPlaces=" + seatPlaces
                + ",\n\t standPlaces=" + standPlaces
                + "\n}";
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
