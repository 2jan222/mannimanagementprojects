package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.StationConnectorEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = StationEntityMapper.class)
public interface StationConnectorEntityMapper {

    StationConnectorEntityMapper INSTANCE = Mappers.getMapper(StationConnectorEntityMapper.class);

    @Mapping(target = "pathStationId", source = "pathStationEntity.pathStationId")
    @Mapping(target = "start", source = "startStation")
    @Mapping(target = "timeFromPrevious", source = "duration")
    @Mapping(target = "end", source = "endStation")
    @Mapping(target = "distanceFromPrevious", source = "distance")
    @Mapping(target = "connectionId", source = "stationConnectorId")
    StationConnector toMojo(StationConnectorEntity stationConnectorEntity);

    @Mapping(target = "stationConnectorId", source = "connectionId")
    @Mapping(target = "startStationId", source = "start.stationId")
    @Mapping(target = "startStation", source = "start")
    @Mapping(target = "pathStationEntity", ignore = true)
    @Mapping(target = "endStationId", source = "end.stationId")
    @Mapping(target = "duration", source = "timeFromPrevious")
    @Mapping(target = "endStation", source = "end")
    @Mapping(target = "distance", source = "distanceFromPrevious")
    StationConnectorEntity toPojo(StationConnector stationConnector);

    @Mapping(target = "pathStationId", source = "pathStationEntity.pathStationId")
    @Mapping(target = "start", source = "startStation")
    @Mapping(target = "timeFromPrevious", source = "duration")
    @Mapping(target = "end", source = "endStation")
    @Mapping(target = "distanceFromPrevious", source = "distance")
    @Mapping(target = "connectionId", source = "stationConnectorId")
    void updateMojo(StationConnectorEntity stationConnectorEntity, @MappingTarget StationConnector stationConnector);

    @Mapping(target = "stationConnectorId", source = "connectionId")
    @Mapping(target = "startStationId", source = "start.stationId")
    @Mapping(target = "startStation", source = "start")
    @Mapping(target = "pathStationEntity", ignore = true)
    @Mapping(target = "endStationId", source = "end.stationId")
    @Mapping(target = "duration", source = "timeFromPrevious")
    @Mapping(target = "endStation", source = "end")
    @Mapping(target = "distance", source = "distanceFromPrevious")
    void updatePojo(StationConnector stationConnector, @MappingTarget StationConnectorEntity stationConnectorEntity);

    List<StationConnector> toMojos(List<StationConnectorEntity> stationConnectorEntities);

    List<StationConnectorEntity> toPojos(List<StationConnector> stationConnectors);
}
