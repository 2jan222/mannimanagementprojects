package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos;


import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatOperationEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = {FlatBusEntityMapper.class, ShiftRideEntityMapper.class})
public interface FlatOperationEntityMapper {
    FlatOperationEntityMapper INSTANCE = Mappers.getMapper(FlatOperationEntityMapper.class);

    @Mapping(target = "bus", source = "flatBusEntity")
    OperationFlat toMojo(FlatOperationEntity flatOperationEntity);

    @Mapping(target = "flatBusEntity", source = "bus")
    FlatOperationEntity toPojo(OperationFlat flatOperation);

    @Mapping(target = "bus", source = "flatBusEntity")
    void updateMojo(FlatOperationEntity flatOperationEntity, @MappingTarget OperationFlat flatOperation);

    @Mapping(target = "flatBusEntity", source = "bus")
    void updatePojo(OperationFlat flatOperation, @MappingTarget FlatOperationEntity flatOperationEntity);

    List<OperationFlat> toMojos(List<FlatOperationEntity> flatOperationEntities);

    List<FlatOperationEntity> toPojos(List<OperationFlat> flatOperations);
}
