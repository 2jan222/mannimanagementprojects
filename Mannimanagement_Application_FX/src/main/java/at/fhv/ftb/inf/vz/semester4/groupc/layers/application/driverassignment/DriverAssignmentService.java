package at.fhv.ftb.inf.vz.semester4.groupc.layers.application.driverassignment;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver.exceptions.AssignDriverContextCreationException;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver.AssignDriverDataTransfer;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver.exceptions.AssignDriverDataTransferUpdateException;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver.DataSupplier;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Driver;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.ShiftEntry;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.ShiftRide;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.BusDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.DriverDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.ShiftEntryDTO;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;

@LoggerableClassDisplayName("[DriverAssignmentService]")
public class DriverAssignmentService {
    private Loggerable logger = Loggerable.getInstance();
    private AssignDriverFacade connection;

    public DriverAssignmentService() {
        this(new DataSupplier());
    }

    @SuppressWarnings("WeakerAccess")
    public DriverAssignmentService(AssignDriverDataTransfer dataTransfer) {
        AssignDriverFacade facade = new AssignDriverFacadeImpl();
        this.connection = facade;
        facade.init(dataTransfer);
    }

    public LinkedList<ShiftEntryDTO> getShiftEntriesByOperationId(Integer operationId, LocalDate date) throws AssignDriverContextCreationException {
        return connection.getShiftsByOperationId(operationId, date);
    }

    public LinkedList<Integer> getOperationsByIDOfDay(LocalDate date) {
        return connection.getOperationsByIDOfDay(date);
    }


    public LinkedList<DriverDTO> getFreeDriverFormId(ShiftEntryDTO dto) throws AssignDriverContextCreationException {
        return connection.getFreeDriverFormShift(dto);
    }

    public boolean assignDriverToShift(ShiftEntryDTO shiftDTO, DriverDTO driverDTO) throws AssignDriverContextCreationException, AssignDriverDataTransferUpdateException {
        return connection.assignDriverToShift(shiftDTO, driverDTO);
    }


    public boolean removeDriverFromShift(ShiftEntryDTO shiftDTO) throws AssignDriverContextCreationException, AssignDriverDataTransferUpdateException {
        return connection.removeDriverFromShift(shiftDTO);
    }

    public String getDriverInformationFromDriverId(Integer driverId, LocalDate date) throws AssignDriverContextCreationException {
        logger.debug("GET DRIVER INFORMATION");
        Driver driverById = connection.getDriverById(driverId, date);
        return (driverById != null) ? driverById.getDisplayString() : null;
    }

    public String getBusLicenseByOperationId(Integer operationId, LocalDate date) {
        BusDTO busDTO = null;
        try {
            busDTO = connection.getBusByOperationId(operationId, date);
        } catch (AssignDriverContextCreationException e) {
            e.printStackTrace();
        }
        return (busDTO != null) ? busDTO.getLicenceNumber() : "BUS COULD NOT BE READ";
    }


    public LinkedList<LocalDate> getDatesWithOperationsWhichHaveBus() {
        return connection.getDatesWithOperationsWhichHaveBus();
    }

    public LinkedList<ShiftRide> getRidesByOperationId(Integer i, LocalDate date) throws AssignDriverContextCreationException {
        return connection.getRidesByOperationId(i, date);
    }
/*
    public LinkedList<DriverDTO> getAllDrivers() {
        return connection.getAllDrivers();
    }


    public OperationDTO getOperationFromId(Integer operationId, LocalDate date) {
        return connection.getOperationFromId(operationId, date);
    }

*/
    public ShiftEntryDTO setShift(Integer operationId, LinkedList<ShiftRide> rides, LocalDate date) throws AssignDriverContextCreationException {
        return connection.setShift(operationId, rides, date);
    }

    public ShiftEntry setShift(Integer operationId, LocalTime start, LocalTime end, LocalDate date) throws AssignDriverContextCreationException, AssignDriverDataTransferUpdateException {
        return connection.setShiftForTimeFrame(operationId, start, end, date);
    }
/*

    public boolean addRideToShift(ShiftEntryDTO shiftDTO, RouteRideDTO toAdd) {
        return connection.addRideToShift(shiftDTO, toAdd);
    }


    public boolean removeRideFromShift(ShiftEntryDTO shiftDTO, RouteRideDTO toRemove) {
        return connection.removeRideFromShift(shiftDTO, toRemove);
    }


    public boolean removeAllRidesAndDriverFromShift(ShiftEntryDTO shiftDTO) {
        return connection.removeAllRidesAndDriverFromShift(shiftDTO);
    }



    public boolean removeShift(ShiftEntryDTO shiftDTO) {
        return connection.removeShift(shiftDTO);
    }


    public boolean isSaved(LocalDate date) {
        return connection.isSaved(date);
    }


    public boolean save(LocalDate date) {
        return connection.save(date);
    }


    public void clearContextForAllDates() {
        connection.clearContextForAllDates();
    }
    */
}
