package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;
import org.jetbrains.annotations.Contract;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Objects;
import java.util.Set;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Table(name = "path", schema = "public", catalog = "kmobil")
public class PathEntity implements DatabaseEntityMarker {
    private Integer pathId;
    private String pathDescription;
    private Integer routeId;
    private boolean isRetour;
    private RouteEntity routeByRouteId;
    private Set<PathStationEntity> pathStationsByPathId;
    private Set<StartTimeEntity> startTimesByPathId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "path_id")
    public Integer getPathId() {
        return pathId;
    }

    public void setPathId(Integer pathId) {
        this.pathId = pathId;
    }

    @Basic
    @Column(name = "path_description")
    public String getPathDescription() {
        return pathDescription;
    }

    public void setPathDescription(String pathDescription) {
        this.pathDescription = pathDescription;
    }

    @Basic
    @Column(name = "route_id")
    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    @ManyToOne
    @JoinColumn(name = "route_id", referencedColumnName = "route_id", nullable = false, insertable = false, updatable = false)
    public RouteEntity getRouteByRouteId() {
        return routeByRouteId;
    }

    public void setRouteByRouteId(RouteEntity routeByRouteId) {
        this.routeByRouteId = routeByRouteId;
    }

    @OneToMany(mappedBy = "pathByPathId", fetch = FetchType.EAGER)
    public Set<PathStationEntity> getPathStationsByPathId() {
        return pathStationsByPathId;
    }

    public void setPathStationsByPathId(Set<PathStationEntity> pathStationsByPathId) {
        this.pathStationsByPathId = pathStationsByPathId;
    }

    @OneToMany(mappedBy = "pathByPathId", fetch = FetchType.EAGER)
    public Set<StartTimeEntity> getStartTimesByPathId() {
        return startTimesByPathId;
    }

    public void setStartTimesByPathId(Set<StartTimeEntity> startTimesByPathId) {
        this.startTimesByPathId = startTimesByPathId;
    }

    @Override
    public String toString() {
        return "PathEntity{" +
                "pathId=" + pathId +
                ", pathDescription='" + pathDescription + '\'' +
                ", routeId=" + routeId +
                ", routeByRouteId=" + routeByRouteId +
                ", pathStationsByPathIdAmount=" + ((pathStationsByPathId != null) ? pathStationsByPathId.size():null) +
                ", startTimesByPathIdAmount=" + ((startTimesByPathId != null) ? startTimesByPathId.size():null) +
                '}';
    }

    @Basic
    @Column(name = "isretour")
    public boolean getRetour() {
        return isRetour;
    }

    public void setRetour(boolean retour) {
        isRetour = retour;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PathEntity that = (PathEntity) o;
        return isRetour == that.isRetour &&
                Objects.equals(pathId, that.pathId) &&
                Objects.equals(pathDescription, that.pathDescription) &&
                Objects.equals(routeId, that.routeId) &&
                Objects.equals(pathStationsByPathId, that.pathStationsByPathId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pathId, pathDescription, routeId, isRetour, pathStationsByPathId);
    }
}
