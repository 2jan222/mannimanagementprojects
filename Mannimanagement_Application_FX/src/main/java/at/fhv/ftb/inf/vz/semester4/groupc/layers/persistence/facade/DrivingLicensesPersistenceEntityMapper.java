package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.DrivingLicensesEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class DrivingLicensesPersistenceEntityMapper extends PersistenceEntityMapper<DrivingLicensesEntity> {
    @Override
    public DrivingLicensesEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(DrivingLicensesEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("DrivingLicensesEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<DrivingLicensesEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<DrivingLicensesEntity> query = session.createQuery("from DrivingLicensesEntity ", DrivingLicensesEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<DrivingLicensesEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<DrivingLicensesEntity> query = session.createQuery("from DrivingLicensesEntity " + whereQueryFragment, DrivingLicensesEntity.class);
            return query.getResultList();
        }
    }

}
