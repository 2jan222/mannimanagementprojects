package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.AbsenceEntity;
import net.bytebuddy.asm.Advice;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class AbsencePersistenceEntityMapper extends PersistenceEntityMapper<AbsenceEntity> {
    @Override
    public AbsenceEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(AbsenceEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("AbsenceEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<AbsenceEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<AbsenceEntity> query = session.createQuery("from AbsenceEntity ", AbsenceEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<AbsenceEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<AbsenceEntity> query = session.createQuery("from AbsenceEntity " + whereQueryFragment, AbsenceEntity.class);
            return query.getResultList();
        }
    }
}
