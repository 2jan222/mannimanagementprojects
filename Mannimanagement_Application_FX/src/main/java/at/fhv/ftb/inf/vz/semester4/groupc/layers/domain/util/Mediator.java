package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.util;

import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

/**
 * Mediator.
 *
 * @param <N> Name type of mediator storage key
 * @param <T> value type of mediator storage value
 */
public class Mediator<N, T> {
    private final HashMap<N, Storage<N, T>> storageMap = new HashMap<>();

    private final CopyOnWriteArrayList<Consumer<N>> observers = new CopyOnWriteArrayList<>();

    public void setValue(N storageName, T value) {
        Storage<N, T> storage = storageMap.computeIfAbsent(storageName, name -> new Storage<>());
        storage.setValue(this, storageName, value);
    }

    public T getValue(N storageName) {
        Storage<N, T> ntStorage = storageMap.get(storageName);
        return (ntStorage != null) ? ntStorage.getValue() : null;
    }

    public void addObserver(N storageName, Runnable observer) {
        observers.add(eventName -> {
            if (eventName.equals(storageName)) {
                observer.run();
            }
        });
    }

    protected void notifyObservers(N eventName) {
        observers.forEach(observer -> observer.accept(eventName));
    }

    public void clear() {
        storageMap.clear();
    }
}
