package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatStationConnectorEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class FlatStationConnectorPersistenceEntityMapper extends PersistenceEntityMapper<FlatStationConnectorEntity> {
    @Override
    public FlatStationConnectorEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(FlatStationConnectorEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("FlatStationConnectorEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<FlatStationConnectorEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<FlatStationConnectorEntity> query = session.createQuery("from FlatStationConnectorEntity ", FlatStationConnectorEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<FlatStationConnectorEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<FlatStationConnectorEntity> query = session.createQuery("from FlatStationConnectorEntity " + whereQueryFragment, FlatStationConnectorEntity.class);
            return query.getResultList();
        }
    }
}
