package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Suspended;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.SuspendedDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = BusDTOMapper.class)
public interface SuspendedDTOMapper {

    SuspendedDTOMapper INSTANCE = Mappers.getMapper(SuspendedDTOMapper.class);

    SuspendedDTO toDTO(Suspended suspended);

    Suspended toMojo(SuspendedDTO suspendedDTO);

    void updateDTO(Suspended suspended, @MappingTarget SuspendedDTO suspendedDTO);

    void updateMojo(SuspendedDTO suspendedDTO, @MappingTarget Suspended suspended);

    List<SuspendedDTO> toDTOs(List<Suspended> suspendeds);

    List<Suspended> toMojos(List<SuspendedDTO> suspendedDTOS);
}
