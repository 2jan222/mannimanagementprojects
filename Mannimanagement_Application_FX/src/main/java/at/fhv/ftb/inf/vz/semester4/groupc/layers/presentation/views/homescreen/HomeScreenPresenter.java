package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.homescreen;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.menu.MenuViewItem;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.util.CommonResourceGetter;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;

import java.net.URL;
import java.util.ResourceBundle;

public class HomeScreenPresenter implements MenuViewItem, Initializable {

    @FXML
    private ImageView iv;

    @Override
    public String getMenuDisplayName() {
        return "Home";
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        iv.setImage(CommonResourceGetter.getBusIcon());
    }
}
