package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.StationEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface StationEntityMapper {
    StationEntityMapper INSTANCE = Mappers.getMapper(StationEntityMapper.class);

    Station toMojo(StationEntity stationEntity);


    @Mapping(target = "startStationConnectorsByStationId", ignore = true)
    @Mapping(target = "endStationConnectorsByStationId", ignore = true)
    StationEntity toPojos(Station station);

    void updateMojo(StationEntity stationEntity, @MappingTarget Station station);


    @Mapping(target = "startStationConnectorsByStationId", ignore = true)
    @Mapping(target = "endStationConnectorsByStationId", ignore = true)
    void updatePojo(Station station, @MappingTarget StationEntity stationEntity);

    List<Station> toMojos(List<StationEntity> stationEntities);

    List<StationEntity> toPojos(List<Station> stations);
}
