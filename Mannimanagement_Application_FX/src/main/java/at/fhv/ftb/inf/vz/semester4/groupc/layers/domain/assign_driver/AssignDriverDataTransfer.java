package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver.exceptions.AssignDriverDataTransferUpdateException;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Driver;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.OperationFlat;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.ShiftEntry;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.StationConnector;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDate;
import java.util.LinkedList;

public interface AssignDriverDataTransfer {

    @NotNull
    LinkedList<Driver> getDriverForDay(@NotNull LocalDate date);

    @NotNull
    LinkedList<OperationFlat> getOperationsForDay(@NotNull LocalDate date);

    @NotNull
    LinkedList<ShiftEntry> getShiftEntriesForDay(@NotNull LocalDate date);

    @NotNull
    LinkedList<LocalDate> getDatesWithOperationWhichHaveBus();

    LinkedList<StationConnector> getStationConnectors();

    void updateDriver(@Nullable Driver driver) throws AssignDriverDataTransferUpdateException;

    void updateOrCreateShiftEntry(@Nullable ShiftEntry shiftEntry) throws AssignDriverDataTransferUpdateException;


}
