package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.menu;


import at.fhv.ftb.inf.vz.semester4.groupc.layers.application.login.LoginService;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.menu.sidebar.SidebarPresenter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.menu.sidebar.SidebarView;
import com.airhacks.afterburner.views.FXMLView;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import com.jfoenix.controls.JFXDrawer;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.net.URL;
import java.util.LinkedList;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * MenuIntegratedPresenter
 * Handles behavior and interaction of the integrated menu.
 */
@LoggerableClassDisplayName("[MenuIntegratedPresenter]")
public class MenuIntegratedPresenter implements Initializable {
    @Inject
    private Loggerable logger;
    @Inject
    private LoginService loginService;
    @FXML
    private Text currentContentViewDisplay;
    @FXML
    private AnchorPane windowPane;
    @FXML
    private AnchorPane menuBar;
    @FXML
    private JFXDrawer sidebarDrawer;
    @FXML
    private AnchorPane contentAnchor;
    private double xOffset = 0;
    private double yOffset = 0;
    private LinkedList<FXMLView> views = new LinkedList<>();
    private Stage stage;

    /**
     * Shows/hides sidebar menu.
     */
    @FXML
    public void hamburgerAction() {
        if (sidebarDrawer.isOpened() || sidebarDrawer.isOpening()) {
            sidebarDrawer.close();
        } else {
            sidebarDrawer.open();
        }
    }

    /**
     * Opens an about alert.
     */
    @FXML
    private void aboutAction() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About");
        alert.setHeaderText("Information");
        alert.setContentText("User manual pages will be displayed here");
        DialogPane dialogPane = alert.getDialogPane();
        Button ok = (Button) dialogPane.lookupButton(ButtonType.OK);
        ok.setId("helpOkButton");
        alert.showAndWait();
    }

    /**
     * Opens an alert to inform about window closing.
     */
    @FXML
    private void closeAction() {
        ButtonType buttonTypeYes = new ButtonType("Yes");
        ButtonType buttonTypeNo = new ButtonType("No");
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Dialog");
        alert.setHeaderText("Close Application ?");
        alert.setContentText(null);
        alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);
        DialogPane dialogPane = alert.getDialogPane();
        Button yesButton = (Button) dialogPane.lookupButton(buttonTypeYes);
        yesButton.setId("closeYesButton");
        Button noButton = (Button) dialogPane.lookupButton(buttonTypeNo);
        noButton.setId("closeNoButton");


        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == buttonTypeYes) {
            Platform.exit();
        }
    }

    /**
     * Changes currently displayed view.
     *
     * @param view to display.
     */
    public void changeContentView(@NotNull FXMLView view) {
        contentAnchor.getChildren().clear();
        MenuViewItem presenter = (MenuViewItem) view.getPresenter();
        Parent view1 = view.getView();
        contentAnchor.getChildren().add(view1);
        presenter.setStage(stage);
        logger.debug("Changed View");
    }

    /**
     * Updates label, which holds name of currently displayed content view.
     *
     * @param text to display.
     */
    public void updateText(String text, boolean updateWindowTitle) {
        currentContentViewDisplay.setText(text);
        if (updateWindowTitle) {
            stage.setTitle("Mannimanagement - " + text);
        }
    }

    /**
     * List of views the menu knows of.
     * List may vary according to the access right of an user.
     * Their presenters should implement MenuViewItem.
     *
     * @return List of {@code FXMLView}s.
     */
    public LinkedList<FXMLView> getViews() {
        return views;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("Init Menu");
        views = loginService.getViewsForUser();
        logger.debug("Initialize presenter");
        setupDrawer();
        setupUndecoratedWindowListeners();
        setupWindowKeyListeners();
    }

    /**
     * Sets up all KeyListeners on main menu level.
     */
    private void setupWindowKeyListeners() {
        //Key "ESC" trigger sidebar menu to open
        windowPane.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ESCAPE) {
                hamburgerAction();
            }
        });
    }

    /**
     * Sets up Mouse Listeners to move the main window, when undecorated.
     */
    private void setupUndecoratedWindowListeners() {
        /*Listeners for movement of undecorated window*/
        menuBar.setOnMousePressed(event -> {
            xOffset = menuBar.getScene().getWindow().getX() - event.getScreenX();
            yOffset = menuBar.getScene().getWindow().getY() - event.getScreenY();
        });
        menuBar.setOnMouseDragged(event -> {
            menuBar.getScene().getWindow().setX(event.getScreenX() + xOffset);
            menuBar.getScene().getWindow().setY(event.getScreenY() + yOffset);
        });
    }

    /**
     * Sets up the sidebar drawer's listeners, ui-placement and presenter connection to this presenter.
     */
    private void setupDrawer() {
        SidebarView sidebarView = new SidebarView();
        SidebarPresenter sidebarPresenter = (SidebarPresenter) sidebarView.getPresenter();
        sidebarPresenter.setMainController(this);
        sidebarPresenter.init(getViews());
        VBox vbox = (VBox) sidebarView.getView();

        sidebarDrawer.setSidePane(vbox);
        sidebarDrawer.setVisible(true);
        sidebarDrawer.open();
        sidebarDrawer.setResizeContent(true);
        sidebarDrawer.setOverLayVisible(false);
        sidebarDrawer.setResizableOnDrag(true);

        //Move invisible node plane outside of rendered window
        //to enable recognition of MouseEvents on underlying nodes
        sidebarDrawer.setOnDrawerOpening(event -> {
            AnchorPane.setRightAnchor(sidebarDrawer, 0.0);
            AnchorPane.setLeftAnchor(sidebarDrawer, 0.0);
            AnchorPane.setTopAnchor(sidebarDrawer, 0.0);
            AnchorPane.setBottomAnchor(sidebarDrawer, 0.0);
        });

        sidebarDrawer.setOnDrawerClosed(event -> {
            AnchorPane.clearConstraints(sidebarDrawer);
            AnchorPane.setLeftAnchor(sidebarDrawer, -255.0);
            AnchorPane.setTopAnchor(sidebarDrawer, 0.0);
            AnchorPane.setBottomAnchor(sidebarDrawer, 0.0);
        });
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
