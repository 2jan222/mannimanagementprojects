package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.path;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.FlatStationConnectorEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Path;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Route;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.StationConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.PersistenceFacade;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatStationConnectorEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.PathDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.mapper.PathDTOMapper;
import com.github.jan222ik.Loggerable;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.List;

public class PathCreationContext {
    private static final PathDTOMapper pathDTOMapper = PathDTOMapper.INSTANCE;
    private static final PathCreationDataTransfer dataTransfer = new PathCreationDataTransferImpl();
    private static final Loggerable logger = Loggerable.getInstance();

    private Route route;
    private Path outwardPath;
    private PathDTO outwardPathDTO;
    private Path returnPath;
    private PathDTO returnPathDTO;

    static {

        long startMilli = System.currentTimeMillis();
        logger.debug("Load Graph");

        List<StationConnector> stationConnectors =
                FlatStationConnectorEntityMapper.INSTANCE.toMojos(
                        PersistenceFacade.getInstance().getAll(FlatStationConnectorEntity.class)
                );

        logger.debug("Fetched Data in " + (System.currentTimeMillis() - startMilli)+ " ms");
        logger.debug("Size " + stationConnectors.size());
        for (StationConnector stationConnector : stationConnectors) {
            //logger.debug("Start: " + stationConnector.getStart().toString());
            //logger.debug("Ende: " + stationConnector.getEnd().toString());
            boolean b = StationPlan.addStationPath(stationConnector, true);
            //logger.debug("Success: " + b);
        }
        logger.debug("Loaded Graph in " + (System.currentTimeMillis() - startMilli) + " ms");
        StationPlan.printGraph();
    }

    @Contract(pure = true)
    public PathCreationContext(Route route) {
        assignFromRoute(route);
    }

    public PathCreationContext(int routeId) {
        assignFromRoute(dataTransfer.getRouteById(routeId));
    }

    public void updateOutwardPath(@NotNull Path outwardPath) {
        outwardPath.setReturn(false);
        outwardPath.setRouteId(route.getRouteId());
        outwardPath.setPathId(this.outwardPath.getPathId());
        route.getPaths().stream().filter(e -> !e.isReturn()).findFirst().ifPresent(e -> route.getPaths().remove(e));
        route.getPaths().add(outwardPath);
        this.outwardPath = outwardPath;
        dataTransfer.updateOrCreate(route);
    }

    public void updateReturnPath(@NotNull Path returnPath) {
        returnPath.setReturn(true);
        returnPath.setRouteId(route.getRouteId());
        returnPath.setPathId(this.returnPath.getPathId());
        route.getPaths().stream().filter(Path::isReturn).findFirst().ifPresent(e -> route.getPaths().remove(e));
        route.getPaths().add(returnPath);
        this.returnPath = returnPath;
        dataTransfer.updateOrCreate(route);
    }

    public void deleteOutwardPath(@NotNull Path oldOutwardPath) {
        if (route.getPaths().contains(oldOutwardPath)) {
            outwardPath = null;
            route.getPaths().remove(oldOutwardPath);
            dataTransfer.deletePath(oldOutwardPath);
        } else {
            logger.warn("Tried to delete not existing outwardPath from Route");
        }
    }

    public void deleteReturnPath(@NotNull Path oldReturnPath) {
        if (route.getPaths().contains(oldReturnPath)) {
            returnPath = null;
            route.getPaths().remove(oldReturnPath);
            dataTransfer.deletePath(oldReturnPath);
        } else {
            logger.warn("Tried to delete not existing returnPath from Route");
        }
    }

    public PathDTO getOutwardPathDTO() {
        if (outwardPathDTO != null) {
            pathDTOMapper.updateDTO(outwardPath, outwardPathDTO);
        } else {
            outwardPathDTO = pathDTOMapper.toDTO(outwardPath);
        }
        return outwardPathDTO;
    }

    public PathDTO getReturnPathDTO() {
        if (returnPathDTO != null) {
            pathDTOMapper.updateDTO(returnPath, returnPathDTO);
        } else {
            returnPathDTO = pathDTOMapper.toDTO(returnPath);
        }
        return returnPathDTO;
    }

    public Path getOutwardPath() {
        if (outwardPath == null) {
            outwardPath = new Path();
            outwardPath.setRouteId(route.getRouteId());
            outwardPath.setReturn(false);
        }
        return outwardPath;
    }

    public Path getReturnPath() {
        if (returnPath == null) {
            returnPath = new Path();
            returnPath.setRouteId(route.getRouteId());
            returnPath.setReturn(false);
        }
        return returnPath;
    }

    public void reloadContextFromDatabase() {
        assignFromRoute(dataTransfer.getRouteById(route.getRouteId()));
    }

    private void assignFromRoute(Route newRoute) {
        route = newRoute;
        LinkedList<Path> paths = route.getPaths();
        logger.debug("SIZE OF PATH FROM DB: " + paths.size());
        paths.forEach(System.out::println);
        if (paths.size() > 0) {
            Path first = paths.getFirst();
            Path second = (paths.size() > 1) ? paths.get(1) : null;
            if (first.isReturn()) {
                returnPath = first;
            } else {
                outwardPath = first;
            }
            if (second != null) {
                if (second.isReturn()) {
                    returnPath = second;    //TODO Check if either was assigned before maybe error
                } else {
                    outwardPath = second;
                }
            }
        }
    }

}
