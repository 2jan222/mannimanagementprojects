package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.menu;

import javafx.stage.Stage;

/**
 * MenuViewItem allows the menus to get information about the views.
 * To be implemented by the views controller.
 */
public interface MenuViewItem {
    /**
     * Get the display name of a view.
     * @return name of view
     */
    String getMenuDisplayName();

    default void onMenuSwitch() {}

    default void onTabExit() {}

    default void setStage(Stage stage) {}
}
