package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.SuspendedEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class SuspendedPersistenceEntityMapper extends PersistenceEntityMapper<SuspendedEntity> {
    @Override
    public SuspendedEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(SuspendedEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("SuspendedEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<SuspendedEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<SuspendedEntity> query = session.createQuery("from SuspendedEntity ", SuspendedEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<SuspendedEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<SuspendedEntity> query = session.createQuery("from SuspendedEntity " + whereQueryFragment, SuspendedEntity.class);
            return query.getResultList();
        }
    }


}
