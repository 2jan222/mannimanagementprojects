package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Route;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.RouteDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * RouteDTO mapper.
 */
@Mapper(uses = {RouteRideDTOMapper.class, PathDTOMapper.class})
public interface RouteDTOMapper {

    RouteDTOMapper INSTANCE = Mappers.getMapper(RouteDTOMapper.class);

    RouteDTO toDTO(Route route);

    Route toMojo(RouteDTO routeDTO);

    void updateDTO(Route route, @MappingTarget RouteDTO routeDTO);

    void updateMojo(RouteDTO routeDTO, @MappingTarget Route route);

    List<RouteDTO> toDTOs(List<Route> routes);

    List<Route> toMojos(List<RouteDTO> routeDTOS);
}
