package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.bus_assignment.operationgroups;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_bus.exceptions.AssignBusException;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.application.grouping.BusAssignmentService;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.bus_assignment.operationgroups.groupbuttons.GroupButtonPresenter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.bus_assignment.operationgroups.groupbuttons.GroupButtonView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.bus_assignment.operationgroups.groupplane.GroupPlanePresenter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.bus_assignment.operationgroups.groupplane.GroupPlaneView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.BusDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.OperationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.RouteRideDTO;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import javafx.scene.Parent;
import org.jetbrains.annotations.NotNull;

/**
 * GroupPlaneButtonPair.
 * Connects the button in the GroupButtonPresenter and GroupPlanePresenter to show and hide groups
 */
@LoggerableClassDisplayName("[GroupPlaneButtonPair]")
public class GroupPlaneButtonPair {
    private static Loggerable logger = Loggerable.getInstance();
    private OperationGroupsPresenter operationGroupsPresenter;
    private GroupButtonPresenter buttonPresenter;
    private GroupPlanePresenter panePresenter;
    private Parent button;
    private Parent plane;
    private String groupName;
    private OperationDTO operation;
    private boolean isVisible = true;
    private BusAssignmentService busAssignmentService;

    public GroupPlaneButtonPair(String groupName, OperationDTO operation, @NotNull OperationGroupsPresenter presenter) {
        this.groupName = groupName;
        this.operation = operation;
        this.operationGroupsPresenter = presenter;
        GroupPlaneView groupPlaneView = new GroupPlaneView();
        GroupButtonView groupButtonView = new GroupButtonView();
        buttonPresenter = ((GroupButtonPresenter) groupButtonView.getPresenter());
        panePresenter = ((GroupPlanePresenter) groupPlaneView.getPresenter());
        buttonPresenter.setPlaneButtonPair(this);
        panePresenter.setPlaneButtonPair(this);
        button = groupButtonView.getView();
        plane = groupPlaneView.getView();
        busAssignmentService = presenter.getBusAssignmentService();
    }

    public void updatePairView() {
        panePresenter.update();
        buttonPresenter.update();
    }

    @Deprecated
    public void addRouteRide(RouteRideDTO routeRideDTO) throws AssignBusException {
        logger.debug("Add Ride");
        //operation = busAssignmentService.addRouteRide(operation, routeRideDTO);
        logger.debug("Added Ride");
        panePresenter.update();
        operationGroupsPresenter.updateUngroupedRides(routeRideDTO, false);
    }

    public void addBus(BusDTO busDTO) {
        logger.debug("Add Bus");
        operation = busAssignmentService.addBus(operation, busDTO);
        logger.debug("Added Bus");
        panePresenter.update();
        operationGroupsPresenter.updateUngroupedBuses(busDTO, false);
    }

    public void removeBus() {
        logger.debug("Remove bus");
        BusDTO move = operation.getBus();
        operation = busAssignmentService.removeBus(operation);
        logger.debug("Removed bus");
        panePresenter.update();
        operationGroupsPresenter.updateUngroupedBuses(move, true);
    }

    public void hideToggle(boolean button) {
        if (isVisible) {
            isVisible = false;
            operationGroupsPresenter.hide(this);
        } else {
            isVisible = true;
            operationGroupsPresenter.show(this);
        }
        if (!button) {
            buttonPresenter.toggleCheckBox();
        }
    }









    /*GETTER & SETTER*/

    public GroupButtonPresenter getButtonPresenter() {
        return buttonPresenter;
    }

    public GroupPlanePresenter getPanePresenter() {
        return panePresenter;
    }

    public Parent getButton() {
        return button;
    }

    public Parent getPlane() {
        return plane;
    }

    public String getGroupName() {
        return groupName;
    }

    public OperationDTO getOperation() {
        return operation;
    }

    public void setOperationDTO(OperationDTO operationDTO) {
        this.operation = operationDTO;
    }

    public void updateUp() {
        operationGroupsPresenter.refreshRoot();
    }
}
