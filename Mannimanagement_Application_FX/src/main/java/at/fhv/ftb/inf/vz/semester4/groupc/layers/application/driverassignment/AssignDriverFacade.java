package at.fhv.ftb.inf.vz.semester4.groupc.layers.application.driverassignment;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver.AssignDriverDataTransfer;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver.exceptions.AssignDriverContextCreationException;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver.exceptions.AssignDriverDataTransferUpdateException;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Driver;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.ShiftEntry;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.ShiftRide;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.BusDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.DriverDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.ShiftEntryDTO;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;

public interface AssignDriverFacade {

    LinkedList<ShiftEntryDTO> getShiftsByOperationId(Integer operationId, LocalDate date) throws AssignDriverContextCreationException;

    LinkedList<Integer> getOperationsByIDOfDay(LocalDate date);

    LinkedList<DriverDTO> getFreeDriverFormShift(ShiftEntryDTO shiftEntryDTO) throws AssignDriverContextCreationException;

    Driver getDriverById(Integer driverId, LocalDate date) throws AssignDriverContextCreationException;

    boolean assignDriverToShift(ShiftEntryDTO shiftDTO, DriverDTO driverDTO) throws AssignDriverContextCreationException, AssignDriverDataTransferUpdateException;

    boolean removeDriverFromShift(ShiftEntryDTO shiftDTO) throws AssignDriverContextCreationException, AssignDriverDataTransferUpdateException;

    boolean save(LocalDate date);

    ShiftEntryDTO setShift(Integer operationId, LinkedList<ShiftRide> rides, LocalDate date) throws AssignDriverContextCreationException;

    ShiftEntry setShiftForTimeFrame(Integer operationId, LocalTime start, LocalTime end, LocalDate date) throws AssignDriverContextCreationException, AssignDriverDataTransferUpdateException;

    BusDTO getBusByOperationId(Integer operationId, LocalDate date) throws AssignDriverContextCreationException;

    LinkedList<LocalDate> getDatesWithOperationsWhichHaveBus();

    void init(AssignDriverDataTransfer dataTransfer);

    LinkedList<ShiftRide> getRidesByOperationId(Integer id, LocalDate date) throws AssignDriverContextCreationException;
}
