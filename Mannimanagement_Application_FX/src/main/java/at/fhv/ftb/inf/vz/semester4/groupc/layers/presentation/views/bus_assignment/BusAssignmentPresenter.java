package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.bus_assignment;


import at.fhv.ftb.inf.vz.semester4.groupc.layers.application.grouping.BusAssignmentService;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.bus_assignment.operationgroups.OperationGroupsPresenter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.bus_assignment.operationgroups.OperationGroupsView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.menu.MenuViewItem;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.BusDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.OperationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.RouteRideDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.util.DragAndDropManager;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.LoggerableColor;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.net.URL;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.function.Consumer;

/**
 * BusAssignmentPresenter.
 * Presenter for all bus to route-ride operations
 */
@LoggerableClassDisplayName("[BusAssignmentPresenter]")
public class BusAssignmentPresenter implements MenuViewItem, Initializable {
    @Inject
    BusAssignmentService busAssignmentService;
    @Inject
    Loggerable logger;

    @FXML
    private DatePicker assignmentDateDatePicker;
    @FXML
    private AnchorPane operationViewRoot;
    @FXML
    private TableView<RouteRideDTO> rideTableView;
    @FXML
    private TableColumn<RouteRideDTO, Integer> routeNumberCol;
    @FXML
    private TableColumn<RouteRideDTO, String> routeNameCol;
    @FXML
    private TableColumn<RouteRideDTO, String> startTimeCol;
    @FXML
    private TableColumn<RouteRideDTO, String> endTimeCol;
    @FXML
    private TableColumn<RouteRideDTO, Integer> ridesNeededPlacesCol;
    @FXML
    private TableView<BusDTO> busTableView;
    @FXML
    private TableColumn<BusDTO, String> busMakeCol;
    @FXML
    private TableColumn<BusDTO, String> busModelCol;
    @FXML
    private TableColumn<BusDTO, String> busNoteCol;
    @FXML
    private TableColumn<BusDTO, Integer> busPlacesSumCol;
    @FXML
    private TableColumn<BusDTO, Integer> busPlacesSeatCol;
    @FXML
    private TableColumn<BusDTO, Integer> busPlacesStandCol;
    @FXML
    private TableColumn<BusDTO, String> busLicenceCol;

    private ObservableList<RouteRideDTO> rides = FXCollections.observableArrayList();
    private ObservableList<BusDTO> buses = FXCollections.observableArrayList();


    private OperationGroupsPresenter operationGroupsPresenter;

    @Override
    public String getMenuDisplayName() {
        return "Assign Buses";
    }

    @Override
    public void onMenuSwitch() {
        refresh();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("initialize");
        /* DATE PICKER */
        initDatePicker();
        /* RIDE TABLE VIEW */
        //initRideTableView();
        /* BUS TABLE VIEW */
        initBusTableView();
        /* Groups View */
        initGroupsView();
        logger.debug("initialized");
    }

    private void initDatePicker() {
        assignmentDateDatePicker.setValue(LocalDate.now());
        assignmentDateDatePicker.setOnAction(ignored -> refresh());
        assignmentDateDatePicker.setValue(LocalDate.now());
        assignmentDateDatePicker.setDayCellFactory(picker -> new DateCell() {
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                LocalDate today = LocalDate.now();
                if (busAssignmentService.getDatesWithOperations().contains(date)) {
                    setStyle("-fx-background-color: #3a62ff;");
                }
                setDisable(empty || date.compareTo(today) < 0 );
            }
        });
    }

    @SuppressFBWarnings("NP_NULL_ON_SOME_PATH")
    public void fetchAllAndUpdate(@NotNull ActionEvent actionEvent) {
        LocalDate value = ((DatePicker) actionEvent.getSource()).getValue();
        fetchAllAndUpdate((value != null) ? value : LocalDate.now());
    }

    private void fetchAllAndUpdate(@NotNull LocalDate date) {
        long start = System.currentTimeMillis();
        logger.debug("FETCH ALL", LoggerableColor.ANSI_YELLOW);
        operationGroupsPresenter.changeDate(date);   //RESETS GROUPS VIEW
        logger.debug("Date Changed after " + (System.currentTimeMillis() - start) + "ms");
        fetchGroupsForDate(date);
        logger.debug("Fetched Groups " + (System.currentTimeMillis() - start) + "ms");
        operationGroupsPresenter.updateGroupView();
        logger.debug("Updated View after " + (System.currentTimeMillis() - start) + "ms");
        //updateUngroupedRides();
        //logger.debug("Updated Ungrouped Rides after" + (System.currentTimeMillis() - start) + "ms");
        fetchUngroupedBuses(date, list -> {
            buses = FXCollections.observableArrayList(list);
            busTableView.setItems(buses);
        });
        logger.debug("Updated Ungrouped Buses after " + (System.currentTimeMillis() - start) + "ms");
    }

    @SuppressFBWarnings("NP_NULL_ON_SOME_PATH")
    private void fetchGroupsForDate(LocalDate date) {
        LinkedList<OperationDTO> openOperations = busAssignmentService.getGroupsForDate(date);
        operationGroupsPresenter.addGroups(openOperations);
    }


    @SuppressFBWarnings("NP_NULL_ON_SOME_PATH")
    private void fetchUngroupedBuses(LocalDate date, @NotNull Consumer<LinkedList<BusDTO>> consumer) {
        LinkedList<BusDTO> busDTOs = busAssignmentService.getUngroupedBusesForDate(date);
        consumer.accept(busDTOs);
    }

    @SuppressFBWarnings("NP_NULL_ON_SOME_PATH")
    @Deprecated
    public void fetchGroupedBuses(LocalDate date, @NotNull Consumer<LinkedList<BusDTO>> consumer) {
        //LinkedList<BusDTO> busDTOs = busAssignmentService.getGroupedBusesForDate(date);
        //consumer.accept(busDTOs);
    }

    public void refresh() {
        logger.debug("refresh");
        LocalDate date = assignmentDateDatePicker.getValue();
        if (date == null) {
            date = LocalDate.now();
        }
        fetchAllAndUpdate(date);
    }

    private void initBusTableView() {
        busTableView.setItems(buses);

        busMakeCol.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getMake()));
        busModelCol.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getModel()));
        busNoteCol.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNote()));

        busPlacesSumCol.setCellValueFactory(cellData ->
                new SimpleIntegerProperty(cellData.getValue().getPlaces()).asObject());
        busPlacesSeatCol.setCellValueFactory(cellData ->
                new SimpleIntegerProperty(cellData.getValue().getSeatPlaces()).asObject());
        busPlacesStandCol.setCellValueFactory(cellData ->
                new SimpleIntegerProperty(cellData.getValue().getStandPlaces()).asObject());
        busLicenceCol.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getLicenceNumber()));

        busTableView.setOnDragDetected(e -> {
            Dragboard dragBoard = busTableView.startDragAndDrop(TransferMode.MOVE);
            ClipboardContent content = new ClipboardContent();
            BusDTO src = busTableView.getSelectionModel().getSelectedItem();
            DragAndDropManager.add("" + src.getBusId(), src);
            content.putString("" + src.getBusId());
            dragBoard.setContent(content);
            e.consume();
        });
    }

    private void initGroupsView() {
        OperationGroupsView operationGroupsView = new OperationGroupsView();
        operationGroupsPresenter = (OperationGroupsPresenter) operationGroupsView.getPresenter();
        operationGroupsPresenter.setParentPresenter(this);
        operationViewRoot.getChildren().add(operationGroupsView.getView());
    }



    public void updateUngroupedBuses(BusDTO busDTO, boolean add) {
        logger.debug("Update BusTableView add = " + add + " BusDTO = " + busDTO);
        if (add) {
            busTableView.getItems().add(busDTO);
        } else {
            busTableView.getItems().remove(busDTO);
        }
        busTableView.sort();
    }

    public void updateUngroupedRides(RouteRideDTO routeRideDTO, boolean add) {
        logger.debug("Update RideTableView add = " + add + " BusDTO = " + routeRideDTO);
        if (add) {
            rideTableView.getItems().add(routeRideDTO);
        } else {
            rideTableView.getItems().remove(routeRideDTO);
        }
    }

    /*
    private void initRideTableView() {
        rideTableView.setItems(rides);

        routeNumberCol.setCellValueFactory(cellData ->
                new SimpleIntegerProperty(cellData.getValue().getRouteNumber()).asObject());
        routeNameCol.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getVariation()));
        final DateTimeFormatter format = DateTimeFormatter.ofPattern("hh:mm a");
        startTimeCol.setCellValueFactory(cellData ->
                new SimpleStringProperty(
                        cellData.getValue().getStartTime().format(format)));
        endTimeCol.setCellValueFactory(cellData ->
                new SimpleStringProperty(
                        cellData.getValue().getEndTime().format(format)));
        ridesNeededPlacesCol.setCellValueFactory(cellData ->
                new SimpleIntegerProperty(cellData.getValue().getRequiredCapacity()).asObject());

        rideTableView.setOnDragDetected(e -> {
            Dragboard dragBoard = rideTableView.startDragAndDrop(TransferMode.ANY);
            ClipboardContent content = new ClipboardContent();
            RouteRideDTO src = rideTableView.getSelectionModel().getSelectedItem();
            DragAndDropManager.add(src);
            content.putString("" + src.hashCode());
            dragBoard.setContent(content);
            e.consume();
        });

        rideTableView.setSortPolicy(table -> {
            Comparator<RouteRideDTO> comparator = Comparator.comparing(RouteRideDTO::getStartTime);
            FXCollections.sort(table.getItems(), comparator);
            return true;
        });
    }
     */
}

