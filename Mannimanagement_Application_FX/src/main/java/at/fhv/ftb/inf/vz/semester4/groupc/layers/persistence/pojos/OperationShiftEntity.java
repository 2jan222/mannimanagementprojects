package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.TestOnly;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "operation_shift", schema = "public", catalog = "kmobil")
public class OperationShiftEntity implements DatabaseEntityMarker {
    private Integer operationShiftId;
    private LocalDate date;
    private LocalTime startTime;
    private LocalTime endTime;
    private Integer operationId;
    private Integer driverId;
    private Integer busId;
    private Set<RouteRideEntityFast> routeRidesByOperationShiftId;
    private DriverEntity driverByDriverId;

    @Contract(pure = true)
    public OperationShiftEntity() {
    }

    @Contract(pure = true)
    public OperationShiftEntity(Integer operationShiftId, LocalDate date, LocalTime startTime, LocalTime endTime, Integer operationId, Integer driverId, Integer busId) {
        this.operationShiftId = operationShiftId;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.operationId = operationId;
        this.driverId = driverId;
        this.busId = busId;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "operation_shift_id")
    public Integer getOperationShiftId() {
        return operationShiftId;
    }

    public void setOperationShiftId(Integer operationShiftId) {
        this.operationShiftId = operationShiftId;
    }

    @Basic
    @Column(name = "date")
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Basic
    @Column(name = "start_time")
    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "end_time")
    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "operation_id")
    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    @Basic
    @Column(name = "driver_id")
    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    @Basic
    @Column(name = "bus_id")
    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "operation_shift_ride", joinColumns = {@JoinColumn(name = "operation_shift_id")}, inverseJoinColumns = {
            @JoinColumn(name = "operation_ride_id")})
    public Set<RouteRideEntityFast> getRouteRidesByOperationShiftId() {
        return routeRidesByOperationShiftId;
    }

    public void setRouteRidesByOperationShiftId(Set<RouteRideEntityFast> routeRidesByOperationShiftId) {
        this.routeRidesByOperationShiftId = routeRidesByOperationShiftId;
    }

    @ManyToOne
    @JoinColumn(name = "driver_id", referencedColumnName = "driver_id", nullable = false, insertable = false, updatable = false)
    public DriverEntity getDriverByDriverId() {
        return driverByDriverId;
    }

    public void setDriverByDriverId(DriverEntity driverByDriverId) {
        this.driverByDriverId = driverByDriverId;
    }

    @Override
    public String toString() {
        return "OperationShiftEntity{" +
                "operationShiftId=" + operationShiftId +
                ", date=" + date +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", operationId=" + operationId +
                ", driverId=" + driverId +
                ", busId=" + busId +
                //", routeRidesByOperationShiftId=" + routeRidesByOperationShiftId.size() +
                ", driverByDriverId=" + driverByDriverId +
                '}';
    }

    @Contract(value = "null -> false", pure = true)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OperationShiftEntity that = (OperationShiftEntity) o;
        return Objects.equals(operationShiftId, that.operationShiftId) &&
                Objects.equals(date, that.date) &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(endTime, that.endTime) &&
                Objects.equals(operationId, that.operationId) &&
                Objects.equals(driverId, that.driverId) &&
                Objects.equals(busId, that.busId) &&
                Objects.equals(routeRidesByOperationShiftId, that.routeRidesByOperationShiftId) &&
                Objects.equals(driverByDriverId, that.driverByDriverId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(operationShiftId, date, startTime, endTime, operationId, driverId, busId, routeRidesByOperationShiftId, driverByDriverId);
    }
}
