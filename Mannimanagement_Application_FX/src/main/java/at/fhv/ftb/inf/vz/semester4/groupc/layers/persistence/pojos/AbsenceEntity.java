package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.TestOnly;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "absence", schema = "public", catalog = "kmobil")
public class AbsenceEntity implements DatabaseEntityMarker {
    private Integer absenceId;
    private Integer driverId;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private DriverEntity driver;

    @Contract(pure = true)
    public AbsenceEntity() {
    }

    @Contract(pure = true)
    @TestOnly
    public AbsenceEntity(int absenceId, int driverId, LocalDate dateFrom, LocalDate dateTo) {
        this.absenceId = absenceId;
        this.driverId = driverId;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "absence_id")
    public Integer getAbsenceId() {
        return absenceId;
    }

    public void setAbsenceId(Integer absenceId) {
        this.absenceId = absenceId;
    }

    @Basic
    @Column(name = "driver_id")
    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    @Basic
    @Column(name = "date_from")
    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    @Basic
    @Column(name = "date_to")
    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    @ManyToOne
    @JoinColumn(name = "driver_id", referencedColumnName = "driver_id", nullable = false, insertable = false, updatable = false)
    public DriverEntity getDriver() {
        return driver;
    }

    public void setDriver(DriverEntity driver) {
        this.driver = driver;
    }
}
