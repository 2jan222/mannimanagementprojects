package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.path;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.util.Mediator;
import com.github.jan222ik.Loggerable;

public class PathContextMediator extends Mediator<Integer, PathCreationContext> {
    private static Loggerable logger = Loggerable.getInstance();

    @Override
    public void setValue(Integer storageName, PathCreationContext value) {
        super.setValue(storageName, value);
        logger.debug("Set data context for route: " + storageName);
    }

    @Override
    public PathCreationContext getValue(Integer storageName) {
        PathCreationContext value = super.getValue(storageName);
        logger.debug("GET data context for route " + storageName);
        return value;
    }
}
