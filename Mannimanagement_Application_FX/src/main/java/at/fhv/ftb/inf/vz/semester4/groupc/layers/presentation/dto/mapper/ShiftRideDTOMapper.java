package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.ShiftRide;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.ShiftRideDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = StationDTOMapper.class)
public interface ShiftRideDTOMapper {

    ShiftRideDTOMapper INSTANCE = Mappers.getMapper(ShiftRideDTOMapper.class);

    @Mapping(target = "startStation", source = "start")
    @Mapping(target = "endStation", source = "end")
    @Mapping(target = "startStationName", source = "start.shortName")
    @Mapping(target = "endStationName", source = "end.shortName")
    ShiftRideDTO toDTO(ShiftRide shiftRide);

    @Mapping(target = "start", source = "startStation")
    @Mapping(target = "end", source = "endStation")
    ShiftRide toMojo(ShiftRideDTO shiftRideDTO);

    @Mapping(target = "startStation", source = "start")
    @Mapping(target = "endStation", source = "end")
    @Mapping(target = "startStationName", source = "start.shortName")
    @Mapping(target = "endStationName", source = "end.shortName")
    void updateDTO(ShiftRide shiftRide, @MappingTarget ShiftRideDTO shiftRideDTO);

    @Mapping(target = "start", source = "startStation")
    @Mapping(target = "end", source = "endStation")
    void updateMojo(ShiftRideDTO shiftRideDTO, @MappingTarget ShiftRide shiftRide);

    List<ShiftRideDTO> toDTOs(List<ShiftRide> shiftRides);

    List<ShiftRide> toMojos(List<ShiftRideDTO> shiftRideDTOS);

}
