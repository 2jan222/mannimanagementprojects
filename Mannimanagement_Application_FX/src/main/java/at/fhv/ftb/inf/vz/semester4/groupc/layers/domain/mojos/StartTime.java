package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos;

import java.time.LocalTime;
import java.util.Collection;
import java.util.LinkedList;

public class StartTime {
    private Integer startTimeId;
    private Integer pathId;
    private Integer requiredCapacity;
    private LocalTime startTime;
    private Integer startTimeType;
    private LinkedList<RouteRide> routeRides = new LinkedList<>();
    private Path path;

    public Integer getStartTimeId() {
        return startTimeId;
    }

    public void setStartTimeId(Integer startTimeId) {
        this.startTimeId = startTimeId;
    }

    public Integer getPathId() {
        return pathId;
    }

    public void setPathId(Integer pathId) {
        this.pathId = pathId;
    }

    public Integer getRequiredCapacity() {
        return requiredCapacity;
    }

    public void setRequiredCapacity(Integer requiredCapacity) {
        this.requiredCapacity = requiredCapacity;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public Integer getStartTimeType() {
        return startTimeType;
    }

    public void setStartTimeType(Integer startTimeType) {
        this.startTimeType = startTimeType;
    }

    public LinkedList<RouteRide> getRouteRides() {
        return (routeRides != null) ? routeRides : new LinkedList<>();
    }

    public void setRouteRides(Collection<RouteRide> routeRides) {
        this.routeRides = new LinkedList<>(routeRides);
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "StartTime{" +
                "startTimeId=" + startTimeId +
                ", pathId=" + pathId +
                ", requiredCapacity=" + requiredCapacity +
                ", startTime=" + startTime +
                ", startTimeType=" + startTimeType +
                ", routeRides=" + routeRides.size() +
                ", path=" + path +
                '}';
    }
}
