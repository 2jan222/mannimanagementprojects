package at.fhv.ftb.inf.vz.semester4.groupc.layers.application.path;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Path;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.PathDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.StationDTO;

public interface PathModelFacade {
    PathDTO getOutgoingPath(Integer routeId);

    PathDTO getReturnPath(Integer routeId);

    boolean addStationToOutgoingPath(Integer pathPositionBefore, StationDTO newStation, Integer routeId);

    boolean addStationToReturningPath(Integer pathPositionBefore, StationDTO newStation, Integer routeId);

    void updateOutgoingPath(Path pathFromStations);

    void updateReturnPath(Path pathFromStations);

    void delete(int posOnPath, int routeId, boolean isReture);
}
