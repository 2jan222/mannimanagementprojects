package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;


import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.PathStationEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class PathStationPersistenceEntityMapper extends PersistenceEntityMapper<PathStationEntity> {
    @Override
    public PathStationEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(PathStationEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("PathStationEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<PathStationEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<PathStationEntity> query = session.createQuery("from PathStationEntity ", PathStationEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<PathStationEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<PathStationEntity> query = session.createQuery("from PathStationEntity " + whereQueryFragment, PathStationEntity.class);
            return query.getResultList();
        }
    }


}
