package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.web.ShiftWishEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class ShiftWishEntityMapper extends PersistenceEntityMapper<ShiftWishEntity> {
    @Override
    public ShiftWishEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(ShiftWishEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("ShiftWishEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<ShiftWishEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<ShiftWishEntity> query = session.createQuery("from ShiftWishEntity ", ShiftWishEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<ShiftWishEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<ShiftWishEntity> query = session.createQuery("from ShiftWishEntity " + whereQueryFragment, ShiftWishEntity.class);
            return query.getResultList();
        }
    }
}
