package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatBusEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface FlatBusEntityMapper {

    FlatBusEntityMapper INSTANCE = Mappers.getMapper(FlatBusEntityMapper.class);

    Bus toMojo(FlatBusEntity flatBusEntity);


    @Mapping(target = "registrationDate", ignore = true)
    @Mapping(target = "note", ignore = true)
    @Mapping(target = "maintenanceKm", ignore = true)
    FlatBusEntity toPojo(Bus flatBus);

    void updateMojo(FlatBusEntity flatBusEntity, @MappingTarget Bus flatBus);

    @Mapping(target = "registrationDate", ignore = true)
    @Mapping(target = "note", ignore = true)
    @Mapping(target = "maintenanceKm", ignore = true)
    void updatePojo(Bus flatBus, @MappingTarget FlatBusEntity flatBusEntity);

    List<Bus> toMojos(List<FlatBusEntity> flatBusEntities);

    List<FlatBusEntity> toPojos(List<Bus> flatBuses);
}
