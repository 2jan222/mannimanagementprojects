package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos;

import org.jetbrains.annotations.Contract;

import java.time.LocalDate;
import java.util.LinkedList;

/**
 * OperationDTO.
 */
public class OperationDTO {
    private Integer operationId;
    private Integer busId;
    private LocalDate date;
    private LinkedList<RouteRideDTO> routeRideDTOS;
    private BusDTO bus;

    @Contract(pure = true)
    public OperationDTO(Integer operationId, LocalDate date) {
        this.operationId = operationId;
        this.date = date;
    }

    @Contract(pure = true)
    public OperationDTO() {}

    @Contract(pure = true)
    public OperationDTO(LocalDate dateOfGroups) {
        date = dateOfGroups;
    }

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LinkedList<RouteRideDTO> getRouteRideDTOS() {
        return routeRideDTOS;
    }

    public void setRouteRideDTOS(LinkedList<RouteRideDTO> routeRideDTOS) {
        this.routeRideDTOS = routeRideDTOS;
    }

    public BusDTO getBus() {
        return bus;
    }

    public void setBus(BusDTO bus) {
        this.bus = bus;
    }

    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    @Override
    public String toString() {
        return "OperationDTO{" +
                "operationId=" + operationId +
                ", busId=" + busId +
                ", date=" + date +
                ", routeRideDTOS=" + routeRideDTOS.size() +
                ", bus=" + bus +
                '}';
    }
}
