package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Driver;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.OperationFlat;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.ShiftEntry;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.ShiftRide;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.inject.Singleton;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.LinkedList;

import static at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver.AssignDriverActionChecker.*;

@Singleton
@LoggerableClassDisplayName("[A-D-Actions]")
public class AssignDriverActions {
    private static final Loggerable logger = Loggerable.getInstance();
    private static final AssignDriverActions actions = new AssignDriverActions();

    @Contract(pure = true)
    private AssignDriverActions() {
    }

    @Contract(pure = true)
    public static AssignDriverActions getInstance() {
        return actions;
    }

    @Nullable
    public static ShiftEntry createNewShift(Integer operationId,
                                     @NotNull LinkedList<ShiftRide> rides,
                                     @NotNull DataContext context) {
        if (rides.size() > 0) {
            ShiftEntry shiftEntry = new ShiftEntry(null, context.getDate(), rides.getFirst().getStartTime(),
                    rides.getLast().getEndTime().plus(1, ChronoUnit.MINUTES), operationId, context.getBusIdByOperationId(operationId));
            shiftEntry.setShiftRides(rides);
            boolean isOverlapping = checkShiftsOverlappingTime(shiftEntry, context);
            if (!isOverlapping) {
                boolean isLawful = AssignDriverActionChecker
                        .checkShiftComplianceToLaw(shiftEntry, 1, context);
                if (isLawful) {
                    context.addShift(shiftEntry); //Persist (All Checks true)
                    return shiftEntry;
                }
            }
        }
        return null;
    }

    @Nullable
    public static ShiftEntry createNewShiftForTimeframe(@NotNull OperationFlat operation,
                                                 @Nullable LocalTime start,
                                                 @Nullable LocalTime end,
                                                 @NotNull DataContext context) {
        start = (start != null) ? start : LocalTime.MIN;
        end = (end != null) ? end : LocalTime.MAX;
        LinkedList<ShiftRide> shiftRides = operation.getShiftRides();
        shiftRides.sort(Comparator.comparing(ShiftRide::getStartTime));
        LinkedList<ShiftRide> addRides = new LinkedList<>();
        try {
            for (ShiftRide s : shiftRides) {
                if (s.getEndTime().isBefore(end) && s.getStartTime().isAfter(start)) {
                    addRides.add(s);
                }
            }
            addRides.sort(Comparator.comparing(ShiftRide::getStartTime));
            if (addRides.size() > 0) {
                LinkedList<ShiftRide> rides = new LinkedList<>(addRides);
                ShiftEntry shiftEntry = new ShiftEntry(null, context.getDate(), rides.getFirst().getStartTime(),
                        rides.getLast().getEndTime(), operation.getOperationId(), context.getBusIdByOperationId(operation.getOperationId()));
                shiftEntry.setShiftRides(rides);
                boolean isOverlapping = checkShiftsOverlappingTime(shiftEntry, context);
                if (!isOverlapping) {
                    boolean isLawful = AssignDriverActionChecker
                            .checkShiftComplianceToLaw(shiftEntry, 0, context);
                    if (isLawful) {
                        logger.debug("Shift is lawful");
                        return shiftEntry;
                    }
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    public static boolean assignDriverToShift(Driver driver, ShiftEntry shiftEntry, DataContext context) {
        logger.debug("Try to assign driver " + driver + " to shift " + shiftEntry);
        if (driver != null) {
            System.out.println(1);
            if (driver.isQualifiedForShift(shiftEntry)) {
                System.out.println(2);
                if (context.hasShiftEntry(shiftEntry)) {
                    System.out.println(3);
                    if (shiftEntry.getDriverId() == null) {
                        System.out.println(4);
                        boolean shiftOverlapping = checkDriverShiftsNewShiftOverlapping(shiftEntry, driver, context);
                        System.out.println(5);
                        if (!shiftOverlapping) {
                            System.out.println(6);
                            boolean allowance = checkDriverAllowanceToPerformShiftEntry(shiftEntry, driver, context);
                            System.out.println(7);
                            if (allowance) {
                                shiftEntry.setDriver(driver);
                                shiftEntry.setDriverId(driver.getDriverId());
                                context.assignDriverToShift(driver, shiftEntry); //Persist (All Checks true)
                                logger.info("Driver is clear");
                                return true;
                            } else {
                                logger.debug("Driver has no allowance to work this shift");
                            }
                        } else {
                            logger.debug("ShiftEntry is overlapping with other shifts of driver");
                        }
                    } else {
                        logger.debug("Shift Entry already has a driver");
                    }
                } else {
                    logger.debug("Context does not know ShiftEntry");
                }
            } else {
                logger.debug("Driver is not qualified enough");
            }
            return false;
        } else {
            return removeDriverFromShift(shiftEntry, context);
        }
    }

    @Contract(pure = true)
    public static boolean removeDriverFromShift(@NotNull ShiftEntry shiftEntry, @NotNull DataContext context) {
        logger.debug("Try to remove driver to shift");
        context.removeDriverFromShift(shiftEntry.getDriverId(), shiftEntry);
        shiftEntry.setDriverId(null);
        shiftEntry.setDriver(null);
        return true;
    }

    public static LinkedList<Driver> calculateDriversApplicableForShift(@NotNull ShiftEntry target,
                                                                 @NotNull DataContext context) {
        logger.debug("Calc free drivers");
        LinkedList<Driver> allowed = new LinkedList<>();
        if (target.getDriverId() == null) {
            LinkedList<Driver> drivers = context.getDrivers();
            for (Driver d : drivers) {
                boolean onVacation = isOnVacation(d, target.getDate());
                if (!onVacation) {
                    boolean overlapping = checkDriverShiftsNewShiftOverlapping(target, d, context);
                    if (!overlapping) {
                        boolean allowance = checkDriverAllowanceToPerformShiftEntry(target, d, context);
                        if (allowance) {
                            logger.debug("Driver is free[@id" + d.getDriverId() + "]");
                            allowed.add(d);
                        }
                    }
                }
            }
        }
        return allowed;
    }

}
