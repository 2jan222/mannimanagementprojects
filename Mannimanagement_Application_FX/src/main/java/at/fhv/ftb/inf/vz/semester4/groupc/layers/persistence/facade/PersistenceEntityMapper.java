package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public abstract class PersistenceEntityMapper<T> implements DBMapper<T> {

    protected static void updateHelper(String table, String setString, String whereString) {
        System.out.println("Query = " + "UPDATE " + table + " " + setString + " " + whereString);
        Transaction tx;
        try (Session sess = DatabaseConnector.getSession()) {
            tx = sess.beginTransaction();
            Query query = sess.createQuery("UPDATE " + table + " " + setString + " " + whereString);
            query.executeUpdate();
            sess.flush();
            tx.commit();
        }
    }

    @Override
    public void create(DatabaseEntityMarker value) {
        Transaction tx;
        try (Session sess = DatabaseConnector.getSession()) {
            tx = sess.beginTransaction();
            sess.save(value);
            sess.flush();
            tx.commit();
        }

    }


    @Override
    abstract public T read(Integer id);

    public <R extends DatabaseEntityMarker> R read(Class<R> clazz, Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(clazz, id);
        }
    }

    @Override
    public void update(DatabaseEntityMarker value) {
        Transaction tx;
        try (Session sess = DatabaseConnector.getSession()) {
            tx = sess.beginTransaction();
            sess.saveOrUpdate(value);
            sess.flush();
            tx.commit();
        }
    }

    @Override
    public abstract void update(String setQueryFragment, String whereQueryFragment);

    @Override
    public void delete(DatabaseEntityMarker value) {
        Transaction tx;
        try (Session sess = DatabaseConnector.getSession()) {
            tx = sess.beginTransaction();
            sess.delete(value);
            sess.flush();
            tx.commit();
        }

    }

    @Override
    abstract public List<T> getAll();

    @Override
    abstract public List<T> getAllWhere(String whereQueryFragment);
}
