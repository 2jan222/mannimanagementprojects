package at.fhv.ftb.inf.vz.semester4.groupc.layers.application.grouping;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_bus.AssignBusActions;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_bus.AssignBusDataTransfer;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_bus.AssignBusDataTransferImpl;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Bus;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Operation;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.BusDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.OperationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.mapper.BusDTOMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.mapper.OperationDTOMapper;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.util.LinkedList;

/**
 * BusAssignmentService.
 */

@LoggerableClassDisplayName("[BusAssignmentService]")
public class BusAssignmentService {
    private static final Loggerable logger = Loggerable.getInstance();
    private static final AssignBusDataTransfer dataTransfer = new AssignBusDataTransferImpl();

    public OperationDTO removeBus(@NotNull OperationDTO operationDTO) {
        logger.debug("Remove BusDTO{id =" + operationDTO.getBusId() + "} from OperationDTO{" +
                "id = " + operationDTO.getOperationId() + "}");
        Operation operation = OperationDTOMapper.INSTANCE.toMojo(operationDTO);
        AssignBusActions.removeBusFromGroup(operation);
        dataTransfer.removeBusFromOperation(operation.getOperationId());
        operationDTO = OperationDTOMapper.INSTANCE.toDTO(operation);
        return operationDTO;
    }

    /**
     * Translates DTOs to Mojo, Redirects to AssignBusActions.assignBusToGroup(...) and updates database afterwards.
     *
     * @param operationDTO operation/group to add bus to
     * @param busDTO       bus to be added to group
     * @return updated operationDTO
     * @throws RuntimeException if bus does not exist on database.
     */
    public OperationDTO addBus(@NotNull OperationDTO operationDTO, @NotNull BusDTO busDTO) {
        logger.debug("Add BusDTO{id = " + busDTO.getBusId() + "} to OperationDTO{id =" +
                operationDTO.getOperationId() + "}");
        Bus bus = BusDTOMapper.INSTANCE.toMojo(busDTO);
        Operation operation = OperationDTOMapper.INSTANCE.toMojo(operationDTO);
        AssignBusActions.assignBusToGroup(operation, bus);
        dataTransfer.assignBusToGroup(busDTO.getBusId(), operationDTO.getOperationId());
        logger.debug("Added bus");
        return OperationDTOMapper.INSTANCE.toDTO(operation);
    }

    public LinkedList<OperationDTO> getGroupsForDate(@NotNull LocalDate dateIn) {
        LinkedList<Operation> groupsForDate = dataTransfer.getGroupsForDate(dateIn);
        return new LinkedList<>(OperationDTOMapper.INSTANCE.toDTOs(groupsForDate));
    }

    public LinkedList<BusDTO> getUngroupedBusesForDate(@NotNull LocalDate dateIn) {
        LinkedList<Bus> ungroupedBusesForDate = dataTransfer.getUngroupedBusesForDate(dateIn);
        return new LinkedList<>(BusDTOMapper.INSTANCE.toDTOs(ungroupedBusesForDate));
    }

    public LinkedList<LocalDate> getDatesWithOperations() {
        return dataTransfer.getDatesWithOperations();
    }
}
