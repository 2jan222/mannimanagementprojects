package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;

@SuppressWarnings("WeakerAccess")
public class Absence {
    private Integer absenceId;
    private Integer driverId;
    private LocalDate dateFrom;
    private LocalDate dateTo;

    public Integer getAbsenceId() {
        return absenceId;
    }

    public void setAbsenceId(Integer absenceId) {
        this.absenceId = absenceId;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public boolean isDateInAbsence(@NotNull LocalDate date) {
        return (date.isAfter(dateFrom) && date.isBefore(dateTo));
    }

}
