package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.menu.sidebar;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.menu.MenuIntegratedPresenter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.menu.MenuViewItem;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.util.FutureRunnable;
import com.airhacks.afterburner.views.FXMLView;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * SidebarPresenter.
 * Presenter for MenuSidebar.
 */
@LoggerableClassDisplayName("[Sidebar]")
public class SidebarPresenter {
    @Inject
    private Loggerable logger;
    @FXML
    private VBox sidebarContainer;
    private HashMap<String, FXMLView> viewByName = new HashMap<>();
    private Button currentTab;
    private MenuIntegratedPresenter mainController;


    private void changeTab(@NotNull FXMLView view) {
        logger.info("Change tab to '" + ((MenuViewItem) view.getPresenter()).getMenuDisplayName() + "'");
        mainController.changeContentView(view);
        Platform.runLater(new FutureRunnable(() -> {
            long start = System.currentTimeMillis();
            ((MenuViewItem) view.getPresenter()).onMenuSwitch();
            logger.debug("Menu Switched Done (" + (System.currentTimeMillis() - start) + " ms)");
        }));

    }

    private void switchStyle(Button current, boolean setWindowTitle) {
        if (currentTab != null) {
            currentTab.setStyle(" -fx-alignment: BASELINE_LEFT;\n" +
                    "    -fx-background-color: #3F3F3F;\n" +
                    "    -fx-text-fill: #FFF;\n" +
                    "    -fx-border-color: none;\n" +
                    "    -fx-max-width: 999999px;\n" +
                    "    margin-top: 15px;");
        }
        current.setStyle(" -fx-alignment: BASELINE_LEFT;\n" +
                "    -fx-background-color: #0096c9;\n" +
                "    -fx-text-fill: #FFF;\n" +
                "    -fx-border-color: none;\n" +
                "    -fx-max-width: 999999px;\n" +
                "    margin-top: 15px;");
        mainController.updateText(current.getText().replace("\n", ""), setWindowTitle);
        currentTab = current;
    }

    public void setMainController(MenuIntegratedPresenter controller) {
        mainController = controller;
        sidebarContainer.setOnMouseExited(event -> mainController.hamburgerAction());
    }

    private void changeToTab(@NotNull ActionEvent actionEvent, boolean setWindowTitle) {
        if (currentTab != null) {
            ((MenuViewItem) viewByName.get(currentTab.getText()).getPresenter()).onTabExit();
        }
        switchStyle((Button) actionEvent.getSource(), setWindowTitle);
        changeTab(viewByName.get(((Button) actionEvent.getSource()).getText()));
    }

    public void init(@NotNull LinkedList<FXMLView> views) {
        String menuDisplayName;
        Button first = null;
        Button btn;
        for (FXMLView view : views) {
            if (view.getPresenter() == null) {
                logger.error("Presenter is null of view: " + view.toString());
            } else {
                try {
                    MenuViewItem presenter = (MenuViewItem) view.getPresenter();
                    menuDisplayName = presenter.getMenuDisplayName();
                    viewByName.put(menuDisplayName, view);
                    btn = createSidebarButton(menuDisplayName);
                    sidebarContainer.getChildren().add(btn);
                    if (first == null) {
                        first = btn;
                    }
                } catch (ClassCastException castException) {
                    logger.error(castException.getMessage());
                }
            }
        }
        if (first != null) {
            changeToTab(new ActionEvent(first, null), false);
        }
    }

    private Button createSidebarButton(String text) {
        Button btn = new Button(text);
        btn.setId(text);
        /*
        btn.getStyleClass().add("sidebarMenu");
        File file = new File("sidebarbutton.css");
        File file2 = new File("at/fhv/ftb/inf/vz/semester4/groupc/layers/presentation/application/views/menu/sidebar/sidebarbutton.css");
        System.out.println(file);
        System.out.println(file.getAbsolutePath());
        System.out.println("FILE 2");
        System.out.println(file2);
        System.out.println(file2.getAbsolutePath());
        System.out.println(file2.exists()   );
        btn.getStylesheets().add(file2.getAbsolutePath());*/
        btn.setStyle(" -fx-alignment: BASELINE_LEFT;\n" +
                "    -fx-background-color: #3F3F3F;\n" +
                "    -fx-text-fill: #FFF;\n" +
                "    -fx-border-color: none;\n" +
                "    -fx-max-width: 999999px;\n" +
                "    margin-top: 15px;");
        btn.setOnAction(this::changeToTab);
        VBox.setMargin(btn, new Insets(15, 0, 0, 0));
        return btn;
    }

    @FXML
    private void changeToTab(ActionEvent actionEvent) {
        changeToTab(actionEvent, true);
    }

}
