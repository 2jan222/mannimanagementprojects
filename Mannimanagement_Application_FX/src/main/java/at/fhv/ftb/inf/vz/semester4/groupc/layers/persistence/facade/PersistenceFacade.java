package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.*;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.web.HolidayWishEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.web.ShiftWishEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.web.WebDriverEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class PersistenceFacade {
    private static PersistenceFacade ourInstance = new PersistenceFacade();

    private HashMap<Class<? extends DatabaseEntityMarker>, DBMapper<? extends DatabaseEntityMarker>> mappers = new HashMap<>();

    @Contract(pure = true)
    public static PersistenceFacade getInstance() {
        return ourInstance;
    }

    private PersistenceFacade() {

        mappers.putIfAbsent(BusEntity.class, new BusPersistenceEntityMapper());
        mappers.putIfAbsent(DriverEntity.class, new DriverPersistenceEntityMapper());
        mappers.putIfAbsent(DrivingLicensesEntity.class, new DrivingLicensesPersistenceEntityMapper());
        mappers.putIfAbsent(OperationEntity.class, new OperationPersistenceEntityMapper());
        mappers.putIfAbsent(PathEntity.class, new PathPersistenceEntityMapper());
        mappers.putIfAbsent(PathStationEntity.class, new PathStationPersistenceEntityMapper());
        mappers.putIfAbsent(RouteEntity.class, new RoutePersistenceEntityMapper());
        mappers.putIfAbsent(RouteRideEntity.class, new RouteRidePersistenceEntityMapper());
        mappers.putIfAbsent(ShiftEntity.class, new ShiftPersistenceEntityMapper());
        mappers.putIfAbsent(StartTimeEntity.class, new StartTimePersistenceEntityMapper());
        mappers.putIfAbsent(StationEntity.class, new StationPersistenceEntityMapper());
        mappers.putIfAbsent(SuspendedEntity.class, new SuspendedPersistenceEntityMapper());
        mappers.putIfAbsent(StationConnectorEntity.class, new StationConnectorPersistenceEntityMapper());
        mappers.putIfAbsent(FlatBusEntity.class, new FlatBusPersistenceEntityMapper());
        mappers.putIfAbsent(FlatStationEntity.class, new FlatStationPersistenceEntityMapper());
        mappers.putIfAbsent(FlatStationConnectorEntity.class, new FlatStationConnectorPersistenceEntityMapper());
        mappers.putIfAbsent(OperationShiftEntity.class, new OperationShiftPersistenceEntityMapper());
        mappers.putIfAbsent(AbsenceEntity.class, new AbsencePersistenceEntityMapper());
        mappers.putIfAbsent(FlatOperationEntity.class, new FlatOperationPersistenceEntityMapper());
        mappers.putIfAbsent(FlatRouteEntity.class, new FlatRoutePersistenceEntityMapper());
        mappers.putIfAbsent(OperationRouteRideEntityFast.class, new OperationRouteRideEntityFastPersistenceEntityMapper());
        mappers.putIfAbsent(WebDriverEntity.class, new WebDriverEntityMapper());
        mappers.putIfAbsent(HolidayWishEntity.class, new HolidayWishEntityMapper());
        mappers.putIfAbsent(ShiftWishEntity.class, new ShiftWishEntityMapper());
        mappers.putIfAbsent(RouteRideEntityFast.class, new RouteRideFastPersistenceEntityMapper());

    }

    public <R extends DatabaseEntityMarker> List<R> getAll(Class<R> clazz) {
        DBMapper<? extends DatabaseEntityMarker> mapper = getMapper(clazz);
        return mapper.getAll().stream().map(clazz::cast).collect(Collectors.toList());

    }

    public <R extends DatabaseEntityMarker> List<R> getAllWhere(Class<R> clazz, String where) {
        DBMapper<? extends DatabaseEntityMarker> mapper = getMapper(clazz);
        return mapper.getAllWhere(where).stream().map(clazz::cast).collect(Collectors.toList());

    }

    public void create(@NotNull DatabaseEntityMarker object) {
        DBMapper<? extends DatabaseEntityMarker> mapper = getMapper(object.getClass());
        mapper.create(object);
    }

    public <R extends DatabaseEntityMarker> R read(Integer id, Class<R> clazz) {
        DBMapper<? extends DatabaseEntityMarker> mapper = getMapper(clazz);
        return clazz.cast(mapper.read(id));
    }

    public void update(@NotNull DatabaseEntityMarker object) {
        DBMapper<? extends DatabaseEntityMarker> mapper = getMapper(object.getClass());
        mapper.update(object);
    }

    public void delete(@NotNull DatabaseEntityMarker object) {
        DBMapper<? extends DatabaseEntityMarker> mapper = getMapper(object.getClass());
        mapper.delete(object);
    }

    public void updateWithFragments(Class<? extends DatabaseEntityMarker> clazz, String setQueryFragment, String whereQueryFragment) {
        DBMapper<? extends DatabaseEntityMarker> mapper = getMapper(clazz);
        mapper.update(setQueryFragment, whereQueryFragment);
    }

    public <R> R executeTransaction(@NotNull Function<Session, R> function) {
        Session session = DatabaseConnector.getSession();
        Transaction transaction = session.beginTransaction();
        R apply = function.apply(session);
        transaction.commit();
        session.close();
        return apply;
    }

    private <T extends DatabaseEntityMarker> DBMapper<? extends DatabaseEntityMarker> getMapper(Class<T> clazz) {
        return mappers.get(clazz);
    }

}
