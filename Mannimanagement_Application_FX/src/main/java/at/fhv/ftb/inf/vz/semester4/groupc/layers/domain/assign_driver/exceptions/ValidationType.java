package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver.exceptions;

public enum ValidationType {
    BUS_ID_NULL,
    BUS_NULL,
    OPERATION_NULL,
    OPERATION_ID_NULL,
    OPERATION_HAS_NO_BUS
}
