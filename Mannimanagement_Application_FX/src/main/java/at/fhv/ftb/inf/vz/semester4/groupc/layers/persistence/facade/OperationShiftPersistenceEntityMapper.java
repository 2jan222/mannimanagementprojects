package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.OperationShiftEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class OperationShiftPersistenceEntityMapper extends PersistenceEntityMapper<OperationShiftEntity> {
    @Override
    public OperationShiftEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(OperationShiftEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("OperationShiftEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<OperationShiftEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<OperationShiftEntity> query = session.createQuery("from OperationShiftEntity ", OperationShiftEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<OperationShiftEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<OperationShiftEntity> query = session.createQuery("from OperationShiftEntity " + whereQueryFragment, OperationShiftEntity.class);
            return query.getResultList();
        }
    }
}
