package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatBusEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class FlatBusPersistenceEntityMapper extends PersistenceEntityMapper<FlatBusEntity> {
    @Override
    public FlatBusEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(FlatBusEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("FlatBusEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<FlatBusEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<FlatBusEntity> query = session.createQuery("from FlatBusEntity ", FlatBusEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<FlatBusEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<FlatBusEntity> query = session.createQuery("from FlatBusEntity " + whereQueryFragment, FlatBusEntity.class);
            return query.getResultList();
        }
    }
}
