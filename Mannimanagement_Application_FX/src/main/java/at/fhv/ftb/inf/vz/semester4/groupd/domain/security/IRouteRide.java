package at.fhv.ftb.inf.vz.semester4.groupd.domain.security;

import at.fhv.ftb.inf.vz.semester4.groupd.domain.Operation;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.StartTime;

import java.time.LocalTime;
import java.util.List;

public interface IRouteRide {
    public int getRouteRideId();

    public List<Operation> getOperations();

    public IRoute getRoute();

    public StartTime getStartTime();
    
    public LocalTime getStartingTime();

    public LocalTime getEndingTime();

    public String getStartStationName();

    public String getEndStationName();
}
