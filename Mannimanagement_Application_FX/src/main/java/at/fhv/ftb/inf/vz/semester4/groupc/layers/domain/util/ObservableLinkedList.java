package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.util;

import at.fhv.ftb.inf.vz.semester4.groupc.Unused;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

@Deprecated
@Unused
public class ObservableLinkedList<E extends Observable> extends Observable implements Observer {
    private Hashtable<E, Boolean> changedItems;
    private LinkedList<E> values;

    @Contract(pure = true)
    public ObservableLinkedList() {
        values = new LinkedList<>();
        changedItems = new Hashtable<>();
    }

    public void addAll(@NotNull LinkedList<E> elements) {
        elements.forEach(this::add);
        notifyObservers(changedItems);
        changedItems.clear();
    }

    private void addUpdateAfter(@NotNull E e) {
        e.addObserver(this);
        values.add(e);
        changedItems.put(e, false);
        setChanged();
    }

    public void add(E e) {
        addUpdateAfter(e);
        notifyObservers(changedItems);
        changedItems.clear();
    }

    public boolean remove(E e) {
        boolean remove = values.remove(e);
        changedItems.put(e, remove);
        setChanged();
        notifyObservers(changedItems);
        changedItems.clear();
        return remove;
    }

    public LinkedList<E> getList() {
        return values;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(Observable o, Object arg) {
        if (o != null) {
            try {
                E e = (E) o;
                changedItems.put(e, false);
                setChanged();
                notifyObservers(changedItems);
                changedItems.clear();
            } catch (ClassCastException ignored) {
                System.out.println("ERROR CLASS CAST");
            }
        }
    }

    @Override
    public String toString() {
        return "ObservableLinkedList{" +
                //"changedItems=" + changedItems + ", "
                "values=" + values +
                '}';
    }
}
