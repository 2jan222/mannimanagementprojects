package at.fhv.ftb.inf.vz.semester4.groupc.layers.application.path;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Path;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.RouteEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.StationEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.PersistenceFacade;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.StationEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.PathDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.PathStationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.RouteDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.StationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.mapper.RouteDTOMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.mapper.StationDTOMapper;
import javafx.collections.ObservableList;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.List;

public class PathService {
    private final PathModelFacade facade = new PathModelFacadeImpl();

    public LinkedList<StationDTO> fetchStations() {
        List<StationEntity> list = PersistenceFacade.getInstance().getAll(StationEntity.class);
        return new LinkedList<>(StationDTOMapper.INSTANCE.toDTOs(StationEntityMapper.INSTANCE.toMojos(list)));
    }

    public LinkedList<RouteDTO> getRoutes() {
        return new LinkedList<>(RouteDTOMapper.INSTANCE.toDTOs(RouteEntityMapper.INSTANCE.toMojos(
                PersistenceFacade.getInstance().getAll(RouteEntity.class))));
    }

    public PathDTO getOutgoingPath(@NotNull RouteDTO currentRoute) {
        return facade.getOutgoingPath(currentRoute.getRouteId());
    }

    public PathDTO getReturnPath(@NotNull RouteDTO currentRoute) {
        return facade.getReturnPath(currentRoute.getRouteId());
    }

    public boolean addStation(@NotNull PathDTO pathDTO, Integer beforeDTO, StationDTO newStation) {
        if (pathDTO.isReturn()) {
            return facade.addStationToReturningPath(beforeDTO, newStation, pathDTO.getRouteId());
        } else {
            return facade.addStationToOutgoingPath(beforeDTO, newStation, pathDTO.getRouteId());
        }
    }

    public Path createPathFromStations(@NotNull ObservableList<PathStationDTO> stations, Integer routeId, boolean isRetoure) {
        Path path = new Path();
        path.setRouteId(routeId);
        PathStationDTO before = null;
        for (PathStationDTO s : stations) {
            Integer b = (before != null) ? before.getPositionOnPath() : null;
            if (!isRetoure) {
                facade.addStationToReturningPath(b, s.getConnection().getEnd(), routeId);
            } else {
                facade.addStationToOutgoingPath(b, s.getConnection().getEnd(), routeId);
            }
            before = s;
        }
        return path;
    }

    public void updateOutgoingPath(Path pathFromStations) {
        facade.updateOutgoingPath(pathFromStations);
    }

    public void updateOutgoingPathFromItems(ObservableList<PathStationDTO> stations, Integer routeId) {
        updateOutgoingPath(createPathFromStations(stations, routeId, false));
    }

    public void updateReturnPathFromItems(ObservableList<PathStationDTO> stations, Integer routeId) {
        updateReturnPath(createPathFromStations(stations, routeId, true));
    }

    private void updateReturnPath(Path pathFromStations) {
        facade.updateReturnPath(pathFromStations);
    }

    public void delete(int posOnPath, int routeId, boolean isReture) {
        facade.delete(posOnPath, routeId, isReture);
    }
}
