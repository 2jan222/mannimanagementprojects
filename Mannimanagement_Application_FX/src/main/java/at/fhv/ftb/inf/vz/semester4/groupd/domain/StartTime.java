package at.fhv.ftb.inf.vz.semester4.groupd.domain;


import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.StartTimeEntity;

import java.time.LocalTime;
import java.util.Set;
import java.util.stream.Collectors;

public class StartTime {
	private StartTimeEntity startTimeEntity;
	
    private int startTimeId;
    private int requiredCapacity;
    private LocalTime startTime;
    private Integer startTimeType;
    private Set<RouteRide> routeRides;
    private Path path;
    
    public StartTime (StartTimeEntity startTime) {
    	startTimeEntity = startTime;
    }

    public StartTime() {
        this(new StartTimeEntity());
    }

    public int getStartTimeId() {
    	if(startTimeId == 0) {
    		startTimeId = startTimeEntity.getStartTimeId();
    	}
    	
        return startTimeId;
    }

    public void setStartTimeId(int startTimeId) {
        this.startTimeId = startTimeId;
    }
    
    public int getRequiredCapacity() {
    	if(requiredCapacity == 0) {
    		requiredCapacity = startTimeEntity.getRequiredCapacity();
    	}
        return requiredCapacity;
    }

    public void setRequiredCapacity(int requiredCapacity) {
        this.requiredCapacity = requiredCapacity;
    }

    public LocalTime getStartTime() {
    	if(startTime == null) {
    		startTime = startTimeEntity.getStartTime();
    	}
    	
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public Integer getStartTimeType() {
        return startTimeEntity.getStartTimeType();
    }

    public void setStartTimeType(Integer startTimeType) {
        startTimeEntity.setStartTimeType(startTimeType);
    }
    
    public Set<RouteRide> getRouteRides() {
    	if(routeRides == null) {
    		routeRides = startTimeEntity.getRouteRidesByStartTimeId().stream().map(RouteRide::new).collect(Collectors.toSet());
    	}
        return routeRides;
    }

    public void setRouteRides(Set<RouteRide> routeRides) {
        this.routeRides = routeRides;
    }

    public Path getPath() {
    	if(path == null) {
    		path = new Path(startTimeEntity.getPathByPathId());
    	}
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }
    
    public DayType getDaytype() {
    	return DayType.values()[(getStartTimeType() != null) ? getStartTimeType() : 0];
    }
}
