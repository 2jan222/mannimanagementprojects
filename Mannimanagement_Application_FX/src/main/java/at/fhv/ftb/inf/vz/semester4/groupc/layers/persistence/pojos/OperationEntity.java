package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.TestOnly;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "operation", schema = "public", catalog = "kmobil")
public class OperationEntity implements DatabaseEntityMarker {
    private Integer operationId;
    private LocalDate date;
    private Set<RouteRideEntityFast> routeRidesByOperationId;
    private Integer busId;
    private FlatBusEntity busByBusId;

    @Contract(pure = true)
    public OperationEntity() {
    }

    @Contract(pure = true)
    public OperationEntity(Integer operationId, LocalDate date, Set<RouteRideEntityFast> routeRidesByOperationId, Integer busId, FlatBusEntity busByBusId) {
        this.operationId = operationId;
        this.date = date;
        this.routeRidesByOperationId = routeRidesByOperationId;
        this.busId = busId;
        this.busByBusId = busByBusId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "operation_id")
    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    @Basic
    @Column(name = "date")
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }


    @Column(name = "bus_id")
    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "operation_ride", joinColumns = {@JoinColumn(name = "operation_id")}, inverseJoinColumns = {
            @JoinColumn(name = "route_ride_id")})
    public Set<RouteRideEntityFast> getRouteRidesByOperationId() {
        return routeRidesByOperationId;
    }

    public void setRouteRidesByOperationId(Set<RouteRideEntityFast> routeRidesByOperationId) {
        this.routeRidesByOperationId = routeRidesByOperationId;
    }

    @Override
    public String toString() {
        return "OperationEntity{" +
                "operationId=" + operationId +
                ", date=" + date +
                ", routeRidesByOperationId=" + ((routeRidesByOperationId != null) ? routeRidesByOperationId.size() : "null") +
                ", busId=" + busId +
                '}';
    }

    @ManyToOne
    @JoinColumn(name = "bus_id", referencedColumnName = "bus_id", insertable = false, updatable = false)
    public FlatBusEntity getBusByBusId() {
        return busByBusId;
    }

    public void setBusByBusId(FlatBusEntity busByBusId) {
        this.busByBusId = busByBusId;
    }

    @Contract(value = "null -> false", pure = true)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OperationEntity that = (OperationEntity) o;
        return Objects.equals(operationId, that.operationId) &&
                Objects.equals(date, that.date) &&
                Objects.equals(routeRidesByOperationId, that.routeRidesByOperationId) &&
                Objects.equals(busId, that.busId) &&
                Objects.equals(busByBusId, that.busByBusId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(operationId, date, routeRidesByOperationId, busId, busByBusId);
    }
}
