package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.path.pathcontrolspane.pathsofroute;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.application.path.PathService;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.path.pathcontrolspane.PathControlsPanePresenter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.PathDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.PathStationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.StationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.util.DragAndDropManager;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.util.NotificationCreator;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.net.URL;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.function.Supplier;

/**
 * PathsOfRoutePanePresenter.
 * Views path of a Route.
 * normally two paths: Trip to and Trip back.
 */
@LoggerableClassDisplayName("[PathsOfRoutePanePresenter]")
public class PathsOfRoutePanePresenter implements Initializable {
    @Inject
    private static Loggerable logger;
    @Inject
    private static PathService service;

    @FXML
    private TableView<PathStationDTO> outgoingPathTableView;
    @FXML
    private TableColumn<PathStationDTO, Integer> outgoingPathNumOnPathCol;
    @FXML
    private TableColumn<PathStationDTO, String> outgoingPathShortNameCol;
    @FXML
    private TableColumn<PathStationDTO, String> outgoingPathNameCol;
    @FXML
    private TableColumn<PathStationDTO, String> outgoingPathActionCol;
    @FXML
    private TableColumn<PathStationDTO, String> outgoingPathDurationCol;
    @FXML
    private TableColumn<PathStationDTO, String> outgoingPathDistanceCol;


    @FXML
    private TableView<PathStationDTO> returnPathTableView;
    @FXML
    private TableColumn<PathStationDTO, Integer> returnPathNumOnPathCol;
    @FXML
    private TableColumn<PathStationDTO, String> returnPathShortNameCol;
    @FXML
    private TableColumn<PathStationDTO, String> returnPathNameCol;
    @FXML
    private TableColumn<PathStationDTO, String> returnPathDurationCol;
    @FXML
    private TableColumn<PathStationDTO, String> returnPathDistanceCol;
    @FXML
    private TableColumn<PathStationDTO, String> returnPathActionCol;


    @FXML
    private Button validateBackBtn;
    @FXML
    private Button validateToBtn;
    @FXML
    private Button saveToBtn;
    @FXML
    private Button saveBackBtn;

    private PathControlsPanePresenter wrapperPresenter;
    private Supplier<FontAwesomeIconView> questionIconSupplier = () -> new FontAwesomeIconView(FontAwesomeIcon.
            QUESTION_CIRCLE);
    private Supplier<FontAwesomeIconView> crossIconSupplier = () -> new FontAwesomeIconView(FontAwesomeIcon.
            TIMES_CIRCLE_ALT);
    private Supplier<FontAwesomeIconView> checkIconSupplier = () -> new FontAwesomeIconView(FontAwesomeIcon.
            CHECK_CIRCLE_ALT);


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("initialize");
        initTableView(outgoingPathTableView, outgoingPathNumOnPathCol, outgoingPathShortNameCol, outgoingPathNameCol, outgoingPathDurationCol, outgoingPathDistanceCol, outgoingPathActionCol);
        initTableView(returnPathTableView, returnPathNumOnPathCol, returnPathShortNameCol, returnPathNameCol, returnPathDurationCol, returnPathDistanceCol, returnPathActionCol);
        logger.debug("initialized");
    }

    private void initTableView(@NotNull TableView<PathStationDTO> tableView,
                               @NotNull TableColumn<PathStationDTO, Integer> numOnPathCol,
                               @NotNull TableColumn<PathStationDTO, String> shortNameCol,
                               @NotNull TableColumn<PathStationDTO, String> nameCol,
                               @NotNull TableColumn<PathStationDTO, String> durationCol,
                               @NotNull TableColumn<PathStationDTO, String> distanceCol,
                               @NotNull TableColumn<PathStationDTO, String> actionCol) {
        numOnPathCol.setCellValueFactory(p -> new ReadOnlyObjectWrapper<>(p.getValue().getPositionOnPath()));
        shortNameCol.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getConnection().getEnd().getShortName()));
        nameCol.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getConnection().getEnd().getStationName()));
        durationCol.setCellValueFactory(data -> new SimpleStringProperty(Math.floor(data.getValue().getConnection().getTimeFromPrevious()/60.0) + " min"));
        distanceCol.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getConnection().getDistanceFromPrevious() + " m"));
        actionCol.setCellValueFactory(ignored -> null);
        actionCol.setCellFactory(createCallBack());
        initSetDragAndDropListeners(tableView);
    }

    @NotNull
    @Contract(value = " -> new", pure = true)
    private Callback<TableColumn<PathStationDTO, String>, TableCell<PathStationDTO, String>> createCallBack() {
        return new Callback<TableColumn<PathStationDTO, String>, TableCell<PathStationDTO, String>>() {
            @Override
            public TableCell<PathStationDTO, String> call(final TableColumn<PathStationDTO, String> param) {
                return new TableCell<PathStationDTO, String>() {

                    final FontAwesomeIconView arrowUp = new FontAwesomeIconView(FontAwesomeIcon.ARROW_UP);
                    final Button upBtn = new Button("", arrowUp);
                    final FontAwesomeIconView arrowDown = new FontAwesomeIconView(FontAwesomeIcon.ARROW_DOWN);
                    final Button downBtn = new Button("", arrowDown);
                    final FontAwesomeIconView remove = new FontAwesomeIconView(FontAwesomeIcon.TRASH);
                    final Button removeBtn = new Button("", remove);

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            upBtn.setOnAction(event -> {
                                int index = getIndex();
                                PathStationDTO station = getTableView().getItems().remove(index);
                                index = (index - 1 > -1) ? index - 1 : 0;
                                getTableView().getItems().add(index, station);
                                PathDTO info = (getTableView() == outgoingPathTableView) ? wrapperPresenter.getOutgoingPath() : wrapperPresenter.getPathBackInfo();
                                if (getTableView() == outgoingPathTableView) {
                                    service.updateOutgoingPathFromItems(getTableView().getItems(), info.getRouteId());
                                } else {
                                    service.updateReturnPathFromItems(getTableView().getItems(), info.getRouteId());
                                }
                                logger.debug("Up: " + station);

                            });
                            downBtn.setOnAction(event -> {
                                int index = getIndex();
                                ObservableList<PathStationDTO> items = getTableView().getItems();
                                PathStationDTO station = items.remove(index);
                                index = (index + 1 < items.size()) ? index + 1 : items.size(); //size() as addLast()
                                items.add(index, station);
                                PathDTO info = (getTableView() == outgoingPathTableView) ? wrapperPresenter.getOutgoingPath() : wrapperPresenter.getPathBackInfo();
                                if (getTableView() == outgoingPathTableView) {
                                    service.updateOutgoingPathFromItems(getTableView().getItems(), info.getRouteId());
                                } else {
                                    service.updateReturnPathFromItems(getTableView().getItems(), info.getRouteId());
                                }
                                logger.debug("Down: " + station);
                            });
                            removeBtn.setOnAction(event -> {
                                PathStationDTO pathStationDTO = getTableView().getItems().get(getIndex());
                                getTableView().getItems().remove(pathStationDTO);
                                PathDTO info = (getTableView() == outgoingPathTableView) ? wrapperPresenter.getOutgoingPath() : wrapperPresenter.getPathBackInfo();
                                if (getTableView() == outgoingPathTableView) {
                                    service.delete(pathStationDTO.getPositionOnPath(), info.getRouteId(), false);
                                } else {
                                    //application.updateReturnPathFromItems(getTableView().getItems(), info.getRouteId());
                                    service.delete(pathStationDTO.getPositionOnPath(), info.getRouteId(), true);
                                }
                                logger.debug("DELETE: " + pathStationDTO);
                            });
                            upBtn.setStyle("-fx-background-color: none");
                            downBtn.setStyle("-fx-background-color: none");
                            removeBtn.setStyle("-fx-background-color: none");
                            //HBox hBox = new HBox(upBtn, downBtn, removeBtn);
                            HBox hBox = new HBox(removeBtn);
                            hBox.setSpacing(3);
                            setGraphic(hBox);
                            setText(null);
                        }
                    }
                };
            }
        };
    }

    public void setWrapperPresenter(PathControlsPanePresenter wrapperPresenter) {
        this.wrapperPresenter = wrapperPresenter;
    }

    public void refreshUp() {
        wrapperPresenter.refreshFromBelow();
    }

    public void refresh() {
        logger.debug("Refresh");
        PathDTO pathToInfo = wrapperPresenter.getOutgoingPath();
        PathDTO pathBackInfo = wrapperPresenter.getPathBackInfo();
        setOutgoingPathStations(pathToInfo);
        setPathBackStations(pathBackInfo);
        //validateButtonListeners(pathToInfo, validateToBtn);
        //validateButtonListeners(pathBackInfo, validateBackBtn);
        //validateToBtn.setGraphic((pathToInfo.isHasChanged()) ? questionIconSupplier.get() : (pathToInfo.isIsValid()) ? checkIconSupplier.get() : crossIconSupplier.get());
        //validateBackBtn.setGraphic((pathBackInfo.isHasChanged()) ? questionIconSupplier.get() : (pathBackInfo.isIsValid()) ? checkIconSupplier.get() : crossIconSupplier.get());
        //saveBtnChangeListenerSetup(pathToInfo, saveToBtn);
        //saveBtnChangeListenerSetup(pathBackInfo, saveBackBtn);
        //refreshDown(); has no children presenter
    }

    /*
    private void saveBtnChangeListenerSetup(@NotNull PathCreationInfo pathInfo, @NotNull Button button) {
        pathInfo.registerIsSavedChangeListener((observable, oldValue, newValue) -> {
            logger.debug("Change Listener For IsSave Property has fired");
            button.setVisible(!newValue);
        });
    }

    private void validateButtonListeners(@NotNull PathCreationInfo pathInfo, @NotNull Button button) {
        pathInfo.registerIsValidChangeListener((observable, oldValue, newValue) -> {
            logger.debug("Change Listener For IsValid Property has fired");
            if (newValue) {
                button.setGraphic(checkIconSupplier.get());
            } else {
                button.setGraphic(crossIconSupplier.get());
            }
        });
        pathInfo.registerHasChangedChangeListener((observable, oldValue, newValue) -> {
            logger.debug("Change Listener For hasChanged Property has fired");
            if (newValue) {
                button.setGraphic(questionIconSupplier.get());
            }
        });
    }

     */

    private void setOutgoingPathStations(@NotNull PathDTO path) {
        LinkedList<PathStationDTO> pathStations = path.getPathStations();
        pathStations.sort(Comparator.comparingInt(PathStationDTO::getPositionOnPath));
        //pathStations.add(0, new PathStationDTO(0, new StationConnectorDTO(null, pathStations.getFirst().getConnection().getStart(), 0, 0)));
        //pathStations.forEach(e -> logger.debug(e));
        outgoingPathTableView.setItems(FXCollections.observableArrayList(path.getPathStations()));
    }

    private void setPathBackStations(@NotNull PathDTO path) {
        LinkedList<PathStationDTO> pathStations = path.getPathStations();
        pathStations.sort(Comparator.comparingInt(PathStationDTO::getPositionOnPath));
        returnPathTableView.setItems(FXCollections.observableArrayList(pathStations));
    }

    private void initSetDragAndDropListeners(@NotNull TableView<PathStationDTO> tableView) {
        tableView.setOnDragEntered(Event::consume);
        tableView.setOnDragDropped(e -> {
            logger.debug("Drag Dropped");
            logger.debug(e.getGestureSource().toString());
            String string = e.getDragboard().getString();
            Object o = DragAndDropManager.get(string);
            try {
                if (o instanceof StationDTO) {  //FIXME DRAG AND DROP
                    logger.debug("Add Station to List");
                    Integer beforeOnPath;
                    try {
                        beforeOnPath = tableView.getItems().get(tableView.getItems().size() - 1).getPositionOnPath();
                    } catch (IndexOutOfBoundsException ex) {
                        logger.debug("BEFORE: NULL");
                        beforeOnPath = null;
                    }
                    PathDTO pathDTO = (tableView == outgoingPathTableView) ? wrapperPresenter.getOutgoingPath() : wrapperPresenter.getPathBackInfo();
                    boolean success = service.addStation(pathDTO, beforeOnPath, (StationDTO) o);
                    if (success) {
                        refresh();
                    } else {
                        logger.debug("Station could not be added");
                        NotificationCreator.createErrorNotification("Error", "Station could not be added");
                    }
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }
            //DragAndDropManager.remove(o);
            e.consume();
        });

        tableView.setOnDragOver(e -> {
            e.acceptTransferModes(TransferMode.ANY);
            e.consume();
        });
        tableView.setOnDragDone(e -> {
            logger.debug("Drag done");
            e.getDragboard().clear();
            e.setDropCompleted(true);
            e.consume();
        });
    }

    @FXML
    private void saveToPath() {
        logger.debug("UI CALL SAVE PATH TO");
        //wrapperPresenter.getOutgoingPath().save(wrapperPresenter.getCurrentRoute(), true);
    }

    @FXML
    private void saveBackPath() {
        logger.debug("UI CALL SAVE PATH BACK");
        //wrapperPresenter.getOutgoingPath().save(wrapperPresenter.getCurrentRoute(), false);
    }

    @FXML
    private void validateToPath() {
        logger.debug("UI CALL VALIDATE PATH TO");
        boolean validate = true;//wrapperPresenter.getOutgoingPath().validate();
        if (!validate) {
            pathNeedsTwoStationsMassage();
        }
    }

    @FXML
    private void validateBackPath() {
        logger.debug("UI CALL VALIDATE PATH BACK");
        boolean validate = true;//wrapperPresenter.getPathBackInfo().validate();
        if (!validate) {
            pathNeedsTwoStationsMassage();
        }
    }

    private void pathNeedsTwoStationsMassage() {
        logger.debug("Path needs at least 2 stations to be validated");
    }
}
