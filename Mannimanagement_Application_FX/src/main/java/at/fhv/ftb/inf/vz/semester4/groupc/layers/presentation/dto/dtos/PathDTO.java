package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos;

import java.util.LinkedList;

/**
 * PathDTO.
 */
public class PathDTO {

    private Integer pathId;
    private Integer routeId;
    private LinkedList<PathStationDTO> pathStations;
    private boolean isReturn;

    public PathDTO(Integer routeId, boolean isReturn) {
        this.routeId = routeId;
        this.isReturn = isReturn;
    }

    public PathDTO() {
    }

    public Integer getPathId() {
        return pathId;
    }

    public void setPathId(Integer pathId) {
        this.pathId = pathId;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public LinkedList<PathStationDTO> getPathStations() {
        return (pathStations != null) ? pathStations:new LinkedList<>();
    }

    public void setPathStations(LinkedList<PathStationDTO> pathStations) {
        this.pathStations = pathStations;
    }

    @Override
    public String toString() {
        return "PathDTO{" +
                "pathId=" + pathId +
                ", routeId=" + routeId +
                ", pathStations=" + pathStations.size() +
                '}';
    }

    public boolean isReturn() {
        return isReturn;
    }

    public void setReturn(boolean aReturn) {
        isReturn = aReturn;
    }
}
