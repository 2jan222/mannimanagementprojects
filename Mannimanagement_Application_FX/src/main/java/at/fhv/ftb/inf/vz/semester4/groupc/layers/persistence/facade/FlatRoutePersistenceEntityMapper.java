package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatRouteEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class FlatRoutePersistenceEntityMapper extends PersistenceEntityMapper<FlatRouteEntity> {
    @Override
    public FlatRouteEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(FlatRouteEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("FlatRouteEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<FlatRouteEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<FlatRouteEntity> query = session.createQuery("from FlatRouteEntity ", FlatRouteEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<FlatRouteEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<FlatRouteEntity> query = session.createQuery("from FlatRouteEntity " + whereQueryFragment, FlatRouteEntity.class);
            return query.getResultList();
        }
    }
}
