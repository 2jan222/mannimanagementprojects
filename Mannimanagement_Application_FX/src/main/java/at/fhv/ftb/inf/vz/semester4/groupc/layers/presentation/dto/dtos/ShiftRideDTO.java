package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos;

import org.jetbrains.annotations.TestOnly;

import java.time.LocalDate;
import java.time.LocalTime;

public class ShiftRideDTO {
    private Integer shiftRideId;
    private String startStationName;
    private String endStationName;
    private StationDTO startStation;
    private StationDTO endStation;
    private LocalTime startTime;
    private LocalTime endTime;
    private Integer operationId;
    private Integer operationShiftId;
    private LocalDate date;

    public ShiftRideDTO() {
    }

    @TestOnly
    public ShiftRideDTO(Integer shiftRideId, String startStationName, String endStationName, LocalTime startTime,
                        LocalTime endTime, Integer operationId, Integer operationShiftId, LocalDate date) {
        this.shiftRideId = shiftRideId;
        this.startStationName = startStationName;
        this.endStationName = endStationName;
        this.startTime = startTime;
        this.endTime = endTime;
        this.operationId = operationId;
        this.operationShiftId = operationShiftId;
        this.date = date;
    }

    public Integer getShiftRideId() {
        return shiftRideId;
    }

    public void setShiftRideId(Integer shiftRideId) {
        this.shiftRideId = shiftRideId;
    }

    public String getStartStationName() {
        return startStationName;
    }

    public void setStartStationName(String startStationName) {
        this.startStationName = startStationName;
    }

    public String getEndStationName() {
        return endStationName;
    }

    public void setEndStationName(String endStationName) {
        this.endStationName = endStationName;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    public Integer getOperationShiftId() {
        return operationShiftId;
    }

    public void setOperationShiftId(Integer operationShiftId) {
        this.operationShiftId = operationShiftId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "ShiftRideDTO{" +
                "shiftRideId=" + shiftRideId +
                ", startStationName='" + startStationName + '\'' +
                ", endStationName='" + endStationName + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", operationId=" + operationId +
                ", operationShiftId=" + operationShiftId +
                ", date=" + date +
                '}';
    }

    public StationDTO getStartStation() {
        return startStation;
    }

    public void setStartStation(StationDTO startStation) {
        this.startStation = startStation;
    }

    public StationDTO getEndStation() {
        return endStation;
    }

    public void setEndStation(StationDTO endStation) {
        this.endStation = endStation;
    }
}
