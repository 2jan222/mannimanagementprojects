package at.fhv.ftb.inf.vz.semester4;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.ApplicationStart;

public class CoverageStarter {
    public static void main(String[] args) {
        ApplicationStart.main(args);
    }
}
