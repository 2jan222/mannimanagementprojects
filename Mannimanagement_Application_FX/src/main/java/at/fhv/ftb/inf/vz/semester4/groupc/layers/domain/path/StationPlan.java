package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.path;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Station;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.StationConnector;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.LoggerableColor;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.TestOnly;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Graph of Stations.
 */
public class StationPlan {
    private static final Loggerable logger = Loggerable.getInstance();

    private static HashMap<Integer, Station> stationById = new HashMap<>();
    private static HashMap<Station, Integer> lookupStation2Id = new HashMap<>();
    private static HashMap<Integer, LinkedList<Pair<Integer, StationConnector>>> graph = new HashMap<>();

    public static void addStation(@NotNull Station station) {
        if (!stationById.containsKey(station.getStationId())) {
            int lookupId = addStationToLookup(station);
            graph.put(lookupId, new LinkedList<>());
        }
    }

    public static boolean addStationPath(@NotNull StationConnector connection, boolean addMissing) {
        Station start = connection.getStart();
        Station end = connection.getEnd();

        if (start != null && !stationById.containsKey(start.getStationId())) {
            if (addMissing) {
                addStation(start);
            } else {
                return false;
            }
        }
        if (!stationById.containsKey(end.getStationId())) {
            if (addMissing) {
                addStation(end);
            } else {
                return false;
            }
        }
        int lookupEnd = end.getStationId();
        if (start != null && stationById.containsKey(lookupEnd) && stationById.containsKey(start.getStationId())) {
            Pair<Integer, StationConnector> pair = new Pair<>(lookupEnd, connection);
            int lookupStart = start.getStationId();
            if (lookupStart >= 0 && graph.containsKey(lookupStart)) {
                LinkedList<Pair<Integer, StationConnector>> pairs = graph.get(lookupStart);
                if (pairs == null) {
                    pairs = new LinkedList<>();
                }
                pairs.addLast(pair);
            }
            return true;
        } else {
            logger.warn("Connection was not added", LoggerableColor.ANSI_YELLOW);
            return false;
        }

    }

    @SuppressWarnings("WeakerAccess")
    @NotNull
    public static LinkedList<Pair<Station, StationConnector>> getNeighbourStationWithConnections(Integer stationId) {
        if (stationId < 0) {
            return new LinkedList<>();
        } else {
            return graph.get(stationId)
                    .stream()
                    .map(e -> new Pair<>(lookup(e.getKey()), e.getValue()))
                    .collect(Collectors.toCollection(LinkedList::new));
        }
    }

    @NotNull
    public static LinkedList<Station> getNeighbourStations(Station station) {
        int lookup = lookup(station);
        if (lookup < 0) {
            return new LinkedList<>();
        } else {
            return (LinkedList<Station>) graph.get(lookup)
                    .stream()
                    .map(e -> lookup(e.getKey()))
                    .collect(Collectors.toList());
        }
    }

    @NotNull
    public static LinkedList<Station> getAllStations() {
        return new LinkedList<>(stationById.values());
    }

    public static LinkedList<StationConnector> getAllConnections() {
        LinkedList<StationConnector> all = new LinkedList<>();
        graph.values().forEach(list -> list.forEach(e -> {
            if (!all.contains(e.getValue())) {
                all.add(e.getValue());
            }
        }));
        return all;
    }

    private static int lookup(@NotNull Station station) {
        Integer integer = lookupStation2Id.get(station);
        return (integer != null) ? integer : -1;
    }

    @Nullable
    private static Station lookup(int id) {
        return stationById.get(id);
    }

    private static int addStationToLookup(@NotNull Station station) {
        int id = station.getStationId();
        if (!lookupStation2Id.containsKey(station) && !stationById.containsValue(station)) {
            //SECOND BOOLEAN EXPRESSION CAN BE OMITTED. KEEP FOR FAIL SAVE.
            stationById.put(id, station);
            lookupStation2Id.put(station, id);
        }
        return id;
    }

    private static void removeStationFromLookUp(Station station) {
        stationById.remove(lookupStation2Id.remove(station));
    }


    @SuppressWarnings("WeakerAccess")
    public static LinkedList<StationConnector> getConnectionsBetween(Integer startId, Integer endId) {
        LinkedList<Pair<Station, StationConnector>> withConnections = getNeighbourStationWithConnections(startId);
        LinkedList<StationConnector> stationConnectors = new LinkedList<>();
        withConnections.forEach(e -> {
            if (Objects.equals(e.getKey().getStationId(), endId)) {
                stationConnectors.add(e.getValue());
            }
        });
        return stationConnectors;
    }

    @Contract(pure = true)
    @TestOnly
    StationPlan() {
    }

    public static void printGraph() {
        lookupStation2Id.keySet().stream().sorted(Comparator.comparing(Station::getStationId)).forEach(e -> {
            System.out.println("├─" +e.getStationName());
            LinkedList<Pair<Integer, StationConnector>> list = graph.get(lookupStation2Id.get(e));
            list.forEach(k -> System.out.println("│\t " +  ((list.size() - 1 != list.indexOf(k)) ? "├─":"└─") + k.getValue().getEnd().getStationName()));
        });
    }

    public static boolean doesStationExist(int staionId) {
        return stationById.containsKey(staionId);
    }

    public static Station getStationById(Integer stationId) {
        return stationById.get(stationId);
    }


    /**
     * Container for generic objects.
     * @param <K> type k known as key
     * @param <V> type v known as value
     */
    public static class Pair<K, V> {
        private K key;
        private V value;

        @Contract(pure = true)
        public Pair(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "Pair{" +
                    "key=" + key +
                    ", value=" + value +
                    '}';
        }
    }
}
