package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.tour;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.PersistenceFacade;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.OperationEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteRideEntityFast;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.Operation;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.Route;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.security.IRoute;
import at.fhv.ftb.inf.vz.semester4.groupd.persistance.CreateOperationFacade;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import org.hibernate.Session;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.github.jan222ik.LoggerableColor.ANSI_PURPLE;

@LoggerableClassDisplayName(value = "[CreateOperationFacadeImpl]", color = ANSI_PURPLE)
public class CreateOperationFacadeImpl implements CreateOperationFacade {
    private static final Loggerable logger = Loggerable.getInstance();
    private static final PersistenceFacade db = PersistenceFacade.getInstance();
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Override
    public List<? extends IRoute> getValidRoutesByMonth(int month) {
        logger.debug("Get Valid Routes By Month", ANSI_PURPLE);
        String where = "WHERE '" + month + "' BETWEEN month(validFrom) AND month(validTo)";
        List<RouteEntity> routeEntities = db.getAllWhere(RouteEntity.class, where);
        //routeEntities.forEach(logger::debug);
        return routeEntities.stream().map(Route::new).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<? extends IRoute> getValidRoutesByDay(@NotNull LocalDate date) {
        logger.debug("Get Valid Routes By Day", ANSI_PURPLE);
        String where = "WHERE '" + date.format(formatter) + "' BETWEEN validFrom AND validTo";
        List<RouteEntity> allWhere = db.getAllWhere(RouteEntity.class, where);
        logger.debug("Elements found:(" + allWhere.size() + ")");
        return allWhere.stream().map(Route::new).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<Operation> getOperationsByDate(@NotNull Date valueOf) {
        logger.debug("Get Operations By Date", ANSI_PURPLE);
        String where = "WHERE date = '" + valueOf.toLocalDate().format(formatter) + "'";
        List<OperationEntity> allWhere = db.getAllWhere(OperationEntity.class, where);
        logger.debug("Elements found:(" + allWhere.size() + ")");
        return allWhere.stream().map(Operation::new).collect(Collectors.toCollection(LinkedList::new));
        //return new LinkedList<>();
    }

    @Override
    public List<Operation> getOperationsByMonth(int month) {
        logger.debug("Get Operations By Month", ANSI_PURPLE);
        String where = "WHERE '" + month + "' = month(date)";
        List<OperationEntity> operationEntities = db.getAllWhere(OperationEntity.class, where);
        return operationEntities.stream().map(Operation::new).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public void deleteOperation(@NotNull Operation oper) {
        logger.debug("Delete Operation", ANSI_PURPLE);
        db.executeTransaction(session -> {
            //session.delete(oper.getCapsuledEntity());
            oper.getCapsuledEntity().getRouteRidesByOperationId().forEach(
                    e -> {
                        Integer id = (Integer) session.createSQLQuery(
                                "DELETE FROM operation_shift_ride WHERE operation_ride_id = '" + e.getRouteRideId() + "' RETURNING operation_shift_id"
                        ).uniqueResult();
                        session.createSQLQuery("DELETE FROM operation_shift WHERE operation_shift_id = " + ndls(id)).executeUpdate();
                    }
            );
            session.createSQLQuery("DELETE FROM operation_ride WHERE operation_id = '" + oper.getCapsuledEntity().getOperationId() + "';" +
                    " DELETE FROM operation WHERE operation_id = '" + oper.getCapsuledEntity().getOperationId() + "'"
            ).executeUpdate();
            return null;
        });
    }

    @Override
    public void deleteOperations(@NotNull List<Operation> deleteOperation) {
        logger.debug("Delete Operations[" + deleteOperation.size() + "]", ANSI_PURPLE);
        deleteOperation.forEach(this::deleteOperation);
    }

    @Override
    public void saveOperation(@NotNull Operation oper) {        //TODO
        logger.debug("Save Operation", ANSI_PURPLE);
        Object read = db.read(oper.getCapsuledEntity().getOperationId(), OperationEntity.class);
        if (read != null) {
            logger.debug("EXISTS");
            db.executeTransaction(session -> {
                session.createSQLQuery(
                        "UPDATE operation " +
                                "SET date = " + ndls(oper.getCapsuledEntity().getDate().format(formatter)) + "," +
                                " bus_id = " + ndls(oper.getCapsuledEntity().getBusId())
                ).executeUpdate();
                Set<RouteRideEntityFast> rides = oper.getCapsuledEntity().getRouteRidesByOperationId();
                if (rides != null) {
                    rides.forEach(e -> createOrUpdateRide(e, session, ((OperationEntity) read).getOperationId()));
                }
                System.out.println("oper = " + oper.getCapsuledEntity());
                //oper.setStatus(ChangeStatus.OK);
                return null;
            });
        } else {
            logger.debug("NOT EXISTENT");
            db.executeTransaction(session -> {
                Integer id = (Integer) session.createSQLQuery(
                        "INSERT INTO operation(operation_id, date, bus_id) " +
                                "VALUES (DEFAULT, "
                                + ndls(oper.getCapsuledEntity().getDate().format(formatter)) + ", "
                                + ndls(oper.getCapsuledEntity().getBusId()) + ") RETURNING operation_id").uniqueResult();
                Set<RouteRideEntityFast> rides = oper.getCapsuledEntity().getRouteRidesByOperationId();
                if (rides != null) {
                    rides.forEach(e -> {
                        Integer orUpdateRide = createOrUpdateRide(e, session, id);
                        e.setRouteRideId(orUpdateRide);     //TODO ARE SAME ID ?
                    });
                }
                oper.getCapsuledEntity().setOperationId(id);

                System.out.println("oper = " + oper.getCapsuledEntity());
                //oper.setStatus(ChangeStatus.OK);
                return null;
            });
        }
    }

    public Integer createOrUpdateRide(RouteRideEntityFast entity, Session session, Integer operationID) {
        if (entity.getRouteRideId() != null) {
            return (Integer) session.createSQLQuery(
                    "INSERT INTO operation_ride(operation_ride_id, operation_id, route_ride_id) " +
                            "VALUES(DEFAULT, '" + operationID + "','" + entity.getRouteRideId() + "') " +
                            "RETURNING operation_ride_id").uniqueResult();
        } else {
            return (Integer) session.createSQLQuery(
                    "UPDATE operation_ride SET operation_id = " + ndls(operationID) + "," +
                            " route_ride_id = " + ndls(entity.getRouteRideId()) + " WHERE  operation_ride_id = " + ndls(entity.getRouteRideId()) +
                            " RETURNING operation_ride_id").uniqueResult();
        }
    }

    @Override
    public void saveOperations(@NotNull List<Operation> newOperations) {
        logger.debug("Save Operations[" + newOperations.size() + "]", ANSI_PURPLE);
        newOperations.forEach(this::saveOperation);
    }

    /*
    @NotNull
    @Contract("_ -> new")
    private DatePeriod getMonthsPeriod(int month) {
        LocalDate now = LocalDate.now();
        LocalDate monthInCurrentYear = LocalDate.of(now.getYear(), month, 1);
        LocalDate start;
        LocalDate end;
        if (now.isAfter(monthInCurrentYear)) {
            if (now.getMonth() == monthInCurrentYear.getMonth()) {
                start = now;
            } else {
                start = monthInCurrentYear;
            }
        } else {
            if (now.getMonth() == monthInCurrentYear.getMonth()) {
                start = now.plusYears(1);
            } else {
                start = monthInCurrentYear.plusYears(1);
            }
        }
        end = LocalDate.of(start.getYear(), start.getMonth(), start.getMonth().length(start.isLeapYear()));
        return new DatePeriod(start, end);
    }

    private class DatePeriod {
        LocalDate start;
        LocalDate end;

        @Contract(pure = true)
        public DatePeriod(LocalDate start, LocalDate end) {
            this.start = start;
            this.end = end;
        }
    }

     */

    @NotNull
    @Contract(pure = true)
    private String ndls(Object o) {
        return (o != null) ? "'" + o + "'" : "null";
    }
}
