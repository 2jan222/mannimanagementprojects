package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.util;

import org.jetbrains.annotations.Contract;

import java.util.HashSet;
import java.util.List;
import java.util.function.Consumer;

/**
 * Arg Parser.
 *
 * @author Janik Mayr
 */
public class ArgsParser {
    private static HashSet<String> parsedArgs = new HashSet<>();

    /**
     * Executes code when argument is known to parser.
     *
     * @param argKeyword keyword to search in arguments for.
     * @param consumer for keyword.
     */
    public static void checkForKeyword(String argKeyword, Consumer<String> consumer) {
        if (parsedArgs.contains(argKeyword)) {
            consumer.accept(argKeyword);
        }
    }


    /**
     * Checks if argument is known to parser.
     * @param argKeyword keyword to search in arguments for.
     * @return true, if parser contains argument.
     */
    @Contract(pure = true)
    public static boolean containsKeyword(String argKeyword) {
        return parsedArgs.contains(argKeyword);
    }

    /**
     * Pareses all arguments and stores them.
     *
     * @param args to parse.
     */
    public static void parseArgs(List<String> args) {
        parsedArgs.addAll(args);
    }

}

