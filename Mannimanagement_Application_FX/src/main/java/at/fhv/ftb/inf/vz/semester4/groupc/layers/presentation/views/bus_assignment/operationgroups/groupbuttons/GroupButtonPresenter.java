package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.bus_assignment.operationgroups.groupbuttons;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.bus_assignment.operationgroups.GroupPlaneButtonPair;
import de.jensd.fx.glyphs.control.GlyphCheckBox;
import javafx.fxml.FXML;

/**
 * GroupButtonPresenter.
 * Presenter of the button to show or hide a group
 */
public class GroupButtonPresenter {
    @FXML
    private GlyphCheckBox checkBox;

    private GroupPlaneButtonPair pairOf;


    public void setCheckBoxText(String groupName) {
        checkBox.setText(groupName);
    }

    public void hideToggle() {
        pairOf.hideToggle(true);
    }

    public void setPlaneButtonPair(GroupPlaneButtonPair groupPlaneButtonPair) {
        pairOf = groupPlaneButtonPair;
    }

    public void toggleCheckBox() {
        checkBox.setSelected(!checkBox.isSelected());
    }

    public void update() {
        setCheckBoxText(pairOf.getGroupName());
    }
}
