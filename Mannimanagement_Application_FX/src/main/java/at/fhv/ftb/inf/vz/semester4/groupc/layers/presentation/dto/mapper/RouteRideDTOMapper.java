package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.RouteRide;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.RouteRideDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;


/**
 * RouteRideDTO mapper.
 */
@Mapper(uses = {BusDTOMapper.class, PathStationDTOMapper.class})
public interface RouteRideDTOMapper {

    RouteRideDTOMapper INSTANCE = Mappers.getMapper(RouteRideDTOMapper.class);

    @Mapping(target = "pathStations", source = "pathStations")
    @Mapping(target = "startTime", source = "startTime")
    @Mapping(target = "routeNumber", source = "routeNumber")
    @Mapping(target = "id", source = "routeRideId")
    RouteRideDTO toDTO(RouteRide routeRide);

    @Mapping(target = "pathStations", source = "pathStations")
    @Mapping(target = "startTime", source = "startTime")
    @Mapping(target = "routeNumber", source = "routeNumber")
    @Mapping(target = "startTimeId", ignore = true)
    @Mapping(target = "routeRideId", source = "id")
    RouteRide toMojo(RouteRideDTO routeRideDTO);

    @Mapping(target = "pathStations", source = "pathStations")
    @Mapping(target = "startTime", source = "startTime")
    @Mapping(target = "routeNumber", source = "routeNumber")
    @Mapping(target = "id", source = "routeRideId")
    void updateDTO(RouteRide routeRide, @MappingTarget RouteRideDTO routeRideDTO);

    @Mapping(target = "pathStations", source = "pathStations")
    @Mapping(target = "startTime", source = "startTime")
    @Mapping(target = "routeNumber", source = "routeNumber")
    @Mapping(target = "startTimeId", ignore = true)
    @Mapping(target = "routeRideId", source = "id")
    void updateMojo(RouteRideDTO routeRideDTO, @MappingTarget RouteRide routeRide);

    List<RouteRideDTO> toDTos(List<RouteRide> routeRides);

    List<RouteRide> toMojos(List<RouteRideDTO> routeRideDTOS);


   /* @Named("endTimeRoute")
    default LocalTime endTimeMapping(RouteRide mojo) {
        LinkedList<PathStation> pathStations = mojo.getPathStations();
        long l = 0;
        for (PathStation station : pathStations) {
            l = l + station.getConnection().getTimeFromPrevious();
        }
        return mojo.getStartTime().plus(l, ChronoUnit.SECONDS);

    }*/

}
