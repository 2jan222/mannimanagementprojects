package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;

public class ShiftEntryDTO {
    private Integer shiftEntryId;
    private LocalDate date;
    private LocalTime startTime;
    private LocalTime endTime;
    private Integer operationId;
    private Integer driverId;
    private Integer busId;
    private LinkedList<ShiftRideDTO> shiftRides;

    public ShiftEntryDTO() {
    }

    public ShiftEntryDTO(Integer shiftEntryId, LocalDate date, LocalTime startTime, LocalTime endTime, Integer operationId,
                         LinkedList<ShiftRideDTO> shiftRides) {
        this.shiftEntryId = shiftEntryId;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.operationId = operationId;
        this.shiftRides = shiftRides;
    }

    public Integer getShiftEntryId() {
        return shiftEntryId;
    }

    public void setShiftEntryId(Integer shiftEntryId) {
        this.shiftEntryId = shiftEntryId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    public LinkedList<ShiftRideDTO> getShiftRides() {
        return shiftRides;
    }

    public void setShiftRides(LinkedList<ShiftRideDTO> shiftRides) {
        this.shiftRides = shiftRides;
    }

    @Override
    public String toString() {
        return "ShiftEntryDTO{" +
                "shiftEntryId=" + shiftEntryId +
                ", date=" + date +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", operationId=" + operationId +
                ", driverId=" + driverId +
                ", busId=" + busId +
                ", shiftRides=" + shiftRides +
                '}';
    }
}
