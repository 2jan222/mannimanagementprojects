package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteRideEntityFast;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class RouteRideFastPersistenceEntityMapper extends PersistenceEntityMapper<RouteRideEntityFast> {
    @Override
    public RouteRideEntityFast read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(RouteRideEntityFast.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("RouteRideEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<RouteRideEntityFast> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<RouteRideEntityFast> query = session.createQuery("from RouteRideEntityFast ", RouteRideEntityFast.class);
            return query.getResultList();
        }
    }

    @Override
    public List<RouteRideEntityFast> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<RouteRideEntityFast> query = session.createQuery("from RouteRideEntityFast " + whereQueryFragment, RouteRideEntityFast.class);
            return query.getResultList();
        }
    }
}
