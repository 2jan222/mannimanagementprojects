package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Table(name = "suspended", schema = "public", catalog = "kmobil")
public class SuspendedEntity implements DatabaseEntityMarker {
    private Integer suspendedId;
    private Integer busId;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private String cause;
    private BusEntity busByBusId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "suspended_id")
    public Integer getSuspendedId() {
        return suspendedId;
    }

    public void setSuspendedId(Integer suspendedId) {
        this.suspendedId = suspendedId;
    }

    @Basic
    @Column(name = "bus_id")
    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    @Basic
    @Column(name = "date_from")
    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    @Basic
    @Column(name = "date_to")
    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    @Basic
    @Column(name = "cause")
    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SuspendedEntity that = (SuspendedEntity) o;
        return suspendedId == that.suspendedId &&
                busId == that.busId &&
                Objects.equals(dateFrom, that.dateFrom) &&
                Objects.equals(dateTo, that.dateTo) &&
                Objects.equals(cause, that.cause);
    }

    @Override
    public int hashCode() {
        return Objects.hash(suspendedId, busId, dateFrom, dateTo, cause);
    }

    @ManyToOne
    @JoinColumn(name = "bus_id", referencedColumnName = "bus_id", nullable = false, insertable = false, updatable = false)
    public BusEntity getBusByBusId() {
        return busByBusId;
    }

    public void setBusByBusId(BusEntity busByBusId) {
        this.busByBusId = busByBusId;
    }

    @Override
    public String toString() {
        return "SuspendedEntity{" +
                "suspendedId=" + suspendedId +
                ", busId=" + busId +
                ", dateFrom=" + dateFrom +
                ", dateTo=" + dateTo +
                ", cause='" + cause + '\'' +
                ", busByBusId=" + busByBusId +
                '}';
    }
}
