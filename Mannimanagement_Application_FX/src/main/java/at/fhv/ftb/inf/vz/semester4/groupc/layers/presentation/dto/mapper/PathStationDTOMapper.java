package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.PathStation;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.PathStationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * PathStationDTO mapper.
 */
@Mapper(uses = StationConnectorDTOMapper.class)
public interface PathStationDTOMapper {
    PathStationDTOMapper INSTANCE = Mappers.getMapper(PathStationDTOMapper.class);

    @Mapping(target = "connection", source = "connection")
    PathStationDTO toDTO(PathStation pathStation);

    @Mapping(target = "connection", source = "connection")
    PathStation toMojo(PathStationDTO pathStationDTO);

    @Mapping(target = "connection", source = "connection")
    void updateDTO(PathStation pathStation, @MappingTarget PathStationDTO pathStationDTO);

    @Mapping(target = "connection", source = "connection")
    void updateMojo(PathStationDTO pathStationDTO, @MappingTarget PathStation pathStation);

    List<PathStationDTO> toDTOs(List<PathStation> pathStations);

    List<PathStation> toMojos(List<PathStationDTO> pathStationDTOS);
}
