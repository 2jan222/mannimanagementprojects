package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Objects;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Table(name = "path_station", schema = "public", catalog = "kmobil")
public class PathStationEntity implements DatabaseEntityMarker {
    private Integer pathStationId;
    private Integer pathId;
    private Integer stationConnectorId;
    private Integer positionOnPath;
    private PathEntity pathByPathId;
    private StationConnectorEntity stationConnector;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "path_station_id")
    public Integer getPathStationId() {
        return pathStationId;
    }

    public void setPathStationId(Integer pathStationId) {
        this.pathStationId = pathStationId;
    }

    @Basic
    @Column(name = "position_on_path")
    public Integer getPositionOnPath() {
        return positionOnPath;
    }

    public void setPositionOnPath(Integer positionOnPath) {
        this.positionOnPath = positionOnPath;
    }


    @Column(name = "path_id")
    public Integer getPathId() {
        return pathId;
    }

    public void setPathId(Integer pathId) {
        this.pathId = pathId;
    }

    @Column(name = "station_connection_id")
    public Integer getStationConnectorId() {
        return stationConnectorId;
    }

    public void setStationConnectorId(Integer stationConnectorId) {
        this.stationConnectorId = stationConnectorId;
    }


    @ManyToOne
    @JoinColumn(name = "path_id", referencedColumnName = "path_id", nullable = false, insertable = false, updatable = false)
    public PathEntity getPathByPathId() {
        return pathByPathId;
    }

    public void setPathByPathId(PathEntity pathByPathId) {
        this.pathByPathId = pathByPathId;
    }

    @Override
    public String toString() {
        return "PathStationEntity{" +
                "pathStationId=" + pathStationId +
                ", pathId=" + pathId +
                ", stationConnectorId=" + stationConnectorId +
                ", positionOnPath=" + positionOnPath +
                ", pathByPathId=" + pathByPathId +
                ", stationConnector=" + stationConnector +
                '}';
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "station_connection_id", nullable = false, insertable = false, updatable = false)
    public StationConnectorEntity getStationConnector() {
        return stationConnector;
    }

    public void setStationConnector(StationConnectorEntity stationConnector) {
        this.stationConnector = stationConnector;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PathStationEntity that = (PathStationEntity) o;
        return Objects.equals(pathStationId, that.pathStationId) &&
                Objects.equals(pathId, that.pathId) &&
                Objects.equals(stationConnectorId, that.stationConnectorId) &&
                Objects.equals(positionOnPath, that.positionOnPath) &&
                Objects.equals(stationConnector, that.stationConnector);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pathStationId, pathId, stationConnectorId, positionOnPath, stationConnector);
    }
}
