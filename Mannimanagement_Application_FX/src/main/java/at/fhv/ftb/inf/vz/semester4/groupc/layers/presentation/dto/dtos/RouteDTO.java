package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos;

import java.time.LocalDate;
import java.util.Set;

/**
 * RouteDTO.
 */
public class RouteDTO {
    private Integer routeId;
    private Integer routeNumber;
    private LocalDate validFrom;
    private LocalDate validTo;
    private String variation;
    private Set<PathDTO> paths;
    private Set<RouteRideDTO> routeRides;

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public Integer getRouteNumber() {
        return routeNumber;
    }

    public void setRouteNumber(Integer routeNumber) {
        this.routeNumber = routeNumber;
    }

    public LocalDate getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDate validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDate getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDate validTo) {
        this.validTo = validTo;
    }

    public String getVariation() {
        return variation;
    }

    public void setVariation(String variation) {
        this.variation = variation;
    }

    public Set<PathDTO> getPaths() {
        return paths;
    }

    public void setPaths(Set<PathDTO> paths) {
        this.paths = paths;
    }

    public Set<RouteRideDTO> getRouteRides() {
        return routeRides;
    }

    public void setRouteRides(Set<RouteRideDTO> routeRides) {
        this.routeRides = routeRides;
    }

    @Override
    public String toString() {
        return "RouteDTO{" +
                "routeId=" + routeId +
                ", routeNumber=" + routeNumber +
                ", validFrom=" + validFrom +
                ", validTo=" + validTo +
                ", variation='" + variation + '\'' +
                ", paths=" + paths.size() +
                ", routeRides=" + routeRides.size() +
                '}';
    }
}
