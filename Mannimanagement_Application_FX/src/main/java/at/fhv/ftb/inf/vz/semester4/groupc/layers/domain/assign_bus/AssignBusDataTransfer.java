package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_bus;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Bus;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Operation;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.util.LinkedList;

public interface AssignBusDataTransfer {
    void removeBusFromOperation(Integer operationId);

    void assignBusToGroup(Integer busId, Integer operationId);

    LinkedList<Operation> getGroupsForDate(@NotNull LocalDate dateIn);

    LinkedList<Bus> getUngroupedBusesForDate(@NotNull LocalDate dateIn);

    LinkedList<LocalDate> getDatesWithOperations();
}
