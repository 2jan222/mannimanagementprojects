package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;
import org.jetbrains.annotations.Contract;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "bus", schema = "public", catalog = "kmobil")
public class FlatBusEntity implements DatabaseEntityMarker {
    private Integer busId;
    private Integer maintenanceKm;
    private String licenceNumber;
    private String make;
    private String model;
    private String note;
    private LocalDate registrationDate;
    private Integer seatPlaces;
    private Integer standPlaces;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bus_id")
    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    @Basic
    @Column(name = "maintenance_km")
    public Integer getMaintenanceKm() {
        return maintenanceKm;
    }

    public void setMaintenanceKm(Integer maintenanceKm) {
        this.maintenanceKm = maintenanceKm;
    }

    @Basic
    @Column(name = "licence_number")
    public String getLicenceNumber() {
        return licenceNumber;
    }

    public void setLicenceNumber(String licenceNumber) {
        this.licenceNumber = licenceNumber;
    }

    @Basic
    @Column(name = "make")
    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    @Basic
    @Column(name = "domain")
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Basic
    @Column(name = "note")
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Basic
    @Column(name = "registration_date")
    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Basic
    @Column(name = "seat_places")
    public Integer getSeatPlaces() {
        return seatPlaces;
    }

    public void setSeatPlaces(Integer seatPlaces) {
        this.seatPlaces = seatPlaces;
    }

    @Basic
    @Column(name = "stand_places")
    public Integer getStandPlaces() {
        return standPlaces;
    }

    public void setStandPlaces(Integer standPlaces) {
        this.standPlaces = standPlaces;
    }

    @Contract(value = "null -> false", pure = true)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlatBusEntity flatBusEntity = (FlatBusEntity) o;
        return Objects.equals(busId, flatBusEntity.busId) &&
                Objects.equals(maintenanceKm, flatBusEntity.maintenanceKm) &&
                Objects.equals(seatPlaces, flatBusEntity.seatPlaces) &&
                Objects.equals(standPlaces, flatBusEntity.standPlaces) &&
                Objects.equals(licenceNumber, flatBusEntity.licenceNumber) &&
                Objects.equals(make, flatBusEntity.make) &&
                Objects.equals(model, flatBusEntity.model) &&
                Objects.equals(note, flatBusEntity.note) &&
                Objects.equals(registrationDate, flatBusEntity.registrationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(busId, maintenanceKm, licenceNumber, make, model, note, registrationDate, seatPlaces, standPlaces);
    }

    @Override
    public String toString() {
        return "FlatBusEntity{" +
                "busId=" + busId +
                ", maintenanceKm=" + maintenanceKm +
                ", licenceNumber='" + licenceNumber + '\'' +
                ", make='" + make + '\'' +
                ", domain='" + model + '\'' +
                ", note='" + note + '\'' +
                ", registrationDate=" + registrationDate +
                ", seatPlaces=" + seatPlaces +
                ", standPlaces=" + standPlaces +
                '}';
    }
}
