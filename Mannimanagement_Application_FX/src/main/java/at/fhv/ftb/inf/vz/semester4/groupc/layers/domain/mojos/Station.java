package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos;


import org.jetbrains.annotations.Contract;

public class Station {
    private Integer stationId;
    private String stationName;
    private String shortName;

    @Contract(pure = true)
    public Station(Integer stationId, String stationName, String shortName) {
        this.stationId = stationId;
        this.stationName = stationName;
        this.shortName = shortName;
    }

    @Contract(pure = true)
    public Station() {
    }

    public Integer getStationId() {
        return stationId;
    }

    public void setStationId(Integer stationId) {
        this.stationId = stationId;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    public String toString() {
        return "Station{" +
                "stationId=" + stationId +
                ", stationName='" + stationName + '\'' +
                ", shortName='" + shortName + '\'' +
                '}';
    }
}
