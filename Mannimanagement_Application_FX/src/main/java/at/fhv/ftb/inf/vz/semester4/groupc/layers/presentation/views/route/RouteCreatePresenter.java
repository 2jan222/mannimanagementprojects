package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.route;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.application.route.RouteCreationService;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.util.CommonResourceGetter;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.control.Control;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.stage.Stage;

import javax.inject.Inject;
import java.time.LocalDate;

/**
 * RouteCreatePresenter.
 * Views a form to creat a new route
 */

@LoggerableClassDisplayName("[RouteCreatePresenter]")
public class RouteCreatePresenter {
    @FXML
    private FontAwesomeIconView dateFromErrorTriangle;
    @FXML
    private FontAwesomeIconView dateToErrorTriangle;
    @FXML
    private FontAwesomeIconView routeNumberErrorTriangle;
    @FXML
    private Label routeNumberError;
    @FXML
    private Label dateFromError;
    @FXML
    private Label dateToError;
    @FXML
    private TextField routeNumberTextField;
    @FXML
    private DatePicker dateToPicker;
    @FXML
    private DatePicker dateFromPicker;
    @Inject
    private Loggerable logger;
    @Inject
    private RouteCreationService routeCreationService;
    private Runnable onFin;

    @FXML
    public void saveAction(ActionEvent actionEvent) {
        LocalDate dateFrom = dateFromPicker.getValue();
        LocalDate dateTo = dateToPicker.getValue();
        String intString = routeNumberTextField.getText();
        String variation = null;
        if (intString.matches("^[0-9]+$")) {
            setError(routeNumberError, null);
        } else {
            setError(routeNumberError, "Field must only contain numbers or must not be empty");
        }
        if (dateFrom != null) {
            if (dateTo != null) {
                if (dateFrom.isBefore(dateTo)) {
                    setError(dateFromError, null);
                    setError(dateToError, null);
                    int routeNumber = Integer.parseInt(intString);
                    boolean numberInUse = routeCreationService.isNumberInUse(routeNumber, dateFrom, dateTo);
                    logger.debug("" + numberInUse);
                    if (numberInUse) {
                        setError(routeNumberError, "Number already in use in time frame");
                    } else {
                        persist(routeNumber, dateFrom, dateTo, variation);
                        Stage stage = (Stage) CommonResourceGetter.getWindowFromActionEvent(actionEvent);
                        stage.close();
                        Platform.runLater(onFin);
                    }
                } else {
                    setError(dateFromError, "Date must be before date to");
                    setError(dateToError, "Date must be after date from");
                }
            } else {
                setError(dateToError, "Date must not be empty");
            }
        } else {
            setError(dateFromError, "Date must not be empty");
            if (dateTo == null) {
                setError(dateToError, "Date must not be empty");
            }
        }
    }

    private void persist(int routeNumber, LocalDate dateFrom, LocalDate dateTo, String variation) {
        routeCreationService.persist(routeNumber, dateFrom, dateTo, variation);
    }

    private void setError(Label label, String tooltip) {
        if (tooltip == null) {
            label.setVisible(false);
            label.setTooltip(null);
        } else {
            label.setVisible(true);
            label.setTooltip(new Tooltip(tooltip));
            showTooltip(label, tooltip);
        }

    }

    public static void showTooltip(Control control, String tooltipText) {
        Stage owner = (Stage) CommonResourceGetter.getWindowFromNode(control);
        Point2D p = control.localToScene(0.0, 0.0);

        final Tooltip customTooltip = new Tooltip();
        customTooltip.setText(tooltipText);

        control.setTooltip(customTooltip);
        customTooltip.setAutoHide(true);

        customTooltip.show(owner, p.getX()
                + control.getScene().getX() + control.getScene().getWindow().getX() + 20, p.getY()
                + control.getScene().getY() + control.getScene().getWindow().getY());

    }


    public void setRunnableOnSuccessfulFinish(Runnable r) {
        onFin = r;
    }
}
