package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.PathStationEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteRideEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;

@Mapper(uses = {PathStationEntityMapper.class, BusEntityMapper.class})
public interface RouteRideEntityMapper {
    RouteRideEntityMapper INSTANCE = Mappers.getMapper(RouteRideEntityMapper.class);

    @Mapping(target = "bus", ignore = true)
    @Mapping(target = "busId", ignore = true)
    @Mapping(target = "endTime", source = "routeRideEntity", qualifiedByName = "endTimeMapping")
    @Mapping(target = "routeNumber", source = "routeByRouteId.routeNumber")
    //@Mapping(target = "bus", source = "busByBusId")
    @Mapping(target = "variation", source = "routeByRouteId.variation")
    @Mapping(target = "pathStations", source = "startTimeByStartTimeId.pathByPathId.pathStationsByPathId")
    @Mapping(target = "pathId", source = "startTimeByStartTimeId.pathId")
    @Mapping(target = "date", source = "routeRideDate")
    @Mapping(target = "requiredCapacity", source = "startTimeByStartTimeId.requiredCapacity")
    @Mapping(target = "startTime", source = "startTimeByStartTimeId.startTime")
    RouteRide toMojo(RouteRideEntity routeRideEntity);

    @Mapping(target = "operationShiftEntityByOperationShiftId", ignore = true)
    @Mapping(target = "routeByRouteId", ignore = true)
    @Mapping(target = "operationByOperationId", ignore = true)
    //@Mapping(target = "busByBusId", source = "bus")
    @Mapping(target = "startTimeId", ignore = true)
    @Mapping(target = "routeRideDate", source = "date")
    @Mapping(target = "startTimeByStartTimeId", ignore = true)
    RouteRideEntity toPojo(RouteRide routeRide);

    @Mapping(target = "bus", ignore = true)
    @Mapping(target = "endTime", source = "routeRideEntity",  qualifiedByName = "endTimeMapping")
    @Mapping(target = "routeNumber", source = "routeByRouteId.routeNumber")
    //@Mapping(target = "bus", source = "busByBusId")
    @Mapping(target = "busId", ignore = true)
    @Mapping(target = "variation", source = "routeByRouteId.variation")
    @Mapping(target = "pathStations", source = "startTimeByStartTimeId.pathByPathId.pathStationsByPathId")
    @Mapping(target = "pathId", source = "startTimeByStartTimeId.pathId")
    @Mapping(target = "date", source = "routeRideDate")
    @Mapping(target = "requiredCapacity", source = "startTimeByStartTimeId.requiredCapacity")
    @Mapping(target = "startTime", source = "startTimeByStartTimeId.startTime")
    void updateMojo(RouteRideEntity routeRideEntity, @MappingTarget RouteRide routeRide);


    @Mapping(target = "operationShiftEntityByOperationShiftId", ignore = true)
    @Mapping(target = "routeByRouteId", ignore = true)
    @Mapping(target = "operationByOperationId", ignore = true)
   // @Mapping(target = "busByBusId", source = "bus")
    @Mapping(target = "startTimeId", ignore = true)
    @Mapping(target = "routeRideDate", source = "date")
    @Mapping(target = "startTimeByStartTimeId", ignore = true)
    void updatePojo(RouteRide routeRide, @MappingTarget RouteRideEntity routeRideEntity);

    List<RouteRide> toMojos(List<RouteRideEntity> routeRideEntities);

    List<RouteRideEntity> toPojos(List<RouteRide> routeRides);

    @Named("endTimeRoute")
    default LocalTime endTimeMapping(RouteRideEntity mojo) {
        Set<PathStationEntity> pathStations = mojo.getStartTimeByStartTimeId().getPathByPathId().getPathStationsByPathId();
        long l = 0;
        for (PathStationEntity station : pathStations) {
            l = l + station.getStationConnector().getDuration();
        }
        return mojo.getStartTimeByStartTimeId().getStartTime().plus(l, ChronoUnit.SECONDS);

    }
}
