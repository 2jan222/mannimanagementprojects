package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.util;

import at.fhv.ftb.inf.vz.semester4.groupc.Unused;

/**
 * Used to view class context of stack.
 */
@Deprecated
@Unused
public class SecMag extends SecurityManager {

    public void inspect() {
        int i = 0;
        int maxDepth = 60;
        for (; i <= maxDepth && i < this.getClassContext().length; ++i) {
            Class aClass = this.getClassContext()[i];
            System.out.println(aClass);
        }

    }
}
