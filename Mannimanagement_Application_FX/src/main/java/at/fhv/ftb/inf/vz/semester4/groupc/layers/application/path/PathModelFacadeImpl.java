package at.fhv.ftb.inf.vz.semester4.groupc.layers.application.path;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Path;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.PathStation;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Station;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.StationConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.path.PathContextMediator;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.path.PathCreationContext;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.path.StationPlan;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.PathDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.StationDTO;
import com.github.jan222ik.Loggerable;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;

public class PathModelFacadeImpl implements PathModelFacade {
    private final static Loggerable logger = Loggerable.getInstance();
    private final PathContextMediator pathContexts = new PathContextMediator();

    @Override
    public PathDTO getOutgoingPath(Integer routeId) {
        PathDTO path = getContext(routeId).getOutwardPathDTO();
        return (path != null) ? path : new PathDTO(routeId, false);
    }

    @Override
    public PathDTO getReturnPath(Integer routeId) {
        PathDTO path = getContext(routeId).getReturnPathDTO();
        return (path != null) ? path : new PathDTO(routeId, true);
    }

    @Override
    public boolean addStationToOutgoingPath(Integer pathPositionBefore, StationDTO newStation, Integer routeId) {
        logger.debug("ADD STATION TO OUTGOING PATH");
        PathCreationContext context = getContext(routeId);
        logger.debug("GOT Context");
        int size = 0;
        try {
            size = context.getOutwardPath().getPathStations().size();
        } catch (Throwable t) {
            t.printStackTrace();
        }
        logger.debug("SIZE START = " + size);
        addStationToPath(context.getOutwardPath(), newStation, pathPositionBefore);
        logger.debug("SIZE AFTER: " + context.getOutwardPath().getPathStations().size());
        context.updateOutwardPath(context.getOutwardPath());
        logger.debug("SIZE AFTER UPDATE: " + context.getOutwardPath().getPathStations().size());
        return true;
    }

    @Override
    public boolean addStationToReturningPath(Integer pathPositionBefore, StationDTO newStation, Integer routeId) {
        logger.debug("ADD STATION TO RETURN PATH");
        PathCreationContext context = getContext(routeId);
        int size = context.getReturnPath().getPathStations().size();
        logger.debug("SIZE START = " + size);
        addStationToPath(context.getReturnPath(), newStation, pathPositionBefore);
        logger.debug("SIZE AFTER: " + context.getReturnPath().getPathStations().size());
        context.updateReturnPath(context.getReturnPath());
        logger.debug("SIZE AFTER UPDATE: " + context.getReturnPath().getPathStations().size());
        return true;
    }

    //TODO RETURN WARNINGS AND ERRORS
    public void addStationToPath(@NotNull Path path, @NotNull StationDTO stationDTO, Integer positionBefore) {  //TODO RESTRUCTURE TO Make modre sence from checks
        LinkedList<PathStation> pathStations = path.getPathStations();
        pathStations.sort(Comparator.comparingInt(PathStation::getPositionOnPath)); //TODO IS IT ALREADY SORTED WHEN SORTED AFTER DB FETCH
        int listUpdateIndex = pathStations.size();
        Station station = StationPlan.getStationById(stationDTO.getStationId());
        if (station != null) {
            if (positionBefore != null) {
                Optional<PathStation> first = pathStations.stream().filter(e -> Objects.equals(e.getPositionOnPath(), positionBefore)).findFirst();
                if (first.isPresent()) {
                    PathStation before = first.get();
                    PathStation after = (pathStations.indexOf(before) + 1 < pathStations.size()) ? pathStations.get(pathStations.indexOf(before) + 1) : null;
                    LinkedList<StationConnector> btwBeforeNew = StationPlan.getConnectionsBetween(before.getConnection().getEnd().getStationId(), station.getStationId());
                    if (after != null) {
                        //ITEM BETWEEN OTHERS
                        if (!btwBeforeNew.isEmpty()) {  //TODO deside to be strict or more lose
                            LinkedList<StationConnector> btwNewAfter = StationPlan.getConnectionsBetween(stationDTO.getStationId(), before.getConnection().getEnd().getStationId());
                            if (!btwNewAfter.isEmpty()) { //TODO deside to be strict or more lose
                                PathStation pathStation = new PathStation(path.getPathId(), positionBefore + 1, btwBeforeNew.getFirst());
                                after.setConnection(btwNewAfter.getFirst());
                                pathStations.add(pathStations.indexOf(before) + 1, pathStation);
                                listUpdateIndex = pathStations.indexOf(before);
                                logger.debug("ADDED Station in between");
                            } else {
                                logger.warn("No Connection between new station and after station");
                            }
                        } else {
                            logger.warn("No Connection between before station and new station");
                        }
                    } else /*INSERT last*/ {
                        if (!btwBeforeNew.isEmpty()) {
                            pathStations.addLast(new PathStation(path.getPathId(), before.getPositionOnPath() + 1, btwBeforeNew.getFirst()));
                            logger.debug("ADDED Station at last position");
                        } else {
                            logger.warn("No connection between before station and new station");
                        }
                    }
                } else {
                    logger.warn("Before was not found");
                }
            } else /*INSERT first*/ {

                if (pathStations.isEmpty()) {
                    pathStations.addFirst(new PathStation(path.getPathId(), 1, new StationConnector(null, station, 0, 0)));
                    logger.debug("ADDED Station to empty list");
                } else {
                    LinkedList<StationConnector> connectionsBetween = StationPlan.getConnectionsBetween(station.getStationId(), pathStations.getFirst().getConnection().getEnd().getStationId());
                    if (!connectionsBetween.isEmpty()) {
                        pathStations.addFirst(new PathStation(path.getPathId(), 1, new StationConnector(null, station, 0, 0)));
                        pathStations.get(1).setConnection(connectionsBetween.getFirst());
                        logger.debug("ADDED Station at first position");
                    } else {
                        logger.warn("No Connection between new station and after station");
                    }
                }
                listUpdateIndex = 1;
            }
            /*UPDATE FUTURE LIST INDICES*/
            for (int i = listUpdateIndex; i < pathStations.size(); i++) {
                PathStation item = pathStations.get(i);
                item.setPositionOnPath(item.getPositionOnPath() + 1);
            }
        } else {
            logger.warn("Station does not exist[id=" + stationDTO.getStationId() + "]");
        }
        path.setPathStations(pathStations);
    }


    @Override
    public void updateOutgoingPath(@NotNull Path pathFromStations) {
        if (pathFromStations.getRouteId() != null) {
            PathCreationContext context = getContext(pathFromStations.getRouteId());
            context.updateOutwardPath(pathFromStations);
        } else {
            logger.warn("Tried to update or create outgoing path, without a route id");
        }
    }

    @Override
    public void updateReturnPath(Path pathFromStations) {
        if (pathFromStations.getRouteId() != null) {
            PathCreationContext context = getContext(pathFromStations.getRouteId());
            context.updateReturnPath(pathFromStations);
        } else {
            logger.warn("Tried to update or create return path, without a route id");
        }
    }

    @Override
    public void delete(int posOnPath, int routeId, boolean isReture) {
        System.out.println("DELETE MODEL");
        PathCreationContext context = getContext(routeId);
        Path path = (isReture) ? context.getReturnPath() : context.getOutwardPath();
        if (path != null) {
            Optional<PathStation> before = path.getPathStations().stream().filter(e -> e.getPositionOnPath() == posOnPath - 1).findFirst();
            Optional<PathStation> toDelete = path.getPathStations().stream().filter(e -> e.getPositionOnPath() == posOnPath).findFirst();
            Optional<PathStation> after = path.getPathStations().stream().filter(e -> e.getPositionOnPath() == posOnPath + 1).findFirst();
            if (toDelete.isPresent()) {
                path.getPathStations().remove(toDelete.get());  //DELETE ELEMENT FROM LIST
                if (before.isPresent()) {
                    //Has before
                    if (after.isPresent()) {
                        Station delEndStation = toDelete.get().getConnection().getEnd();
                        try {
                            after.get().setConnection(StationPlan.getConnectionsBetween(delEndStation.getStationId(), after.get().getConnection().getStart().getStationId()).getFirst());
                            System.out.println("Success elements before and after");
                        } catch (NoSuchElementException e) {
                            System.out.println("Fail No Station Connector");
                        }
                    } else {
                        //IS LAST
                        System.out.println("Success has before");
                    }
                } else {
                    //ROOT
                    if (after.isPresent()) {
                        //Has after elements

                        StationConnector connection = after.get().getConnection();
                        connection.setStart(null);
                        connection.setDistanceFromPrevious(0);
                        connection.setTimeFromPrevious(0);
                        after.get().setConnection(connection);

                        System.out.println("Success first elements after");
                    } else {
                        //IS LAST ELEMENT
                        //NOTHING TO UPDATE
                        System.out.println("Success No After");
                    }
                }
            } else {
                //PosOnPath Does Not Exist
                System.out.println("FAIL");
            }
            path.getPathStations().forEach(e -> {
                if (e.getPositionOnPath() >= posOnPath) {
                    e.setPositionOnPath(e.getPositionOnPath() - 1);
                }
            });
            if (isReture) {
                context.updateReturnPath(path);
            } else {
                context.updateOutwardPath(path);
            }
        } else {
            logger.warn("Tried to delete from null path");
        }
    }

    private PathCreationContext getContext(int id) {
        PathCreationContext value = pathContexts.getValue(id);
        if (value == null) {
            value = new PathCreationContext(id);
            pathContexts.setValue(id, value);
        }
        return value;
    }
}
