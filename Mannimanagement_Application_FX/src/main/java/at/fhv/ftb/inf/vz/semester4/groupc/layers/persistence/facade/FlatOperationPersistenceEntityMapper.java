package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatOperationEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class FlatOperationPersistenceEntityMapper extends PersistenceEntityMapper<FlatOperationEntity> {


    @Override
    public FlatOperationEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(FlatOperationEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("FlatOperationEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<FlatOperationEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<FlatOperationEntity> query = session.createQuery("from FlatOperationEntity ", FlatOperationEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<FlatOperationEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<FlatOperationEntity> query = session.createQuery("from FlatOperationEntity " + whereQueryFragment, FlatOperationEntity.class);
            return query.getResultList();
        }
    }
}
