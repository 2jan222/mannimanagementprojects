package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.path;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Path;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Route;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.RouteEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.StationConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.StationConnectorEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.PersistenceFacade;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.PathEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.PathStationEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.StationConnectorEntity;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

@LoggerableClassDisplayName("[PathCreationDataTransfer]")
public class PathCreationDataTransferImpl implements PathCreationDataTransfer {
    private static final Loggerable logger = Loggerable.getInstance();
    private static final PersistenceFacade pf = PersistenceFacade.getInstance();
    private HashMap<Integer, RouteEntity> repo = new HashMap<>();

    @Override
    public Route getRouteById(Integer routeId) {
        logger.debug("GET ROUTE FROM DB[@id:" + routeId + "]");
        RouteEntity read = pf.read(routeId, RouteEntity.class);
        repo.put(read.getRouteId(), read);
        return RouteEntityMapper.INSTANCE.toMojo(read);
    }

    @Override
    public void updateOrCreate(@NotNull Route route) {
        logger.debug("UPDATE ROUTE TO DB[@id:" + route.getRouteId() +"]");
        RouteEntity routeEntity = repo.get(route.getRouteId());
        System.out.println("routeEntity = " + routeEntity);
        System.out.println("route = " + route);
        if (routeEntity == null) {
            routeEntity = RouteEntityMapper.INSTANCE.toPojo(route);
        } else {
            RouteEntityMapper.INSTANCE.updatePojo(route, routeEntity);
            System.out.println("Update ENTITY");
        }
        /*
        route.getPaths().forEach(e -> {
            PathEntity pathEntity = new PathEntity();
            pathEntity.setRetour(e.isReturn());
            pathEntity.setRouteId(e.getRouteId());
            pathEntity.setPathId(e.getPathId());
            pf.update(pathEntity);
            e.getPathStations().forEach(k ->
            {
                Integer id = createUpdateConnector(k.getConnection());
                k.setConnectionId(id);
                PathStationEntity kPo = PathStationEntityMapper.INSTANCE.toPojo(k);
                System.out.println("PATH ID:" + pathEntity.getPathId());
                kPo.setPathId(pathEntity.getPathId());
                pf.update(kPo);
            });
            System.out.println("PATH E:" + e);
            PathEntityMapper.INSTANCE.updatePojo(e, pathEntity);
            pf.update(pathEntity);
        });
*/

        pf.update(routeEntity);
        /*
        pf.executeTransaction(new Function<Session, Object>() {
            @Override
            public Object apply(Session session) {
                route.getPaths().forEach(e -> {
                    PathEntity pathEntity = new PathEntity();
                    pathEntity.setRetour(e.isReturn());
                    pathEntity.setRouteId(e.getRouteId());
                    pathEntity.setPathId(e.getPathId());
                    pf.update(pathEntity);
                    System.out.println("path " + pathEntity);
                    e.getPathStations().forEach(k ->
                    {
                        Integer id = createUpdateConnector(k.getConnection());
                        k.setConnectionId(id);
                        PathStationEntity kPo = PathStationEntityMapper.INSTANCE.toPojo(k);
                        kPo.setPathId(pathEntity.getPathId());
                        if (kPo.getStationConnectorId() == null) {
                            pf.update(kPo);
                        }
                    });
                    //PathEntityMapper.INSTANCE.updatePojo(e, pathEntity);
                    //pf.update(pathEntity);
                });


                return null;
            }
        });*/
        System.out.println("ROUTE-ENTITY: " + routeEntity);
        System.out.println("PATH-ENTITIES: ");
        for (PathEntity e : routeEntity.getPathsByRouteId()) {
            e.setRouteId(routeEntity.getRouteId());
            System.out.println(e);
            pf.update(e);
            System.out.println("PATH-STATION-ENTITIES: ");
            for (PathStationEntity pathStationEntity : e.getPathStationsByPathId()) {
                StationConnectorEntity connector = pathStationEntity.getStationConnector();
                System.out.println("STATION-CONNECTOR: " + connector);
                if (connector.getStationConnectorId() == null) {
                    pf.update(connector);
                }
                pathStationEntity.setStationConnectorId(connector.getStationConnectorId());
                pathStationEntity.setPathId(e.getPathId());
                System.out.println(pathStationEntity);
                if (pathStationEntity.getPathStationId() != null) {
                    System.out.println("UPDATE EXISTING PATHSTATION");
                    String set = "set path_id = " + pathStationEntity.getPathId() + ", position_on_path = " + pathStationEntity.getPositionOnPath() + ", station_connection_id = " + pathStationEntity.getStationConnectorId() + " ";
                    String where = "where path_station_id = " + pathStationEntity.getPathStationId();
                    pf.updateWithFragments(PathStationEntity.class, set, where);
                } else {
                    pf.executeTransaction(session -> {
                        session.createSQLQuery("INSERT INTO path_station (station_connection_id, position_on_path, path_id) " +
                                "VALUES (" + pathStationEntity.getStationConnectorId() +
                                ", " + pathStationEntity.getPositionOnPath() +
                                "," + pathStationEntity.getPathId() +
                                ")");
                        return null;
                    });
                }
            }
        }
        RouteEntityMapper.INSTANCE.updateMojo(routeEntity, route);
        repo.put(routeEntity.getRouteId(), routeEntity);
    }

    @Override
    public void deletePath(@NotNull Path toDelete) {
        logger.debug("DELETE ROUTE TO DB[@id:" + toDelete.getRouteId() +"]");
        RouteEntity routeEntity = repo.get(toDelete.getRouteId());
        if (routeEntity == null) {
            System.out.println("TRIED TO DELETE NOT EXISTING PATH");
        } else {
            LinkedHashSet<PathEntity> collect = routeEntity.getPathsByRouteId()
                    .stream()
                    .filter(pathEntity -> pathEntity.getRetour() == toDelete.isReturn())
                    .collect(Collectors.toCollection(LinkedHashSet::new));
            collect.forEach(pf::delete);
        }
    }

    @NotNull
    private Integer createUpdateConnector(StationConnector connector) {
        StationConnectorEntity entity = StationConnectorEntityMapper.INSTANCE.toPojo(connector);
        pf.update(entity);
        return entity.getStationConnectorId();
    }
}
