package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.path;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Path;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Route;

public interface PathCreationDataTransfer {
    Route getRouteById(Integer routeId);

    void updateOrCreate(Route route);

    void deletePath(Path toDelete);
}
