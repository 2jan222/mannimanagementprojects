package at.fhv.ftb.inf.vz.semester4.groupd.domain;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatBusEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.OperationEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteRideEntityFast;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.security.IOperation;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class Operation implements IOperation, Available {
	private OperationEntity _operationEntity;
	private DayType _dayType;
	private long _checkSum;
	private boolean _checksumValid;
	private ChangeStatus _state;

	public Operation() {
		_operationEntity = new OperationEntity();
		_dayType = DayType.WORKDAY;
		_checksumValid = true;
	}

	public Operation(OperationEntity operationEntity) {
		_operationEntity = operationEntity;
		_dayType = DayType.getDayTypeOfDate(getDate());
		_state = ChangeStatus.OK;
	}

	public OperationEntity getCapsuledEntity() {
		return _operationEntity;
	}

	public int getOperationId() {
		return _operationEntity.getOperationId();
	}

	public DayType getDayType() {
		return _dayType;
	}

	public void setDayType(DayType dayType) {
		_dayType = dayType;
	}

	public String getName() {
		return "Tour " + getOperationId();
	}

	public List<RouteRide> getRouteRides() {
		return _operationEntity.getRouteRidesByOperationId().stream().map(RouteRide::new).collect(Collectors.toList());
	}

	public long getCheckSum() {
		if (_checkSum == 0 || !_checksumValid) {

            if (_dayType != null) {
				_checkSum += _dayType.hashCode();
			}
			for (RouteRideEntityFast ride : _operationEntity.getRouteRidesByOperationId()) {
				_checkSum += ride.getStartTimeByStartTimeId().hashCode();
			}
			_checksumValid = true;
		}

		return _checkSum;
	}

	public void invalidateChecksum() {
		_checksumValid = false;
	}

	public LocalDate getDate() {
		return _operationEntity.getDate();
	}

	public void setDate(LocalDate date) {
		_operationEntity.setDate(date);
	}

	public void setBus(FlatBusEntity bus) {
		_operationEntity.setBusByBusId(bus);
	}

	public FlatBusEntity getBus() {
		return _operationEntity.getBusByBusId();
	}

	public String getBusLicence() {
		return _operationEntity.getBusByBusId() != null ? _operationEntity.getBusByBusId().getLicenceNumber() : "";
	}

	public ChangeStatus getStatus() {
		return _state;
	}

	public void setStatus(ChangeStatus state) {
		_state = state;
	}

	@Override
	public String toString() {
		return "Operation [_operationId=" + getOperationId() + ", _dayType=" + _dayType + ", _checkSum=" + _checkSum
				+ "]";
	}
}
