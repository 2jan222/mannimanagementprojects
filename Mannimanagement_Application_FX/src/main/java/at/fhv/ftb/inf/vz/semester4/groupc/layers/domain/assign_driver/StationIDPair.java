package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver;

import org.jetbrains.annotations.Contract;

import java.util.Objects;

@SuppressWarnings("WeakerAccess")
public class StationIDPair {
    private Integer x;
    private Integer y;

    @Contract(pure = true)
    public StationIDPair(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    @Contract(value = "null -> false", pure = true)
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StationIDPair)) {
            return false;
        }
        StationIDPair stationIDPair = (StationIDPair) o;
        return Objects.equals(x, stationIDPair.x) && Objects.equals(y, stationIDPair.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
