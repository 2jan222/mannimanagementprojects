package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_bus;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Bus;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Operation;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.RouteRide;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.time.LocalTime;

/**
 * AssignBusActions.
 * Actions on operations.
 */
@LoggerableClassDisplayName("[AssignBusActions]")
public class AssignBusActions {
    private static Loggerable logger = Loggerable.getInstance();

    public static void assignBusToGroup(@NotNull Operation operation, Bus bus) {
        logger.debug("Assign Bus to Group Start");
        operation.setBus(bus);
        operation.setBusId(bus.getBusId());
        logger.debug("Assigned Bus to Group End");
    }

    @Contract("_ -> param1")
    @SuppressFBWarnings("NP_NULL_ON_SOME_PATH")
    public static Operation removeBusFromGroup(@NotNull Operation operation) {
        operation.setBus(null);
        operation.setBusId(null);
        operation.getRouteRides().forEach(e -> {
            e.setBusId(null);
            e.setBus(null);
        });
        return operation;
    }

    /**
     * Checks for timestamps intersecting after following cases.
     * SG: StartTime Group
     * EG: EndTime Group
     * SC: StartTime Candidate
     * EC: EndTime Candidate
     * Case 1: EC before SG - no overlapping
     * Case 2: EC after SG and SC before EG - overlapping
     * Case 3: EC after SG and SC before EG and SC after SG - overlapping
     * Case 4: EC after SG and EC after EG but SC before EG - overlapping
     * Case 5: EC after SG and SC after EG - not overlapping
     *
     * @param group     with given timestamps
     * @param candidate ride to check time overlapping for
     * @return true, if timestamps intersect
     */
    @SuppressWarnings("WeakerAccess")
    @SuppressFBWarnings("NP_NULL_ON_SOME_PATH")
    public static boolean isTimeOverlapping(@NotNull Operation group, @NotNull RouteRide candidate) {
        logger.debug("overlapping start");
        LocalTime candidateStartTime = candidate.getStartTime();
        logger.debug("candidateStartTime = " + candidateStartTime);
        LocalTime candidateEndTime = candidate.getEndTime();
        logger.debug("candidateStartTime = " + candidateStartTime);
        logger.debug("candidateEndTime = " + candidateEndTime);
        try {
            group.getRouteRides().forEach((ride -> {
                LocalTime groupTime = ride.getStartTime();
                if (candidateEndTime.isAfter(groupTime)) {
                    LocalTime groupEndTime = ride.getEndTime();
                    //Case 2,3,4,5
                    //Calculate EndTime of ride from group
                    if (candidateStartTime.isBefore(groupEndTime)) {
                        //Case 2,3,4
                        throw new RuntimeException("overlapping");
                    }
                }
            }));
            logger.debug("overlapping end");
        } catch (RuntimeException leaveLambda) {
            if (leaveLambda.getMessage().matches("^overlapping$")) {
                return true;
            } else {
                throw leaveLambda;
            }
        }
        return false;
    }


    /*private static LocalTime calcEndTime(RouteRide mojo) {
        return mojo.getEndTime();
    }*/

    /*

    @SuppressWarnings("unused")
    @Contract("!null, !null, _ -> new")
    @Deprecated
    public static Operation createGrouping(LocalDate date,
                                           RouteRide ride, RouteRide... rides) throws AssignBusException {
        LinkedList<RouteRide> groupingCandidates = new LinkedList<>(Arrays.asList(rides));
        groupingCandidates.addFirst(ride);
        Operation operation = new Operation();
        operation.setDate(date);
        for (RouteRide candidate : groupingCandidates) {
            addRideToGroup(operation, candidate);
        }
        return operation;
    }

    @Deprecated
    public static void addRideToGroup(Operation operation, RouteRide candidate) throws AssignBusException {
        logger.debug("Add Ride To Group");
        if (!isTimeOverlapping(operation, candidate)) {
            logger.debug("not overlapping");
            operation.getRouteRides().add(candidate);
            Bus bus = operation.getBus();
            Session session = DatabaseConnector.getSession();
            session.beginTransaction();
            String busId = (bus != null) ? "'" + bus.getBusId() + "'" : "null";
            String sql = "Update Route_Ride Set operation_id = '" + operation.getOperationId()
                    + "', bus_id = " + busId + " WHERE route_ride_id ='" + candidate.getRouteRideId() + "' ;";
            session.createSQLQuery(sql).executeUpdate();
            session.getTransaction().commit();
            session.close();
            logger.debug("Added Ride To Group");
        } else {
            logger.debug("Ride was not added - Ride is overlapping");
            throw new AssignBusException(AssignBusErrors.TIME_OVERLAPPING.setFailObj(candidate));
        }
    }

    @SuppressWarnings("unused")
    @Contract("_, _, _ -> param1")
    @Deprecated
    public static OperationEntity removeRideFromGroup(@NotNull OperationEntity group,
                                                      @NotNull RouteRideEntity element, RouteRideEntity... elements) {
        LinkedList<RouteRideEntity> forRemoval = new LinkedList<>(Arrays.asList(elements));
        forRemoval.addFirst(element);
        forRemoval.forEach(e -> {
            e.getBusByBusId().getRouteRidesByBusId().remove(e);
            e.setBusByBusId(null);
            group.getRouteRidesByOperationId().remove(e);
        });

        return group;
    }
     */
}
