package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Station;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.StationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * Probably outdated mapper.
 */
@Mapper
public interface StationDTOMapper {

    StationDTOMapper INSTANCE = Mappers.getMapper(StationDTOMapper.class);

    StationDTO toDTO(Station station);

    Station toMojo(StationDTO stationDTO);

    void updateDTO(Station station, @MappingTarget StationDTO stationDTO);

    void updateMojo(StationDTO stationDTO, @MappingTarget Station station);

    List<StationDTO> toDTOs(List<Station> stations);

    List<Station> toMojos(List<StationDTO> stationDTOS);

}
