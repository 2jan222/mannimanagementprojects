package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;
import at.fhv.ftb.inf.vz.semester4.groupc.Unused;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Deprecated
@Unused
@Entity
@Table(name = "shift", schema = "public", catalog = "kmobil")
public class ShiftEntity implements DatabaseEntityMarker {
    private Integer shiftId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shift_id")
    public Integer getShiftId() {
        return shiftId;
    }

    public void setShiftId(Integer shiftId) {
        this.shiftId = shiftId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShiftEntity that = (ShiftEntity) o;
        return shiftId == that.shiftId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(shiftId);
    }

    @Override
    public String toString() {
        return "ShiftEntity{" +
                "shiftId=" + shiftId +
                '}';
    }
}
