package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.OperationShiftEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteRideEntityFast;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Mapper(uses = DriverEntityMapper.class)
public interface ShiftEntryEntityMapper {
    ShiftEntryEntityMapper INSTANCE = Mappers.getMapper(ShiftEntryEntityMapper.class);

    @Mapping(target = "operationId", source = "operationId")
    @Mapping(target = "date", source = "date")
    @Mapping(target = "start", source = "startTime")
    @Mapping(target = "end", source = "endTime")
    @Mapping(target = "shiftEntryId", source = "operationShiftId")
    @Mapping(target = "shiftRides", source = "operationShiftEntity", qualifiedByName = "routeRideEntitiesToShiftRides")
    @Mapping(target = "driver", source = "driverByDriverId")
    @Mapping(target = "busID", source = "busId")
    ShiftEntry toMojo(OperationShiftEntity operationShiftEntity);

    @Mapping(target = "operationId", source = "operationId")
    @Mapping(target = "date", source = "date")
    @Mapping(target = "startTime", source = "start")
    @Mapping(target = "endTime", source = "end")
    @Mapping(target = "routeRidesByOperationShiftId", source = "shiftEntry", qualifiedByName = "shiftRidesToRouteRideEntities")
    @Mapping(target = "operationShiftId", source = "shiftEntryId")
    @Mapping(target = "driverByDriverId", source = "driver")
    @Mapping(target = "busId", source = "busID")
    OperationShiftEntity toPojo(ShiftEntry shiftEntry);

    @Mapping(target = "operationId", source = "operationId")
    @Mapping(target = "date", source = "date")
    @Mapping(target = "start", source = "startTime")
    @Mapping(target = "end", source = "endTime")
    @Mapping(target = "shiftEntryId", source = "operationShiftId")
    @Mapping(target = "shiftRides", source = "operationShiftEntity", qualifiedByName = "routeRideEntitiesToShiftRides")
    @Mapping(target = "driver", source = "driverByDriverId")
    @Mapping(target = "busID", source = "busId")
    void updateMojo(OperationShiftEntity operationShiftEntity, @MappingTarget ShiftEntry shiftEntry);

    @Mapping(target = "operationId", source = "operationId")
    @Mapping(target = "date", source = "date")
    @Mapping(target = "startTime", source = "start")
    @Mapping(target = "endTime", source = "end")
    @Mapping(target = "routeRidesByOperationShiftId", source = "shiftEntry", qualifiedByName = "shiftRidesToRouteRideEntities")
    @Mapping(target = "operationShiftId", source = "shiftEntryId")
    @Mapping(target = "driverByDriverId", source = "driver")
    @Mapping(target = "busId", source = "busID")
    void updatePojo(ShiftEntry shiftEntry, @MappingTarget OperationShiftEntity operationShiftEntity);

    List<ShiftEntry> toMojos(List<OperationShiftEntity> operationShiftEntities);

    List<OperationShiftEntity> toPojos(List<ShiftEntry> shiftEntries);

    @Named("routeRideEntitiesToShiftRides")
    default LinkedList<ShiftRide> routeRideEntitiesToShiftRides(OperationShiftEntity pojo) {
        List<RouteRideEntityFast> routeRides = new ArrayList<>(pojo.getRouteRidesByOperationShiftId());
        return new LinkedList<>(ShiftRideEntityMapper.INSTANCE.toMojos(routeRides));
    }

    @Named("shiftRidesToRouteRideEntities")
    default Set<RouteRideEntityFast> shiftRidesToRouteRideEntities(ShiftEntry mojo) {
        List<ShiftRide> shiftRides = mojo.getShiftRides();
        return new HashSet<>(ShiftRideEntityMapper.INSTANCE.toPojos(shiftRides));
    }
}
