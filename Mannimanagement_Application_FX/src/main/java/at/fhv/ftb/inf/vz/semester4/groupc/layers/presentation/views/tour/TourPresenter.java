package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.tour;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.menu.MenuViewItem;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.LoggerableColor;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

@LoggerableClassDisplayName(value = "[TourPresenter]", color = LoggerableColor.ANSI_PURPLE)
public class TourPresenter implements MenuViewItem, Initializable {
    @FXML
    private AnchorPane root;

    @Override
    public String getMenuDisplayName() {
        return "Create Tour";
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Loggerable.getInstance().debug("Init integrated UI", LoggerableColor.ANSI_PURPLE);

    }

    @Override
    public void setStage(Stage stage) {
        stage.getScene().setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.F5) {
                try {
                    Parent page = FXMLLoader.load(TourPresenter.class.getResource("../../../../../../groupd/ui/scenes/tourscene.fxml"));
                    root.getChildren().clear();
                    root.getChildren().add(page);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
