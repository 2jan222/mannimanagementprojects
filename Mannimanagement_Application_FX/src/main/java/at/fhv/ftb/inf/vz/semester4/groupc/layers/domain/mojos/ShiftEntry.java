package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos;

import org.jetbrains.annotations.TestOnly;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.Observable;

import static java.time.temporal.ChronoUnit.SECONDS;

public class ShiftEntry extends Observable {
    private Integer shiftEntryId;
    private Driver driver;
    private LocalDate date;
    private LocalTime start;
    private LocalTime end;
    private Integer operationId;
    private Integer busID;
    private Integer driverId;
    private LinkedList<ShiftRide> shiftRides;

    @TestOnly
    public ShiftEntry(Integer shiftEntryId, LocalDate date, LocalTime start, LocalTime end, Integer operationId, Integer busID, Integer driverId) {
        this.shiftEntryId = shiftEntryId;
        this.date = date;
        this.start = start;
        this.end = end;
        this.operationId = operationId;
        this.busID = busID;
        this.driverId = driverId;
    }



    public ShiftEntry(Driver driver, LocalDate date, LocalTime start, LocalTime end, Integer operationId, Integer busID) {
        this.driver = driver;
        this.date = date;
        this.start = start;
        this.end = end;
        this.operationId = operationId;
        this.busID = busID;
    }

    public ShiftEntry(Integer operationId) {
        this.operationId = operationId;
    }

    public ShiftEntry() {
    }

    public Driver getDriver() {
        return driver;
    }
    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Integer getOperationId() {
        return operationId;
    }

    public void notifyChanges() {
        notifyObservers();
    }

    public LocalTime getStartTime() {
        return start;
    }

    public LocalTime getEndTime() {
        return end;
    }

    public long getDuration() {
        return SECONDS.between(getStartTime(), getEndTime());
    }
    public LocalDate getDate() {
        return date;
    }

    public LinkedList<ShiftRide> getShiftRides() {
        return shiftRides;
    }

    public void setShiftRides(LinkedList<ShiftRide> shiftRides) {
        this.shiftRides = shiftRides;
    }

    public Integer getBusID() {
        return busID;
    }

    public void setBusID(Integer busID) {
        this.busID = busID;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public Integer getShiftEntryId() {
        return shiftEntryId;
    }

    public void setShiftEntryId(Integer shiftEntryId) {
        this.shiftEntryId = shiftEntryId;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getStart() {
        return start;
    }

    public void setStart(LocalTime start) {
        this.start = start;
    }

    public LocalTime getEnd() {
        return end;
    }

    public void setEnd(LocalTime end) {
        this.end = end;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    @Override
    public String toString() {
        return "ShiftEntry{" +
                "shiftEntryId=" + shiftEntryId +
                ", driver=" + driver +
                ", date=" + date +
                ", start=" + start +
                ", end=" + end +
                ", operationId=" + operationId +
                ", busID=" + busID +
                ", driverId=" + driverId +
                ", shiftRides=" + shiftRides.size() +
                '}';
    }
}
