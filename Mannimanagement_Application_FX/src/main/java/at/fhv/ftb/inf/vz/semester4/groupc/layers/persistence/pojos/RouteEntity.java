package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Table(name = "route", schema = "public", catalog = "kmobil")
public class RouteEntity implements DatabaseEntityMarker {
    private Integer routeId;
    private Integer routeNumber;
    private LocalDate validFrom;
    private LocalDate validTo;
    private String variation;
    private Set<PathEntity> pathsByRouteId;
    private Set<RouteRideEntityFast> routeRidesByRouteId;

    @Contract(pure = true)
    public RouteEntity() {
    }

    @Contract(pure = true)
    public RouteEntity(Integer routeId, Integer routeNumber, LocalDate validFrom, LocalDate validTo, String variation, Set<PathEntity> pathsByRouteId, Set<RouteRideEntityFast> routeRidesByRouteId) {
        this.routeId = routeId;
        this.routeNumber = routeNumber;
        this.validFrom = validFrom;
        this.validTo = validTo;
        this.variation = variation;
        this.pathsByRouteId = pathsByRouteId;
        this.routeRidesByRouteId = routeRidesByRouteId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "route_id")
    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    @Basic
    @Column(name = "route_number")
    public Integer getRouteNumber() {
        return routeNumber;
    }

    public void setRouteNumber(Integer routeNumber) {
        this.routeNumber = routeNumber;
    }

    @Basic
    @Column(name = "valid_from")
    public LocalDate getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDate validFrom) {
        this.validFrom = validFrom;
    }

    @Basic
    @Column(name = "valid_to")
    public LocalDate getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDate validTo) {
        this.validTo = validTo;
    }

    @Basic
    @Column(name = "variation")
    public String getVariation() {
        return variation;
    }

    public void setVariation(String variation) {
        this.variation = variation;
    }

    @OneToMany(mappedBy = "routeByRouteId", fetch = FetchType.EAGER)
    public Set<PathEntity> getPathsByRouteId() {
        return pathsByRouteId;
    }

    public void setPathsByRouteId(Set<PathEntity> pathsByRouteId) {
        this.pathsByRouteId = pathsByRouteId;
    }

    @OneToMany(mappedBy = "testRouteEntity", fetch = FetchType.EAGER)
    public Set<RouteRideEntityFast> getRouteRidesByRouteId() {
        return routeRidesByRouteId;
    }

    public void setRouteRidesByRouteId(Set<RouteRideEntityFast> routeRidesByRouteId) {
        this.routeRidesByRouteId = routeRidesByRouteId;
    }

    @Override
    public String toString() {
        return "RouteEntity{" +
                "routeId=" + routeId +
                ", routeNumber=" + routeNumber +
                ", validFrom=" + validFrom +
                ", validTo=" + validTo +
                ", variation='" + variation + '\'' +
                ", pathsByRouteIdAmount=" + pathsByRouteId.size() +
                ", routeRidesByRouteIdAmount=" + routeRidesByRouteId.size() +
                '}';
    }

    @Contract(value = "null -> false", pure = true)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RouteEntity that = (RouteEntity) o;
        return Objects.equals(routeId, that.routeId) &&
                Objects.equals(routeNumber, that.routeNumber) &&
                Objects.equals(validFrom, that.validFrom) &&
                Objects.equals(validTo, that.validTo) &&
                Objects.equals(variation, that.variation) &&
                Objects.equals(pathsByRouteId, that.pathsByRouteId);
    }

    private static Integer i = 0;

    @Override
    public int hashCode() {
        return Objects.hash(routeId, routeNumber, validFrom, validTo);
    }
}
