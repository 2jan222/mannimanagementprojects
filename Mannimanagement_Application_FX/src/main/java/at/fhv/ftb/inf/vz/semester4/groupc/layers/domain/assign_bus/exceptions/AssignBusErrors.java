package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_bus.exceptions;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.RouteRide;
import org.jetbrains.annotations.Contract;

/**
 * Types of errors which can occur in Grouper.
 */

public enum AssignBusErrors {
    TIME_OVERLAPPING(null);
    private RouteRide failOn;

    @Contract(pure = true)
    AssignBusErrors(RouteRide ride) {
        failOn = ride;
    }

    @Contract(pure = true)
    public RouteRide getFailOn() {
        return failOn;
    }

    @Contract("_ -> this")
    public AssignBusErrors setFailObj(RouteRide failObj) {
        failOn = failObj;
        return this;
    }
}
