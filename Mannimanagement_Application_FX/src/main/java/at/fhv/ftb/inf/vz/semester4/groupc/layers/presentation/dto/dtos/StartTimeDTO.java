package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos;

import java.time.LocalTime;
import java.util.LinkedList;

/**
 * StartTimeDTO.
 */
public class StartTimeDTO {
    private Integer startTimeId;
    private Integer pathId;
    private Integer requiredCapacity;
    private LocalTime startTime;
    private Integer startTimeType;
    private LinkedList<RouteRideDTO> routeRides = new LinkedList<>();
    private PathDTO path;

    public Integer getStartTimeId() {
        return startTimeId;
    }

    public void setStartTimeId(Integer startTimeId) {
        this.startTimeId = startTimeId;
    }

    public Integer getPathId() {
        return pathId;
    }

    public void setPathId(Integer pathId) {
        this.pathId = pathId;
    }

    public Integer getRequiredCapacity() {
        return requiredCapacity;
    }

    public void setRequiredCapacity(Integer requiredCapacity) {
        this.requiredCapacity = requiredCapacity;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public Integer getStartTimeType() {
        return startTimeType;
    }

    public void setStartTimeType(Integer startTimeType) {
        this.startTimeType = startTimeType;
    }

    public LinkedList<RouteRideDTO> getRouteRides() {
        return routeRides;
    }

    public void setRouteRides(LinkedList<RouteRideDTO> routeRides) {
        this.routeRides = routeRides;
    }

    public PathDTO getPath() {
        return path;
    }

    public void setPath(PathDTO path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "StartTimeDTO{" +
                "startTimeId=" + startTimeId +
                ", pathId=" + pathId +
                ", requiredCapacity=" + requiredCapacity +
                ", startTime=" + startTime +
                ", startTimeType=" + startTimeType +
                ", routeRides=" + routeRides +
                ", path=" + path +
                '}';
    }
}
