package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.bus_assignment.operationgroups;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.application.grouping.BusAssignmentService;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.bus_assignment.BusAssignmentPresenter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.BusDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.OperationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.RouteRideDTO;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.HBox;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * OperationGroupsPresenter.
 */
@LoggerableClassDisplayName("[OperationGroupsPresenter]")
public class OperationGroupsPresenter {
    @Inject
    BusAssignmentService busAssignmentService;
    @Inject
    private static Loggerable logger;
    @FXML
    private SplitPane splitPlane;
    @FXML
    private HBox groupButtonRoot;
    private LocalDate dateOfGroups;

    private LinkedList<GroupPlaneButtonPair> shown = new LinkedList<>();
    private LinkedList<GroupPlaneButtonPair> hidden = new LinkedList<>();
    private BusAssignmentPresenter parentPresenter;

    public void addGroups(@NotNull LinkedList<OperationDTO> operationDTO) {
        LinkedList<Parent> add = new LinkedList<>();
        for (OperationDTO dto : operationDTO) {
            if (dateOfGroups == null) {
                dateOfGroups = dto.getDate();
            }
            if (dateOfGroups.isEqual(dto.getDate())) {
                GroupPlaneButtonPair groupPlaneButtonPair =
                        new GroupPlaneButtonPair("" + dto.getOperationId(), dto, this);
                shown.add(groupPlaneButtonPair);
                add.add(groupPlaneButtonPair.getButton());
            } else {
                throw new RuntimeException("Date of Group does not match with current day");
            }
        }
        groupButtonRoot.getChildren().setAll(add);
    }

    public GroupPlaneButtonPair addGroup(OperationDTO operation) {
        if (dateOfGroups == null) {
            dateOfGroups = operation.getDate();
        }
        if (dateOfGroups.isEqual(operation.getDate())) {
            GroupPlaneButtonPair groupPlaneButtonPair =
                    new GroupPlaneButtonPair("" + operation.getOperationId(), operation, this);
            shown.add(groupPlaneButtonPair);
            groupButtonRoot.getChildren().add(0, groupPlaneButtonPair.getButton());
            return groupPlaneButtonPair;
        } else {
            throw new RuntimeException("Date of Group does not match with current day");
        }
    }

    public void hide(GroupPlaneButtonPair pair) {
        shown.remove(pair);
        hidden.add(pair);
        updateGroupView();
    }

    public void updateGroupView() {
        splitPlane.getItems().clear();
        List<Parent> collect = shown.stream().map(GroupPlaneButtonPair::getPlane).collect(Collectors.toList());
        splitPlane.getItems().addAll(collect);
        calcDiv();
        shown.forEach(GroupPlaneButtonPair::updatePairView);
        groupButtonRoot.getChildren().clear();
        collect = shown.stream().map(GroupPlaneButtonPair::getButton).collect(Collectors.toList());
        collect.addAll(hidden.stream().map(GroupPlaneButtonPair::getButton).collect(Collectors.toList()));
        groupButtonRoot.getChildren().addAll(collect);
    }

    private void calcDiv() {
        if (shown.size() > 1) {
            double[] b = new double[shown.size() - 1];
            for (int i = 0; i < shown.size() - 1; i++) {
                b[i] = (i + 1.0) / shown.size();
            }
            splitPlane.setDividerPositions(b);
        } else {
            splitPlane.setDividerPositions(0);
        }
    }

    public void show(GroupPlaneButtonPair pair) {
        hidden.remove(pair);
        shown.add(pair);
        updateGroupView();
    }

    public void changeDate(LocalDate date) {
        dateOfGroups = date;
        shown = new LinkedList<>();
        hidden = new LinkedList<>();
        //updateGroupView();
    }

    public void emptyGroup() {
        logger.debug("New Empty Group");
        //addGroup(new OperationDTO(dateOfGroups));
        logger.debug("Created Empty Group");
        calcDiv();
    }

    public void refreshRoot() {
        parentPresenter.refresh();
    }

    public void setParentPresenter(BusAssignmentPresenter parentPresenter) {
        this.parentPresenter = parentPresenter;
    }

    public BusAssignmentService getBusAssignmentService() {
        return busAssignmentService;
    }


    public void updateUngroupedBuses(BusDTO busDTO, boolean add) {
        parentPresenter.updateUngroupedBuses(busDTO, add);
    }

    public void updateUngroupedRides(RouteRideDTO routeRideDTO, boolean add) {
        parentPresenter.updateUngroupedRides(routeRideDTO, add);
    }
}
