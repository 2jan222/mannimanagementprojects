package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_bus;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Bus;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.FlatBusEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Operation;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.OperationEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.PersistenceFacade;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatBusEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.OperationEntity;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.LoggerableColor;
import org.hibernate.query.Query;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AssignBusDataTransferImpl implements AssignBusDataTransfer {

    private static final PersistenceFacade pf = PersistenceFacade.getInstance();
    private static final Loggerable logger = Loggerable.getInstance();

    @Override
    public void removeBusFromOperation(Integer operationId) {
        logger.debug("Remove bus from operation{id = " + operationId + "}");
        final String setQueryFragment = "Set bus_id = null";
        String whereQueryFragment = "WHERE operation_id = '" + operationId + "'";
        pf.updateWithFragments(OperationEntity.class, setQueryFragment,
                whereQueryFragment);
    }

    @Override
    public void assignBusToGroup(Integer busId, Integer operationId) {
        logger.debug("Add bus{id = " + busId + "} to operation{id = " + operationId + "}");
        String setQueryFragment = "SET bus_id = '" + busId + "'";
        String whereQueryFragment = "WHERE operation_id = '" + operationId + "'";
        pf.updateWithFragments(OperationEntity.class, setQueryFragment, whereQueryFragment);
    }

    @Override
    public LinkedList<Operation> getGroupsForDate(@NotNull LocalDate dateIn) {
        List<OperationEntity> daysOperations = pf.getAllWhere(OperationEntity.class,
                "WHERE date = '" + dateIn.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "'");
        logger.debug("SIZE:" + daysOperations.size(), LoggerableColor.ANSI_RED);
        return new LinkedList<>(OperationEntityMapper.INSTANCE.toMojos(daysOperations));
    }

    @Override
    public LinkedList<Bus> getUngroupedBusesForDate(@NotNull LocalDate dateIn) {
        List<FlatBusEntity> allWhere = pf.executeTransaction(session -> {
            Query<FlatBusEntity> query = session.createQuery("FROM FlatBusEntity b WHERE NOT EXISTS (Select o.busId FROM OperationEntity o WHERE o.date = '"
                    + dateIn.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                    + "' AND o.busId = b.busId)", FlatBusEntity.class);
            return query.getResultStream().collect(Collectors.toCollection(LinkedList::new));
        });
        return new LinkedList<>(FlatBusEntityMapper.INSTANCE.toMojos(allWhere));
    }

    @SuppressWarnings({"Duplicates"})
    @Override
    public LinkedList<LocalDate> getDatesWithOperations() {
        LinkedList<LocalDate> dates;
        dates = pf.executeTransaction(session -> {
            Query<LocalDate> query = session.createQuery("Select o.date FROM OperationEntity o WHERE o.date is not null", LocalDate.class);
            Stream<LocalDate> resultStream = query.getResultStream();
            return resultStream.collect(Collectors.toCollection(LinkedList::new));
        });
        return (dates != null) ? dates : new LinkedList<>();
    }
}
