package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.driver_assigment;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver.exceptions.AssignDriverContextCreationException;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver.exceptions.AssignDriverDataTransferUpdateException;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.application.driverassignment.DriverAssignmentService;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.driver_assigment.create_shift.CreateShiftPresenter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.driver_assigment.create_shift.CreateShiftView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.menu.MenuViewItem;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.DriverDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.ShiftEntryDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos.ShiftRideDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.util.CommonResourceGetter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.util.DragAndDropManager;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.util.FutureRunnable;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.util.GenericCallbackCreator;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.util.NotificationCreator;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.util.PopupWindowCreator;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTablePosition;
import javafx.scene.control.TreeTableView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.net.URL;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

@LoggerableClassDisplayName("[DriverAssignmentPresenter]")
public class DriverAssignmentPresenter implements MenuViewItem, Initializable {

    @Inject
    DriverAssignmentService service;
    @Inject
    private Loggerable logger;

    @FXML
    private Label shiftNameLabel;
    @FXML
    private HBox daysOperationList;
    @FXML
    private DatePicker dayDatePicker;
    @FXML
    private TableView<DriverDTO> driverTableView;
    @FXML
    private TableColumn<DriverDTO, Integer> driverIdCol;
    @FXML
    private TableColumn<DriverDTO, String> driverFirstNameCol;
    @FXML
    private TableColumn<DriverDTO, String> driverLastNameCol;
    @FXML
    private TableColumn<DriverDTO, String> driverQualificationsCol;
    @FXML
    private TreeTableColumn<TreeTableViewItemWrapper<String, String, ShiftRideDTO>, String> col;
    @FXML
    private TreeTableView<TreeTableViewItemWrapper<String, String, ShiftRideDTO>> tree;

    private Label tablePlaceholderLabel = new Label("Select a Shift to see applicable drivers");
    private TreeItem<TreeTableViewItemWrapper<String, String, ShiftRideDTO>> root =
            new TreeItem<>(new TreeTableViewItemWrapper<>("ROOT", "", null, null, null));
    private BiConsumer<TreeTableViewItemWrapper<String, String, ShiftRideDTO>,
            TreeTableCell<TreeTableViewItemWrapper<String, String, ShiftRideDTO>, String>> biConsumer = (row, cell) -> {
        if (row.getTableView() != null) {
            final FontAwesomeIconView remove = new FontAwesomeIconView(FontAwesomeIcon.TRASH);
            final Button removeBtn = new Button("", remove);
            String driverInfo = row.getInformation();
            if (driverInfo == null) {
                driverInfo = "Driver: NONE";
                removeBtn.setVisible(false);
            } else {
                driverInfo = "Driver: " + driverInfo;
                removeBtn.setVisible(true);
            }
            removeBtn.setOnAction(event -> {
                logger.debug("Remove Driver");
                try {
                    boolean b = service.removeDriverFromShift(row.getShiftEntry());
                    logger.debug("Deleted: " + b);
                    if (!b) {
                        NotificationCreator.createNotificationInternal("Removal Failed", "Driver could not be removed or was already removed.\nPlease restart Application");
                    }
                    Platform.runLater(new FutureRunnable(this::refresh));
                } catch (AssignDriverContextCreationException | AssignDriverDataTransferUpdateException e) {
                    logger.error(e.getMessage());
                    NotificationCreator.createNotificationInternal("Internal Error", "Internal Error\nPlease restart Application");
                }
            });
            removeBtn.setStyle("-fx-background-color: none");
            remove.setStyle("-fx-padding: 0 0 0 -3");
            VBox vBox = new VBox(new HBox(new Label(driverInfo), removeBtn), row.getTableView());
            initPaneDragAndDrop(vBox);
            cell.setGraphic(vBox);
        } else {
            cell.setGraphic(new Label(row.getGroupName()));
        }
    };
    private boolean appStart = true;


    @Override
    public String getMenuDisplayName() {
        return "Assign Driver";
    }

    @FXML
    public void refresh() {
        logger.debug("Refresh");
        LocalDate date = dayDatePicker.getValue();
        String lastOperation = ((Label) ((VBox) col.getGraphic()).getChildren().get(0)).getText();
        driverTableView.getItems().clear();
        loadDataForDay(date, lastOperation);
    }

    private void loadDataForDay(LocalDate date, String lastOperation) {
        LinkedList<Integer> operationsByIDOfDay = service.getOperationsByIDOfDay(date);
        createOperationButtons(operationsByIDOfDay, date);
        if (operationsByIDOfDay.size() > 0) {
            Integer i = null;
            try {
                i = Integer.parseInt(lastOperation.substring(11));
            } catch (NumberFormatException ignored) {
            }
            Integer operationId = (i != null && operationsByIDOfDay.contains(i)) ? operationsByIDOfDay.get(operationsByIDOfDay.indexOf(i)) : operationsByIDOfDay.getFirst();
            loadOperationShifts(operationId, date);
        } else {
            setColName("No operations for this day", null);
            if (!appStart) {
                NotificationCreator.createNotificationInternal("No Operations", "There are no operations for " + date);
                appStart = false;
            }
            shiftNameLabel.setText("");
            root.getChildren().clear();
            driverTableView.getItems().clear();
        }
    }

    private void setColName(String operationString, String busString) {
        final FontAwesomeIconView busIcon = new FontAwesomeIconView(FontAwesomeIcon.BUS);
        VBox vBox = new VBox();
        vBox.getChildren().add(new Label(operationString));
        if (busString != null) {
            vBox.getChildren().add(new Label(busString, busIcon));
        }
        col.setGraphic(vBox);
    }

    private void loadOperationShifts(Integer operationId, LocalDate date) {
        logger.debug("Load Operation Shifts");
        root.getChildren().clear();
        LinkedList<ShiftEntryDTO> shiftEntryDTOS;
        setColName("Operation: " + operationId, service.getBusLicenseByOperationId(operationId, date));
        try {
            shiftEntryDTOS = service.getShiftEntriesByOperationId(operationId, date);
            ShiftEntryDTO last = null;
            for (ShiftEntryDTO seo : shiftEntryDTOS) {
                LinkedList<ShiftRideDTO> list = new LinkedList<>(seo.getShiftRides());
                String shiftName = "Shift " + seo.getShiftEntryId();
                TreeItem<TreeTableViewItemWrapper<String, String, ShiftRideDTO>> shift =
                        new TreeItem<>(new TreeTableViewItemWrapper<>(shiftName,
                                "", null, null, seo));
                TreeItem<TreeTableViewItemWrapper<String, String, ShiftRideDTO>> dataItem = new TreeItem<>(
                        new TreeTableViewItemWrapper<>("",
                                service.getDriverInformationFromDriverId(seo.getDriverId(), date), list,
                                ShiftRideTableFactory.createTableView(), seo));
                shift.getChildren().add(dataItem);
                shift.setExpanded(true);
                last = seo;
                root.getChildren().add(shift);
                root.getChildren().sort(Comparator.comparing(o -> o.getValue().getGroupName()));
            }
            if (last != null) {
                shiftNameLabel.setText("Shift " + last.getShiftEntryId());
                loadFreeDriverFor(last);
            }
            logger.debug("Loaded Operation Shifts for Operation(" + operationId + ")");
        } catch (AssignDriverContextCreationException e) {
            logger.error("AssignDriverContextCreationException " + e);
            NotificationCreator.createNotificationInternal("Internal Error", "Internal Error\nPlease restart Application");
        }
    }

    private void createOperationButtons(@NotNull LinkedList<Integer> operationsByIDOfDay, LocalDate date) {
        daysOperationList.getChildren().clear();
        operationsByIDOfDay.forEach(e -> {
            final Button btn = new Button("" + e);
            btn.setOnAction(ev -> loadOperationShifts(e, date));
            daysOperationList.getChildren().add(btn);
        });
    }

    private void loadFreeDriverFor(ShiftEntryDTO shiftEntryDTO) {
        try {
            LinkedList<DriverDTO> drivers = service.getFreeDriverFormId(shiftEntryDTO);
            driverTableView.setItems(FXCollections.observableArrayList(drivers));
        } catch (AssignDriverContextCreationException e) {
            logger.error("AssignDriverContextCreationException: " + e.getMessage());
            setDriverTablePlaceholderText("No diver could be found");
            NotificationCreator.createNotificationInternal("Drivers not found", "No driver could be found!");
        }
    }

    private void setDriverTablePlaceholderText(String text) {
        tablePlaceholderLabel.setText(text);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initDatePicker();
        initDriverView();
        initTreeView();
        loadDataForDay(LocalDate.now(), null);

    }

    private void initDatePicker() {
        dayDatePicker.setValue(LocalDate.now());
        dayDatePicker.setDayCellFactory(picker -> new DateCell() {
            @SuppressWarnings("Duplicates")
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                LocalDate today = LocalDate.now();
                if (service.getDatesWithOperationsWhichHaveBus().contains(date)) {
                    setStyle("-fx-background-color: #3a62ff;");
                }
                setDisable(empty || date.compareTo(today) < 0);
            }
        });
    }

    @SuppressWarnings("Duplicates")
    private void initDriverView() {
        driverIdCol.setCellValueFactory(d -> new SimpleObjectProperty<>(d.getValue().getDriverId()));
        driverFirstNameCol.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getFirstname()));
        driverLastNameCol.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getLastname()));
        driverQualificationsCol.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getJobDescription()));
        driverTableView.setPlaceholder(tablePlaceholderLabel);
        setDriverTablePlaceholderText("Select a shift entry");

        driverTableView.setOnDragDetected(e -> {
            Dragboard dragBoard = driverTableView.startDragAndDrop(TransferMode.ANY);
            ClipboardContent content = new ClipboardContent();
            DriverDTO src = driverTableView.getSelectionModel().getSelectedItem();
            DragAndDropManager.add(src);
            content.putString("" + src.hashCode());
            dragBoard.setContent(content);
            e.consume();
        });
    }

    private void initTreeView() {
        GenericCallbackCreator<TreeTableViewItemWrapper<String, String, ShiftRideDTO>, String> genericCallbackCreator = new GenericCallbackCreator<>();
        col.setCellFactory(genericCallbackCreator.createTreeCellFactory(biConsumer));
        col.setText("");
        tree.setShowRoot(false);
        tree.setRoot(root);
        tree.setPlaceholder(new Label("Select date above"));
        tree.getSelectionModel().setCellSelectionEnabled(true);
        ObservableList<TreeTablePosition<TreeTableViewItemWrapper<String, String, ShiftRideDTO>, ?>> selectedCells
                = tree.getSelectionModel().getSelectedCells();

        selectedCells.addListener((ListChangeListener<TreeTablePosition>) c -> {
            try {
                TreeTablePosition<TreeTableViewItemWrapper<String, String, ShiftRideDTO>, ?> tablePosition
                        = selectedCells.get(0);
                ShiftEntryDTO val = tablePosition.getTreeItem().getValue().getShiftEntry();
                shiftNameLabel.setText("Shift " + val.getShiftEntryId());
                System.out.println("val = " + val);
                loadFreeDriverFor(val);
            } catch (IndexOutOfBoundsException outBounds) {
                //outBounds.printStackTrace();
                logger.warn("OutOfBoundsException");
            }
        });
    }


    private void initPaneDragAndDrop(@NotNull VBox vBox) {
        vBox.setOnDragEntered(Event::consume);
        vBox.setOnDragDropped(e -> {
            logger.debug("Drag Dropped");
            logger.debug(e.getGestureSource().toString());
            String string = e.getDragboard().getString();
            Object o = DragAndDropManager.get(string);
            if (o instanceof DriverDTO) {
                TreeItem<TreeTableViewItemWrapper<String, String, ShiftRideDTO>> selectedItem = tree.getSelectionModel().getSelectedItem();
                ShiftEntryDTO shift = selectedItem.getValue().getShiftEntry();
                DriverDTO driverDTO = (DriverDTO) o;
                try {
                    boolean b = service.assignDriverToShift(shift, driverDTO);
                    logger.debug("Assignment Driver: " + b);
                    if (b) {
                        Platform.runLater(new FutureRunnable(this::refresh));
                    } else {
                        String cause = "UNKNWON";
                        NotificationCreator.createWarningNotification("Assignment failed", "The assignment of driver:\n\t" + driverDTO.getFirstname() + " " + driverDTO.getLastname() + "\nCause: " + cause);
                    }
                } catch (AssignDriverContextCreationException | AssignDriverDataTransferUpdateException ex) {
                    logger.error("Application Exception");
                    ex.printStackTrace();
                    NotificationCreator.createErrorNotification("Internal Error", "Internal Error\nPlease restart Application");
                }
            }
            //DragAndDropManager.remove(o);
            e.consume();
        });

        vBox.setOnDragOver(e -> {
            String string = e.getDragboard().getString();
            Object o = DragAndDropManager.get(string);
            if (o instanceof DriverDTO) {
                e.acceptTransferModes(TransferMode.ANY);
            }
            e.consume();
        });

        vBox.setOnDragDone(e -> {
            logger.debug("Drag done");
            e.getDragboard().clear();
            e.setDropCompleted(true);
            e.consume();
        });
    }

    @FXML
    private void popupCreateShift(ActionEvent actionEvent) {
        CreateShiftView view = new CreateShiftView();
        Stage stage = PopupWindowCreator.createWindow(
                view,
                "Create Shift Entries",
                CommonResourceGetter.getBusIcon(),
                CommonResourceGetter.getWindowFromActionEvent(actionEvent),
                Modality.APPLICATION_MODAL
        );
        stage.show();
        String lastOperation = ((Label) ((VBox) col.getGraphic()).getChildren().get(0)).getText();
        Integer i = null;
        try {
            i = Integer.parseInt(lastOperation.substring(11));
        } catch (NumberFormatException ignored) {}
        try {
            ((CreateShiftPresenter) view.getPresenter())
                    .display(new FutureRunnable(this::refresh), i, service.getRidesByOperationId(i, dayDatePicker.getValue()).stream().filter(shiftRide -> shiftRide.getOperationShiftId() == null).collect(Collectors.toCollection(LinkedList::new)), dayDatePicker.getValue());
        } catch (AssignDriverContextCreationException e) {
            e.printStackTrace();
            NotificationCreator.createErrorNotification("Internal Error", "No Date Found");
        }
    }
}
