package at.fhv.ftb.inf.vz.semester4.groupd.domain;

import at.fhv.ftb.inf.vz.semester4.groupd.domain.security.IOperation;
import at.fhv.ftb.inf.vz.semester4.groupd.persistance.entities.RouteRideEntity;
import at.fhv.ftb.inf.vz.semester4.groupd.persistance.entities.SuspendedEntity;

import java.time.LocalDate;

@Deprecated
public class ConflictingOperation {
	private IOperation _operation;
	private Available _cause;
	
	public ConflictingOperation(Operation operation, Available cause) {
		_operation = (IOperation) operation;
		_cause = cause;
	}
	
	public String getOperationId () {
		return String.valueOf(_operation.getOperationId());
	}
	
	public LocalDate getDate() {
		return _operation.getDate();
	}
	
	public String getCause() {
		if(_cause instanceof RouteRideEntity) {
			return "Already assigned tour on this day";
		}
		else if(_cause instanceof SuspendedEntity) {
			SuspendedEntity suspended = (SuspendedEntity)_cause;
			return suspended.getCause().equals("") ? "conflicting suspention" : "conflicting suspention:" + suspended.getCause();
		}
		
		return "conflicting";
	}
}
