package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.util;

import org.jetbrains.annotations.NotNull;

@SuppressWarnings("WeakerAccess")
public class Storage<N, T> {
    private T value;

    T getValue() {
        return value;
    }

    void setValue(@NotNull Mediator<N, T> mediator, N storageName, T value) {
        this.value = value;
        mediator.notifyObservers(storageName);
    }

}
