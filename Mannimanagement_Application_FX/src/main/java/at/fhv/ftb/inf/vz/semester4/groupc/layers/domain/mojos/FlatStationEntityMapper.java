package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatStationEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface FlatStationEntityMapper {

    FlatStationEntityMapper INSTANCE = Mappers.getMapper(FlatStationEntityMapper.class);

    Station toMojo(FlatStationEntity stationEntity);

    FlatStationEntity toPojos(Station station);

    void updateMojo(FlatStationEntity stationEntity, @MappingTarget Station station);

    void updatePojo(Station station, @MappingTarget FlatStationEntity stationEntity);

    List<Station> toMojos(List<FlatStationEntity> stationEntities);

    List<FlatStationEntity> toPojos(List<Station> stations);
}
