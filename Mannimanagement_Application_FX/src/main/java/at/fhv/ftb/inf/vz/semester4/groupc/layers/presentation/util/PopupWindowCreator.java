package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.util;

import com.airhacks.afterburner.views.FXMLView;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;


/**
 * Creator for popup windows.
 */
public class PopupWindowCreator {
    /**
     * Creates a popup window.
     * @param view View of popup window.
     * @param title Title of popup window.
     * @param icon Icon of popup window.
     * @param owner Window parent of popup window.
     * @param initModality Modality of popup window.
     * @param functions VarArg param all consumers get executed on stage.
     * @return Stage, which has been created.
     */
    @SuppressFBWarnings({"NP_NULL_ON_SOME_PATH", "NP_NULL_ON_SOME_PATH_MIGHT_BE_INFEASIBLE"})
    @SafeVarargs
    public static Stage createWindow(@NotNull FXMLView view,
                                     @NotNull String title,
                                     @NotNull Image icon,
                                     @NotNull Window owner,
                                     @NotNull Modality initModality,
                                     @NotNull Consumer<Stage>... functions) {
        Stage popupWindow = new Stage();
        popupWindow.setScene(new Scene(view.getView()));
        popupWindow.setTitle(title);
        popupWindow.initOwner(owner);
        popupWindow.getIcons().add(icon);
        popupWindow.initModality(initModality);
        for (Consumer<Stage> consumer : functions) {
            consumer.accept(popupWindow);
        }
        return popupWindow;
    }
}
