package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.TestOnly;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "bus", schema = "public", catalog = "kmobil")
public class BusEntity implements DatabaseEntityMarker {
    private Integer busId;
    private Integer maintenanceKm;
    private String licenceNumber;
    private String make;
    private String model;
    private String note;
    private LocalDate registrationDate;
    private Integer seatPlaces;
    private Integer standPlaces;
    //private Set<RouteRideEntity> routeRidesByBusId;
    private Set<SuspendedEntity> suspendedsByBusId;
    private Set<OperationEntity> operationsByBusId;

    @Contract(pure = true)
    public BusEntity() {
    }

    @Contract(pure = true)
    @TestOnly
    public BusEntity(int busId, int maintenanceKm, String licenceNumber, String make, String model, String note, LocalDate registrationDate, int seatPlaces, int standPlaces, Set<RouteRideEntity> routeRidesByBusId, Set<SuspendedEntity> suspendedsByBusId, Set<OperationEntity> operationsByBusId) {
        this.busId = busId;
        this.maintenanceKm = maintenanceKm;
        this.licenceNumber = licenceNumber;
        this.make = make;
        this.model = model;
        this.note = note;
        this.registrationDate = registrationDate;
        this.seatPlaces = seatPlaces;
        this.standPlaces = standPlaces;
        //this.routeRidesByBusId = routeRidesByBusId;
        this.suspendedsByBusId = suspendedsByBusId;
        this.operationsByBusId = operationsByBusId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bus_id")
    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    @Basic
    @Column(name = "maintenance_km")
    public Integer getMaintenanceKm() {
        return maintenanceKm;
    }

    public void setMaintenanceKm(Integer maintenanceKm) {
        this.maintenanceKm = maintenanceKm;
    }

    @Basic
    @Column(name = "licence_number")
    public String getLicenceNumber() {
        return licenceNumber;
    }

    public void setLicenceNumber(String licenceNumber) {
        this.licenceNumber = licenceNumber;
    }

    @Basic
    @Column(name = "make")
    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    @Basic
    @Column(name = "domain")
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Basic
    @Column(name = "note")
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Basic
    @Column(name = "registration_date")
    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Basic
    @Column(name = "seat_places")
    public Integer getSeatPlaces() {
        return seatPlaces;
    }

    public void setSeatPlaces(Integer seatPlaces) {
        this.seatPlaces = seatPlaces;
    }

    @Basic
    @Column(name = "stand_places")
    public Integer getStandPlaces() {
        return standPlaces;
    }

    public void setStandPlaces(Integer standPlaces) {
        this.standPlaces = standPlaces;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BusEntity busEntity = (BusEntity) o;
        return Objects.equals(busId, busEntity.busId) &&
                Objects.equals(maintenanceKm, busEntity.maintenanceKm) &&
                Objects.equals(seatPlaces, busEntity.seatPlaces) &&
                Objects.equals(standPlaces, busEntity.standPlaces) &&
                Objects.equals(licenceNumber, busEntity.licenceNumber) &&
                Objects.equals(make, busEntity.make) &&
                Objects.equals(model, busEntity.model) &&
                Objects.equals(note, busEntity.note) &&
                Objects.equals(registrationDate, busEntity.registrationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(busId, maintenanceKm, licenceNumber, make, model, note, registrationDate, seatPlaces, standPlaces);
    }

    /*
    @OneToMany(mappedBy = "busByBusId", fetch = FetchType.EAGER)
    public Set<RouteRideEntity> getRouteRidesByBusId() {
        return routeRidesByBusId;
    }

    public void setRouteRidesByBusId(Set<RouteRideEntity> routeRidesByBusId) {
        this.routeRidesByBusId = routeRidesByBusId;
    }

     */

    @OneToMany(mappedBy = "busByBusId", fetch = FetchType.EAGER)
    public Set<SuspendedEntity> getSuspendedsByBusId() {
        return suspendedsByBusId;
    }

    public void setSuspendedsByBusId(Set<SuspendedEntity> suspendedsByBusId) {
        this.suspendedsByBusId = suspendedsByBusId;
    }

    @Override
    public String toString() {
        return "BusEntity{" +
                "busId=" + busId +
                ", maintenanceKm=" + maintenanceKm +
                ", licenceNumber='" + licenceNumber + '\'' +
                ", make='" + make + '\'' +
                ", domain='" + model + '\'' +
                ", note='" + note + '\'' +
                ", registrationDate=" + registrationDate +
                ", seatPlaces=" + seatPlaces +
                ", standPlaces=" + standPlaces +
                //", routeRidesByBusIdAmount=" + routeRidesByBusId.size() +
                ", suspendedsByBusIdAmount=" + suspendedsByBusId.size() +
                '}';
    }

    @OneToMany(mappedBy = "busByBusId", fetch = FetchType.EAGER)
    public Set<OperationEntity> getOperationsByBusId() {
        return operationsByBusId;
    }

    public void setOperationsByBusId(Set<OperationEntity> operationsByBusId) {
        this.operationsByBusId = operationsByBusId;
    }
}
