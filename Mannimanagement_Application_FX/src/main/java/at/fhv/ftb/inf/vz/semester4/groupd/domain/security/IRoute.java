package at.fhv.ftb.inf.vz.semester4.groupd.domain.security;

import at.fhv.ftb.inf.vz.semester4.groupd.domain.DayType;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.Path;

import java.time.LocalDate;
import java.util.List;

public interface IRoute {
    public int getRouteId();

    public int getRouteNumber();

    public LocalDate getValidFrom();

    public LocalDate getValidTo();

    public String getVariation();

    public List<IRouteRide> getRouteRides();

    public List<? extends IRouteRide> getOpenRouteRides(List<? extends IOperation> oper, DayType daytype);

	public List<Path> getPaths();

    public int getNumberOfRidesPerDayType(DayType dayType);

}
