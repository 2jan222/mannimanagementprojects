package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.util;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableRow;
import javafx.util.Callback;

import java.util.function.BiConsumer;

public class GenericCallbackCreator<ROW, TYPE> {

    public Callback<TreeTableColumn<ROW, TYPE>, TreeTableCell<ROW, TYPE>> createTreeCellFactory(BiConsumer<ROW, TreeTableCell<ROW, TYPE>> rowConsumer) {
        return new Callback<TreeTableColumn<ROW, TYPE>, TreeTableCell<ROW, TYPE>>() {
            @Override
            public TreeTableCell<ROW, TYPE> call(final TreeTableColumn<ROW, TYPE> param) {
                return new TreeTableCell<ROW, TYPE>() {

                    @Override
                    public void updateItem(TYPE item, boolean empty) {
                        super.updateItem(item, empty);
                        TreeTableRow<ROW> value = getTreeTableRow();
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            if (value != null && value.getTreeItem() != null) {
                                ROW rowValue = value.getTreeItem().getValue();
                                rowConsumer.accept(rowValue, this);
                            }
                            setText(null);
                        }
                    }
                };
            }
        };
    }

    public Callback<TableColumn<ROW, TYPE>, TableCell<ROW, TYPE>> createCellFactory(BiConsumer<ROW, TableCell<ROW, TYPE>> rowConsumer) {
        return new Callback<TableColumn<ROW, TYPE>, TableCell<ROW, TYPE>>() {
            @Override
            public TableCell<ROW, TYPE> call(final TableColumn<ROW, TYPE> param) {
                return new TableCell<ROW, TYPE>() {

                    @Override
                    public void updateItem(TYPE item, boolean empty) {
                        super.updateItem(item, empty);
                        //noinspection unchecked
                        TableRow<ROW> value = getTableRow();
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            if (value != null && value.getItem() != null) {
                                ROW rowValue = value.getItem();
                                rowConsumer.accept(rowValue, this);
                            }
                            setText(null);
                        }
                    }
                };
            }
        };
    }

    /*
    @Deprecated
    public Callback<TreeTableColumn<TreeTableViewItemWrapper<String, DriverDTO>, String>, TreeTableCell<TreeTableViewItemWrapper<String, DriverDTO>, String>> crateCellFactory2() {
        return new Callback<TreeTableColumn<TreeTableViewItemWrapper<String, DriverDTO>, String>, TreeTableCell<TreeTableViewItemWrapper<String, DriverDTO>, String>>() {
            @Override
            public TreeTableCell<TreeTableViewItemWrapper<String, DriverDTO>, String> call(final TreeTableColumn<TreeTableViewItemWrapper<String, DriverDTO>, String> param) {
                return new TreeTableCell<TreeTableViewItemWrapper<String, DriverDTO>, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        TreeTableRow<TreeTableViewItemWrapper<String, DriverDTO>> value = getTreeTableRow();
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            if (value != null && value.getTreeItem() != null) {
                                TreeTableViewItemWrapper<String, DriverDTO> treeDTO = value.getTreeItem().getValue();
                                if (treeDTO.getTableView() != null) {
                                    setGraphic(treeDTO.getTableView());
                                } else {
                                    setGraphic(new Label(treeDTO.getGroupName()));
                                }
                            }
                            setText(null);
                        }
                    }
                };
            }
        };
    }

     */

}
