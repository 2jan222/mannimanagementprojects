package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver.exceptions;

@SuppressWarnings("WeakerAccess")
public class AssignDriverContextCreationException extends Exception {
    public AssignDriverContextCreationException(String msg) {
        super(msg);
    }
}
