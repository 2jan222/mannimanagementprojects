package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence;

import at.fhv.ftb.inf.vz.semester4.groupc.Unused;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.PersistenceFacade;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.OperationEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteRideEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.StationConnectorEntity;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import org.hibernate.Session;

import javax.persistence.TypedQuery;

/**
 * QueryService.
 */
@SuppressWarnings("SqlDialectInspection")
@Deprecated
@Unused
@LoggerableClassDisplayName("[Query]")
public abstract class QueryService {
    private static Loggerable logger = Loggerable.getInstance();
    private static final PersistenceFacade pf = PersistenceFacade.getInstance();


    public static void removeBusFromOperation(Integer operationId) {

        logger.debug("Remove bus from operation{id = " + operationId + "}");
        final String setQueryFragment = "Set bus_id = null";
        String whereQueryFragment = "WHERE operation_id = '" + operationId + "'";
        //pf.updateWithFragments(RouteRideEntity.class, setQueryFragment, whereQueryFragment);
        pf.updateWithFragments(OperationEntity.class, setQueryFragment,
                whereQueryFragment);
    }


    public static void assignBusToGroup(Integer busId, Integer operationId) {
        logger.debug("Add bus{id = " + busId + "} to operation{id = " + operationId + "}");
        String setQueryFragment = "SET bus_id = '" + busId + "'";
        String whereQueryFragment = "WHERE operation_id = '" + operationId + "'";
        //pf.updateWithFragments(RouteRideEntity.class, setQueryFragment, whereQueryFragment);
        pf.updateWithFragments(OperationEntity.class, setQueryFragment, whereQueryFragment);
    }

    public static void assignBusToGroupOLD(Integer busId, Integer operationId) {
        logger.debug("Add bus{id = " + busId + "} to operation{id = " + operationId + "}");
        Session session = DatabaseConnector.getSession();
        session.beginTransaction();
        String sql = "Update Route_Ride Set bus_id = '" + busId + "' WHERE operation_id = '" + operationId + "'";
        session.createSQLQuery(sql).executeUpdate();
        String sql2 = "Update Operation Set bus_id = '" + busId + "' WHERE operation_id = '" + operationId + "'";
        session.createSQLQuery(sql2).executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    public static void removeBusFromOperationOLD(Integer operationId) {
        logger.debug("Remove bus from operation{id = " + operationId + "}");
        Session session = DatabaseConnector.getSession();
        session.beginTransaction();
        pf.updateWithFragments(RouteRideEntity.class, "Set bus_id = null",
                "WHERE operation_id = '" + operationId + "'");
        String sql = "Update Route_Ride Set bus_id = null WHERE operation_id = '" + operationId + "'";
        session.createSQLQuery(sql).executeUpdate();
        pf.updateWithFragments(OperationEntity.class, "Set bus_id = null",
                " WHERE operation_id = '" + operationId + "'");
        String sql2 = "Update Operation Set bus_id = null WHERE operation_id = '" + operationId + "'";
        session.createSQLQuery(sql2).executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    public static void removeRideFromOperationOLD(long id) {
        Session session = DatabaseConnector.getSession();
        session.beginTransaction();
        String sql = "Update Route_Ride Set operation_id = null WHERE route_ride_id = '" + id + "'";
        session.createSQLQuery(sql).executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    public static StationConnectorEntity getStationConnectorByStartAndEnd(int start, int end) {
        final String selectFragment = "SELECT s FROM StationConnectorEntity s ";
        String whereFragment = "WHERE s.startStationId = '" + start + "' AND s.endStationId = '" + end + "'";
        StationConnectorEntity stationConnectorEntity;
        try (Session sess = DatabaseConnector.getSession()) {
            TypedQuery query = sess.createQuery(selectFragment + whereFragment, StationConnectorEntity.class);
            stationConnectorEntity = (StationConnectorEntity) query.getSingleResult();
        }
        return stationConnectorEntity;

    }
}
