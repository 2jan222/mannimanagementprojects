package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos;

import org.jetbrains.annotations.Contract;

import java.time.LocalDate;
import java.util.LinkedList;

public class OperationFlat {

    private Integer operationId;
    private LocalDate date;
    private Integer busId;
    private Bus bus;
    private LinkedList<ShiftRide> shiftRides;

    @Contract(pure = true)
    public OperationFlat() {
    }

    @Contract(pure = true)
    public OperationFlat(Integer operationId, LocalDate date, Integer busId, Bus bus, LinkedList<ShiftRide> shiftRides) {
        this.operationId = operationId;
        this.date = date;
        this.busId = busId;
        this.bus = bus;
        this.shiftRides = shiftRides;
    }

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public LinkedList<ShiftRide> getShiftRides() {
        return shiftRides;
    }

    public void setShiftRides(LinkedList<ShiftRide> shiftRides) {
        this.shiftRides = shiftRides;
    }
}
