package at.fhv.ftb.inf.vz.semester4.groupd.domain;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.StationEntity;

import java.util.Set;
import java.util.stream.Collectors;

public class Station {
	private StationEntity _station;
    
    public Station(StationEntity station) {
    	_station = station;
    }

    public int getStationId() {
        return _station.getStationId();
    }

    public void setStationId(int stationId) {
        _station.setStationId(stationId);
    }

    public String getStationName() {
        return _station.getStationName();
    }

    public void setStationName(String stationName) {
        _station.setStationName(stationName);
    }

    public String getShortName() {
        return _station.getShortName();
    }

    public void setShortName(String shortName) {
        _station.setShortName(shortName);
    }

    public Set<PathStation> getPathStations() {
        return _station.getStartStationConnectorsByStationId().stream().map(e -> new PathStation(e.getPathStationEntity())).collect(Collectors.toSet());
    }
}
