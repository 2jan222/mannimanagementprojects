package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.PathEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class PathPersistenceEntityMapper extends PersistenceEntityMapper<PathEntity> {
    @Override
    public PathEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(PathEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("PathEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<PathEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<PathEntity> query = session.createQuery("from PathEntity ", PathEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<PathEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<PathEntity> query = session.createQuery("from PathEntity " + whereQueryFragment, PathEntity.class);
            return query.getResultList();
        }
    }
}