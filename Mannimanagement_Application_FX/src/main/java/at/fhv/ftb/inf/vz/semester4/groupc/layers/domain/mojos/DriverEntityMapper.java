package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.DriverEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = AbsenceEntityMapper.class)
public interface DriverEntityMapper {

    DriverEntityMapper INSTANCE = Mappers.getMapper(DriverEntityMapper.class);

    @Mapping(target = "absences", source = "absenceEntities")
    Driver toMojo(DriverEntity driverEntity);


    @Mapping(target = "absenceEntities", source = "absences")
    DriverEntity toPojo(Driver driver);

    @Mapping(target = "absences", source = "absenceEntities")
    void updateMojo(DriverEntity driverEntity, @MappingTarget Driver driver);


    @Mapping(target = "absenceEntities", source = "absences")
    void updatePojo(Driver driver, @MappingTarget DriverEntity driverEntity);

    List<Driver> toMojos(List<DriverEntity> driverEntities);

    List<DriverEntity> toPojos(List<Driver> drivers);
}
