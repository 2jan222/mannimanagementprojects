package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.util;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

/**
 * Drag and Drop Manager.
 * @author Janik Mayr on 13.04.2019
 */
public class DragAndDropManager {
    private static HashMap<String, Object> objectsAfterHashCode = new HashMap<>();

    public static Object add(Object o) {
        return objectsAfterHashCode.put("" + o.hashCode(), o);
    }

    public static Object get(String hash) {
        return objectsAfterHashCode.get(hash);
    }

    public static Object remove(@NotNull Object o) {
        return objectsAfterHashCode.remove("" + o.hashCode());
    }

    public static Object add(String key, Object o) {
        return objectsAfterHashCode.put(key, o);
    }

    public static void clear() {
        objectsAfterHashCode.clear();
    }

}
