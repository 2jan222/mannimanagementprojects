package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos;

import java.time.LocalDate;

public class DriverDTO {
    private Integer driverId;
    private String address;
    private LocalDate birthday;
    private String email;
    private Integer employmentType;
    private String firstname;
    private String lastname;
    private String jobDescription;
    private String telephonenumber;

    public DriverDTO(Integer driverId, String address, LocalDate birthday, String email, Integer employmentType,
                     String firstname, String lastname, String jobDescription, String telephonenumber) {
        this.driverId = driverId;
        this.address = address;
        this.birthday = birthday;
        this.email = email;
        this.employmentType = employmentType;
        this.firstname = firstname;
        this.lastname = lastname;
        this.jobDescription = jobDescription;
        this.telephonenumber = telephonenumber;
    }

    public DriverDTO() {
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(Integer employmentType) {
        this.employmentType = employmentType;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getTelephonenumber() {
        return telephonenumber;
    }

    public void setTelephonenumber(String telephonenumber) {
        this.telephonenumber = telephonenumber;
    }

    @Override
    public String toString() {
        return "DriverDTO{" +
                "driverId=" + driverId +
                ", address='" + address + '\'' +
                ", birthday=" + birthday +
                ", email='" + email + '\'' +
                ", employmentType=" + employmentType +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", jobDescription='" + jobDescription + '\'' +
                ", telephonenumber='" + telephonenumber + '\'' +
                '}';
    }
}
