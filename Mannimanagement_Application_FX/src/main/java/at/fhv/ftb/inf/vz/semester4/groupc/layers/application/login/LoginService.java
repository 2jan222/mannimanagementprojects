package at.fhv.ftb.inf.vz.semester4.groupc.layers.application.login;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.bus_assignment.BusAssignmentView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.driver_assigment.DriverAssignmentView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.homescreen.HomeScreenView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.path.PathCreationView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.views.tour.TourView;
import com.airhacks.afterburner.views.FXMLView;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * LoginService.
 * Deals with Login
 */
@LoggerableClassDisplayName("[LoginService]")
public class LoginService {
    private HashMap<String, String> map = new HashMap<>();

    {
        map.put("admin", "admin");
        map.put("username", "password");
        map.put("", "");
        /*
        List<WebDriverEntity> all = PersistenceFacade.getInstance().getAll(WebDriverEntity.class);
        all.forEach(e -> {
            if (e.getRole().toLowerCase().contains("admin")) {
                map.put(e.getUsername(), e.getPasswordString().substring(6)); //Cut "{noop}" away
            }
        });
         */
    }

    public boolean validate(String username, String password) {
        return map.containsKey(username) && password.equals(map.get(username));
    }

    public LinkedList<FXMLView> getViewsForUser() {
        //TODO GET PERMITTED VIEWS FOR USER
        LinkedList<FXMLView> views = new LinkedList<>();
        views.add(new HomeScreenView());
        views.add(new BusAssignmentView());
        views.add(new DriverAssignmentView());
        views.add(new PathCreationView());
        views.add(new TourView());
        return views;
    }
}
