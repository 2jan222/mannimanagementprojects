package at.fhv.ftb.inf.vz.semester4.groupc.layers.presentation.dto.dtos;

import java.time.LocalDate;

/**
 * SuspendedDTO.
 */
public class SuspendedDTO {
    private Integer suspendedId;
    private Integer busId;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private String cause;
    private BusDTO bus;

    public Integer getSuspendedId() {
        return suspendedId;
    }

    public void setSuspendedId(Integer suspendedId) {
        this.suspendedId = suspendedId;
    }

    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public BusDTO getBus() {
        return bus;
    }

    public void setBus(BusDTO bus) {
        this.bus = bus;
    }

    @Override
    public String toString() {
        return "SuspendedDTO{" +
                "suspendedId=" + suspendedId +
                ", busId=" + busId +
                ", dateFrom=" + dateFrom +
                ", dateTo=" + dateTo +
                ", cause='" + cause + '\'' +
                ", bus=" + bus +
                '}';
    }
}
