package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.OperationEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = {RouteRideEntityFastMapper.class, BusEntityMapper.class})
public interface OperationEntityMapper {

    OperationEntityMapper INSTANCE = Mappers.getMapper(OperationEntityMapper.class);


    @Mapping(target = "bus", source = "busByBusId")
    @Mapping(target = "routeRides", source = "routeRidesByOperationId")
    Operation toMojo(OperationEntity operationEntity);

    @Mapping(target = "routeRidesByOperationId", source = "routeRides")
    @Mapping(target = "busByBusId", source = "bus")
    OperationEntity toPojo(Operation operation);

    @Mapping(target = "bus", source = "busByBusId")
    @Mapping(target = "routeRides", source = "routeRidesByOperationId")
    Operation updateMojo(OperationEntity operationEntity, @MappingTarget Operation operation);

    @Mapping(target = "routeRidesByOperationId", source = "routeRides")
    @Mapping(target = "busByBusId", source = "bus")
    void updatePojo(Operation operation, @MappingTarget OperationEntity operationEntity);

    List<OperationEntity> toPojos(List<Operation> operations);

    List<Operation> toMojos(List<OperationEntity> operationEntities);

}
