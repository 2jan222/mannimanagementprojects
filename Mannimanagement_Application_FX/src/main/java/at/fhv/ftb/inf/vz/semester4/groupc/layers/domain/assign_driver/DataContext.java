package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_driver;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Bus;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.Driver;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.OperationFlat;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos.ShiftEntry;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.LoggerableColor;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.TestOnly;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Observable;

public class DataContext extends Observable {
    private static Loggerable logger = Loggerable.getInstance();
    private final LocalDate date;
    private HashMap<Integer, OperationFlat> operationById;
    private HashMap<Integer, LinkedList<ShiftEntry>> shiftsByOperationId;
    private HashMap<Integer, LinkedList<ShiftEntry>> shiftsByDriver;
    private HashMap<Integer, ShiftEntry> shiftEntryById;
    private HashMap<Integer, Driver> driverByDriverId;
    private HashMap<StationIDPair, Integer> timesFromPrevious;
    private SimpleBooleanProperty isSaved;

    @TestOnly
    public DataContext(HashMap<StationIDPair, Integer> timesFromPrevious) {
        this.timesFromPrevious = timesFromPrevious;
        date = null;
    }

    public DataContext(LocalDate date, HashMap<Integer, OperationFlat> operationById, HashMap<Integer,
            LinkedList<ShiftEntry>> shiftsByOperationId, HashMap<Integer,
            LinkedList<ShiftEntry>> shiftsByDriver, HashMap<Integer, ShiftEntry> shiftEntryById, HashMap<Integer, Driver> driverByDriverId,
                       SimpleBooleanProperty isSaved, HashMap<StationIDPair, Integer> timesFromPrevious) {
        this.date = date;
        this.operationById = operationById;
        this.shiftsByOperationId = shiftsByOperationId;
        this.shiftsByDriver = shiftsByDriver;
        this.driverByDriverId = driverByDriverId;
        this.shiftEntryById = shiftEntryById;
        this.isSaved = isSaved;
        this.timesFromPrevious = timesFromPrevious;
    }

    public void addShift(@NotNull ShiftEntry shift) {
        shiftsByOperationId.get(shift.getOperationId()).add(shift);
        shiftEntryById.put(shift.getShiftEntryId(), shift);
        if (shift.getDriver() != null) {
            shiftsByDriver.putIfAbsent(shift.getDriverId(), new LinkedList<>());
            shiftsByDriver.get(shift.getDriverId()).add(shift);
        }
        logger.debug("Added Shift");
        isSaved.setValue(true);
        setChanged();
        notifyObservers();
        shiftEntryById.values().forEach(e -> logger.debug(e, LoggerableColor.ANSI_BLUE));
    }


    public LinkedList<ShiftEntry> getShiftEntriesForDriver(Driver driver) {
        return shiftsByDriver.getOrDefault(driver, new LinkedList<>());
    }


    public LocalDate getDate() {
        return date;
    }

    public Integer getBusIdByOperationId(Integer operationId) {
        return (operationById.containsKey(operationId)) ? operationById.get(operationId).getBusId() : null;
    }

    public LinkedList<ShiftEntry> getShiftEntriesForOperation(Integer operationId) {
        return shiftsByOperationId.getOrDefault(operationId, new LinkedList<>());
    }

    @Nullable
    @Contract(value = "!null -> new", pure = true)
    public static DataContextBuilder builder(LocalDate date) {
        return (date != null) ? new DataContextBuilder(date) : null;
    }

    HashMap<StationIDPair, Integer> getMapTimeFromPrevious() {
        return timesFromPrevious;
    }

    public Integer getTimeFromPrevious(int x, int y) {
        return timesFromPrevious.get(new StationIDPair(x, y));
    }


    public void assignDriverToShift(@NotNull Driver driver, @NotNull ShiftEntry shiftEntry) {
        assert (Objects.equals(driver.getDriverId(), shiftEntry.getDriverId()));
        shiftsByDriver.compute(driver.getDriverId(), (key, value) -> {
            if (value == null) { //Should never be null, but better be save than sorry
                value = new LinkedList<>();
            }
            value.add(shiftEntry);
            return value;
        });
        logger.debug("Shift by driver check: " + shiftsByDriver.get(driver.getDriverId()).contains(shiftEntry));
    }

    public void removeDriverFromShift(Integer driverId, @NotNull ShiftEntry shiftEntry) {
        LinkedList<ShiftEntry> shiftsOfDriver = shiftsByDriver.get(driverId);
        if (shiftsOfDriver != null) {
            boolean removeSuccess = shiftsOfDriver.remove(shiftEntry);
            if (!removeSuccess) {
                logger.warn("Removal of driver from shift");
            }
        } else {
            logger.warn("Tried to update non existing driver at this date");
        }
    }

    public LinkedList<Driver> getDrivers() {
        return new LinkedList<>(driverByDriverId.values());
    }

    public LinkedList<Integer> getOperationIds() {
        return new LinkedList<>(operationById.keySet());
    }

    public boolean hasShiftEntry(@NotNull ShiftEntry shiftEntry) {
        return shiftsByOperationId.containsKey(shiftEntry.getOperationId());
    }

    public Driver getDriverById(Integer driverId) {
        return driverByDriverId.get(driverId);
    }

    public ShiftEntry getShiftEntryById(Integer shiftId) {
        return shiftEntryById.get(shiftId);
    }

    //NOT READY TO USE !!!
    public boolean getSaveState() {
        return isSaved.get();
    }

    public void registerSaveStateListener(ChangeListener<Boolean> changeListener) {
        isSaved.addListener(changeListener);
    }
    ///////

    @Override
    public String toString() {
        return "DataContext{" +
                "date=" + date +
                ", operationById=" + operationById +
                ", shiftsByOperationId=" + shiftsByOperationId +
                ", shiftsByDriver=" + shiftsByDriver +
                ", driverByDriverId=" + driverByDriverId +
                ", timesFromPrevious=" + timesFromPrevious +
                ", isSaved=" + isSaved +
                '}';
    }

    public Bus getBusByOperationId(Integer operationId) {
        OperationFlat operationFlat = getOperationById(operationId);
        return (operationFlat != null) ? operationFlat.getBus() : null;
    }

    @Nullable
    @Contract(pure = true)
    private OperationFlat getOperationById(Integer operationId) {
        return operationById.get(operationId);
    }

    public OperationFlat getOperationByOperationId(Integer operationId) {
        return operationById.get(operationId);
    }
}
