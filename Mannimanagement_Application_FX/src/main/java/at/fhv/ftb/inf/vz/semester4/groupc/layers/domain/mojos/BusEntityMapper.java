package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.BusEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface BusEntityMapper {

    BusEntityMapper INSTANCE = Mappers.getMapper(BusEntityMapper.class);

    Bus toMojo(BusEntity busEntity);

    @Mapping(target = "suspendedsByBusId", ignore = true)
   // @Mapping(target = "routeRidesByBusId", ignore = true)
    @Mapping(target = "registrationDate", ignore = true)
    @Mapping(target = "operationsByBusId", ignore = true)
    @Mapping(target = "note", ignore = true)
    @Mapping(target = "maintenanceKm", ignore = true)
    BusEntity toPojo(Bus bus);

    void updateMojo(BusEntity busEntity, @MappingTarget Bus bus);

    @Mapping(target = "suspendedsByBusId", ignore = true)
    //@Mapping(target = "routeRidesByBusId", ignore = true)
    @Mapping(target = "registrationDate", ignore = true)
    @Mapping(target = "operationsByBusId", ignore = true)
    @Mapping(target = "note", ignore = true)
    @Mapping(target = "maintenanceKm", ignore = true)
    void updatePojo(Bus bus, @MappingTarget BusEntity busEntity);

    List<Bus> toMojos(List<BusEntity> busEntities);

    List<BusEntity> toPojos(List<Bus> buses);


}
