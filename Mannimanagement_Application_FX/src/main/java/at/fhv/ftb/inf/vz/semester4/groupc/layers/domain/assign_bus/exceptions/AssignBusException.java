package at.fhv.ftb.inf.vz.semester4.groupc.layers.domain.assign_bus.exceptions;

/**
 * AssignBusException thrown in Grouper.
 */

public class AssignBusException extends RuntimeException {
    private AssignBusErrors error;

    public AssignBusException(AssignBusErrors error) {
        this.error = error;
    }

    public AssignBusErrors getError() {
        return error;
    }
}
