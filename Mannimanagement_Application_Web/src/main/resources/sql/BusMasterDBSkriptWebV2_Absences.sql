﻿DROP TABLE IF EXISTS operation_shift_ride;
DROP TABLE IF EXISTS operation_ride;
DROP TABLE IF EXISTS route_ride;
DROP TABLE IF EXISTS operation_shift;
DROP TABLE IF EXISTS standby_ride;
DROP TABLE IF EXISTS shift_ride;
DROP TABLE IF EXISTS ride;
DROP TABLE IF EXISTS path_station;
DROP TABLE IF EXISTS station_connection;
DROP TABLE IF EXISTS start_time;
DROP TABLE IF EXISTS path;
DROP TABLE IF EXISTS driving_licenses;
DROP TABLE IF EXISTS suspended;
DROP TABLE IF EXISTS operation;
DROP TABLE IF EXISTS absence;
DROP TABLE IF EXISTS shift;
DROP TABLE IF EXISTS web_driver;
DROP TABLE IF EXISTS holiday_wish;
DROP TABLE IF EXISTS shift_wish;
DROP TABLE IF EXISTS route;
DROP TABLE IF EXISTS station;
DROP TABLE IF EXISTS driver;
DROP TABLE IF EXISTS bus;



--Level 0


CREATE TABLE bus(
    	bus_id SERIAL PRIMARY KEY,
	maintenance_km integer NOT NULL, 
	licence_number varchar(255) UNIQUE, 
	make varchar(255), 
	model varchar(255),
	note varchar(255),
	registration_date date,
	seat_places integer NOT NULL, 
	stand_places integer NOT NULL
);

CREATE TABLE driver(
	driver_id SERIAL PRIMARY KEY,
	address varchar(255),
	birthday date, 
	email varchar(255), 
	employment_type integer, 
	firstname varchar(255),
	lastname varchar(255), 
	job_description varchar(255),
	telephonenumber varchar(255)
);

CREATE TABLE station(
	station_id SERIAL PRIMARY KEY,
	station_name varchar(255),
	short_name varchar(50)
);

CREATE TABLE route(
	route_id SERIAL PRIMARY KEY,
	route_number integer NOT NULL,
	valid_from date,
	valid_to date,
	variation varchar(255)
	
);

CREATE TABLE shift(
	shift_id SERIAL PRIMARY KEY
);

--Level 1

CREATE TABLE web_driver(
	web_driver_id SERIAL PRIMARY KEY,
	driver_id integer NOT NULL,
	username varchar(255),
	password_string varchar(255),
	role varchar(255),
	CONSTRAINT FK_WebDriver FOREIGN KEY (driver_id) REFERENCES driver (driver_id)
);

CREATE TABLE holiday_wish(
	holiday_wish_id SERIAL PRIMARY KEY,
	driver_id integer NOT NULL,
	start_date date NOT NULL,
	end_date date NOT NULL,
	state_ordinal integer NOT NULL DEFAULT 0,
	CONSTRAINT FK_Holliday_Driver FOREIGN KEY (driver_id) REFERENCES driver (driver_id)
);

CREATE TABLE shift_wish(
	shift_wish_id SERIAL PRIMARY KEY,
	driver_id integer NOT NULL,
	date date NOT NULL,
	is_positive boolean NOT NULL, 
	negative_cause varchar(255),
	shift_type integer NOT NULL,
	state_ordinal integer NOT NULL DEFAULT 0,
	CONSTRAINT FK_Holliday_Driver FOREIGN KEY (driver_id) REFERENCES driver (driver_id)
);

CREATE TABLE absence(
	absence_id SERIAL PRIMARY KEY,
	driver_id integer NOT NULL,
	date_from date NOT NULL, 
	date_to date NOT NULL,
	CONSTRAINT FK_VacationDriver FOREIGN KEY (driver_id) REFERENCES driver (driver_id)
);

CREATE TABLE operation(
	operation_id SERIAL PRIMARY KEY,
	date date,
	bus_id integer,
	CONSTRAINT FK_OperationBus FOREIGN KEY (bus_id) REFERENCES bus (bus_id)
	
);

CREATE TABLE suspended(
	suspended_id SERIAL PRIMARY KEY,
	bus_id integer NOT NULL,
	date_from date,
	date_to date,
	cause varchar(255),
	CONSTRAINT FK_SuspendedBus FOREIGN KEY (bus_id) REFERENCES bus (bus_id)
);

CREATE TABLE driving_licenses(
	driver_id  integer PRIMARY KEY,
	driving_licenses varchar(255)

);

CREATE TABLE path(
	path_id SERIAL PRIMARY KEY,
	path_description varchar(255),
	route_id integer NOT NULL,
	isretour boolean DEFAULT false NOT NULL,
	CONSTRAINT FK_RoutePath FOREIGN KEY (route_id) REFERENCES route (route_id)
);

CREATE TABLE start_time(
	start_time_id SERIAL PRIMARY KEY,
	required_capacity integer NOT NULL,
	start_time time without time zone,
	start_time_type integer,
	path_id integer NOT NULL,
	CONSTRAINT FK_StartTimePath FOREIGN KEY (path_id) REFERENCES path (path_id)
);

CREATE TABLE station_connection(
	station_connection_id SERIAL PRIMARY KEY,
	station_start integer NOT NULL,
	station_end integer NOT NULL,
	duration integer,
	distance integer,
	CONSTRAINT FK_StationConnectionStationStart FOREIGN KEY (station_start) REFERENCES station (station_id),
	CONSTRAINT FK_StationConnectionStationEnd FOREIGN KEY (station_end) REFERENCES station (station_id)
);

--Level 2

CREATE TABLE path_station(
	path_station_id SERIAL PRIMARY KEY,
	station_connection_id integer NOT NULL,
	position_on_path integer NOT NULL,
	path_id integer NOT NULL,
	CONSTRAINT FK_PathStationStationConnection FOREIGN KEY (station_connection_id) REFERENCES station_connection (station_connection_id),
	CONSTRAINT FK_PathStationPath FOREIGN KEY (path_id) REFERENCES path (path_id)
);

/*
CREATE TABLE ride(
	ride_id SERIAL PRIMARY KEY,
	bus_id integer, 
	operation_id integer,
	--date date,
	start_date_time timestamp without time zone, 
	end_date_time timestamp without time zone, 
	CONSTRAINT FK_RideBus FOREIGN KEY (bus_id) REFERENCES bus (bus_id),
	CONSTRAINT FK_RideOperation FOREIGN KEY (operation_id) REFERENCES operation (operation_id)
);


CREATE TABLE shift_ride(
	shift_id bigint NOT NULL,
	ride_id bigint NOT NULL,
	PRIMARY KEY (shift_id, ride_id),
	CONSTRAINT FK_ShiftRideShift FOREIGN KEY (shift_id) REFERENCES shift (shift_id),
	CONSTRAINT FK_ShiftRideRide FOREIGN KEY (ride_id) REFERENCES ride (ride_id)
);

CREATE TABLE standby_ride(
	standby_ride_id SERIAL PRIMARY KEY,
	bus_id integer NOT NULL,
	operation_id integer NOT NULL,
	start_date_time timestamp without time zone, 
	end_date_time timestamp without time zone,
	CONSTRAINT FK_StandbyRideBus FOREIGN KEY (bus_id) REFERENCES bus (bus_id),
	CONSTRAINT FK_StandbyRideOperation FOREIGN KEY (operation_id) REFERENCES operation (operation_id)
);
*/

CREATE TABLE route_ride(
	route_ride_id SERIAL PRIMARY KEY,
	--bus_id integer,
	--operation_id integer,
	--route_ride_date date,
	route_id integer NOT NULL,
	start_time_id integer NOT NULL,
	--operation_shift_id integer,
	--CONSTRAINT FK_RouteRideBus FOREIGN KEY (bus_id) REFERENCES bus (bus_id),
	--CONSTRAINT FK_RouteRideOperation FOREIGN KEY (operation_id) REFERENCES operation (operation_id),
	CONSTRAINT FK_RouteRideRoute FOREIGN KEY (route_id) REFERENCES route (route_id),
	CONSTRAINT FK_RouteRideStartTime FOREIGN KEY (start_time_id) REFERENCES start_time (start_time_id)
	--CONSTRAINT FK_RouteRideOperationShift FOREIGN KEY (operation_shift_id) REFERENCES operation_shift (operation_shift_id)
);

--Level 3

CREATE TABLE operation_shift(
	operation_shift_id SERIAL PRIMARY KEY,
	date date NOT NULL,
	start_time time without time zone,
	end_time time without time zone,
	operation_id integer,
	driver_id integer, 
	bus_id integer,
	CONSTRAINT FK_OperationShiftOperation FOREIGN KEY (operation_id) REFERENCES operation (operation_id),
	CONSTRAINT FK_OperationShiftDriver FOREIGN KEY (driver_id) REFERENCES driver (driver_id), 
	CONSTRAINT FK_OperationShiftBus FOREIGN KEY (bus_id) REFERENCES bus (bus_id)
	
);
CREATE TABLE operation_ride (
	operation_ride_id SERIAL,
 	operation_id integer,
	route_ride_id integer,
	CONSTRAINT PK_OperationRide PRIMARY KEY (operation_ride_id),
	CONSTRAINT FK_OperationRideOperation FOREIGN KEY (operation_id) REFERENCES operation(operation_id),
	CONSTRAINT FK_OperationRideRide FOREIGN KEY (route_ride_id) REFERENCES route_ride(route_ride_id)

);

CREATE TABLE operation_shift_ride(
	operation_shift_id integer,
	operation_ride_id integer,
	CONSTRAINT PK_OperationShiftRide PRIMARY KEY (operation_shift_id, operation_ride_id),
	CONSTRAINT FK_OperationRideOperation FOREIGN KEY (operation_shift_id) REFERENCES operation_shift(operation_shift_id),
	CONSTRAINT FK_OperationRideRide FOREIGN KEY (operation_ride_id) REFERENCES operation_ride(operation_ride_id)
);





--Level 4

--Level 5

--Inserts

INSERT INTO public.station(short_name,station_name) VALUES
 ('BHF-WEST','Bahnhof West')
,('BHF-OST','Bahnof Ost')
,('BHF-SUED','Bahnhof Sued')
,('BHF-NORD','Bahnhof Nord')
,('SH','Seehof')
,('SB','Saegerbruecke')
,('MG','Messegelände')
,('BP','Busplatz')
,('KH','Kirchhof')
,('MP','Marktplatz')
,('HP','Hofplatz')
,('WP','Wirtpark')
,('FH','Friedhoffen')
,('AB','Aberhof')
,('AH','Ackerhof')
,('GA','Gemeindeamt')
,('LKH','Landeskrankenhaus')
,('SBD','Strandbad')
,('ZM','Zentrum')
,('GB','Gewerbehof')
,('RH','Rathaus')
,('HSP','Hochschulplatz')
,('MI','Miralbellplatz')
,('BW','Bärenwirt')
,('FHP','Ferdinand-Hanusch-Platz')
,('LBH','Lehrbauhof')
,('MRW','Mueller-Rundegg-Weg')
,('HL','Hochleitner')
,('VS','Volksschule')
,('IG','Industriegebiet')
,('WSP','Wirtschaftspark')
,('MAG','Marktgasse')
,('ACH','Ach')
,('MSE','Musikschule')
,('SPZ','Stadtplatz')
,('BH','Bildnerhof')
,('KW','Kornweg')
,('HS','Holzstrasse')
,('STG','Steckgasse')
,('RA','Rosenau')
,('WDF','Wiesendorf')
,('NM','Neumarkt');

INSERT INTO public.bus(maintenance_km,licence_number,make,model,registration_date,seat_places,stand_places) VALUES
 (12000,'K-A12-B3','mercedes-benz','O 405 N','2004-09-11',38,68)
,(12001,'K-A12-B4','mercedes-benz','O 405 N','2007-06-23',38,68)
,(12002,'K-A12-B5','mercedes-benz','O 405 N','2010-04-03',38,68)
,(12003,'K-A12-B6','mercedes-benz','O 405 N','2013-01-12',38,68)
,(12004,'K-A12-B7','mercedes-benz','O 405 N','2015-10-24',38,68)
,(12005,'K-A12-B8','mercedes-benz','O 405 N','2018-08-04',38,68)
,(12006,'K-A12-B9','mercedes-benz','O 405 N','2012-05-15',38,68)
,(12007,'K-A12-B10','mercedes-benz','O 405 N','1999-02-24',38,68)
,(12008,'K-A12-B11','mercedes-benz','O 405 N','2008-12-05',38,68)
,(12009,'K-A12-B12','mercedes-benz','O 405 N','2002-09-15',38,68)
,(12010,'K-A12-B13','mercedes-benz','O 405 N','2014-06-26',38,68)
,(12011,'K-A12-B14','mercedes-benz','O 405 N','2000-04-07',38,68)
,(12012,'K-A12-B15','mercedes-benz','O 405 N','1996-01-16',38,68)
,(12013,'K-A12-B16','mercedes-benz','O 405 N','2015-10-27',38,68)
,(12014,'K-A12-B17','mercedes-benz','O 405 N','2010-08-08',38,68)
,(12015,'K-A12-B18','mercedes-benz','O 405 N','2015-05-19',38,68)
,(12016,'K-A12-B19','mercedes-benz','O 405 N','2018-02-27',38,68)
,(12017,'K-A12-B20','mercedes-benz','O 405 N','2016-12-09',38,68)
,(12018,'K-A12-B21','mercedes-benz','O 404','2017-09-19',61,0)
,(12019,'K-A12-B22','mercedes-benz','O 404','2014-06-30',61,0)
,(12020,'K-A12-B23','mercedes-benz','O 404','2006-04-10',61,0)
,(12021,'K-A12-B24','mercedes-benz','O 404','2002-01-20',61,0)
,(12022,'K-A12-B25','mercedes-benz','O 404','2011-10-31',61,0)
,(12023,'K-A12-B26','mercedes-benz','O 404','2003-08-11',61,0)
,(12024,'K-A12-B27','mercedes-benz','O 404','2014-05-23',61,0)
,(12025,'K-A12-B28','mercedes-benz','O 404','2003-03-03',61,0)
,(12026,'K-A12-B29','mercedes-benz','O 404','2009-12-12',61,0)
,(12027,'K-A12-B30','mercedes-benz','O 404','2015-09-23',61,0)
,(12028,'K-A12-B31','mercedes-benz','O 404','2000-07-04',61,0)
,(12029,'K-A12-B32','mercedes-benz','O 404','1999-04-14',61,0)
,(12030,'K-A12-B33','mercedes-benz','O 404','2016-01-24',61,0)
,(12031,'K-A12-B34','mercedes-benz','O 404','2090-11-04',61,0)
,(12032,'K-A12-B35','mercedes-benz','O 404','2013-08-15',61,0)
,(12033,'K-A12-B36','mercedes-benz','O 404','2004-05-26',61,0)
,(12034,'K-A12-B37','mercedes-benz','O 404','2009-03-07',61,0)
,(12035,'K-A12-B38','mercedes-benz','O 404','2001-12-17',61,0)
,(12036,'K-A12-B39','mercedes-benz','O 404','2004-09-27',61,0)
,(12037,'K-A12-B40','mercedes-benz','O 404','2007-07-09',61,0)
,(12038,'K-A12-B41','mercedes-benz','O 530 Citaro','2010-04-19',68,90)
,(12039,'K-A12-B42','mercedes-benz','O 530 Citaro','2013-01-28',68,90)
,(12040,'K-A12-B43','mercedes-benz','O 530 Citaro','2015-11-09',68,90)
,(12041,'K-A12-B44','mercedes-benz','O 530 Citaro','2018-08-20',68,90)
,(12042,'K-A12-B45','mercedes-benz','O 530 Citaro','2009-05-31',68,90)
,(12043,'K-A12-B46','mercedes-benz','O 530 Citaro','2012-03-11',68,90)
,(12044,'K-A12-B47','mercedes-benz','O 530 Citaro','2012-12-21',68,90)
,(12045,'K-A12-B48','mercedes-benz','O 530 Citaro','2010-10-01',68,90)
,(12046,'K-A12-B49','mercedes-benz','O 530 Citaro','2004-07-12',68,90)
,(12047,'K-A12-B50','mercedes-benz','O 530 Citaro','2013-04-23',68,90)
,(12048,'K-A12-B51','mercedes-benz','O 530 Citaro','2005-02-01',68,90)
,(12049,'K-A12-B52','mercedes-benz','O 530 Citaro','2007-11-12',68,90)
,(12050,'K-A12-B53','mercedes-benz','O 530 Citaro','2012-08-24',68,90)
,(12051,'K-A12-B54','mercedes-benz','O 530 Citaro','2011-06-04',68,90)
,(12052,'K-A12-B55','mercedes-benz','O 530 Citaro','2004-03-15',68,90)
,(12053,'K-A12-B56','mercedes-benz','O 530 Citaro','2008-12-25',68,90)
,(12054,'K-A12-B57','mercedes-benz','O 530 Citaro','2006-10-05',68,90)
,(12055,'K-A12-B58','mercedes-benz','O 530 Citaro','2017-07-16',68,90)
,(12056,'K-A12-B59','mercedes-benz','O 530 Citaro','2011-04-26',68,90)
,(12057,'K-A12-B60','mercedes-benz','O 530 Citaro','2005-02-05',68,90)
,(12058,'K-A12-B61','mercedes-benz','O 530 Citaro','2006-10-05',68,90)
,(12059,'K-A12-B62','mercedes-benz','O 530 Citaro','2017-07-16',68,90);

INSERT INTO public.route(route_number,variation,valid_from,valid_to) VALUES
 (22,NULL,'2019-01-01','2019-12-31')
,(20,NULL,'2019-01-01','2019-12-31')
,(21,NULL,'2019-01-01','2020-12-31')
,(10,NULL,'2019-01-01','2020-12-31')
,(13,NULL,'2019-01-01','2019-12-31')
,(53,NULL,'2018-01-01','2019-12-31')
,(23,NULL,'2018-01-01','2019-12-31')
,(1,NULL,'2017-01-01','2019-12-31')
,(47,NULL,'2019-01-01','2020-12-31')
,(52,NULL,'2018-01-01','2020-12-31')
,(18,NULL,'2019-01-01','2020-12-31')
,(16,NULL,'2019-01-01','2020-12-31')
,(40,NULL,'2019-01-01','2020-12-31')
,(9,NULL,'2019-01-01','2019-12-31');

INSERT INTO public.path(path_description,route_id) VALUES
 ('Uebersee',1)
,('Buchen',2)
,('Sachsen',3)
,('Wuerdberg',4)
,('Ktour',5)
,('Stadttour',6)
,('Seetour',7)
,('Schulweg',8)
,('Siegerpfad',9)
,('Silberpfad',10)
,('Gasthausen',11)
,('Khausen',12)
,('Stadttour',13)
,('Seetour',14);

-- WEEKDAY = 0
-- SATURDAY = 1
-- SCHOOLDAY = 2
-- HOLIDAY = 3
-- 
INSERT INTO public.start_time(required_capacity,start_time,start_time_type,path_id) VALUES
 (50,'08:45:00',0,1)
,(120,'08:45:00',2,2)
,(80,'08:45:00',3,3)
,(40,'08:45:00',1,4)
,(120,'08:45:00',0,5)
,(100,'08:45:00',0,6)
,(50,'08:45:00',3,1)
,(100,'08:45:00',0,2)
,(110,'08:45:00',0,3)
,(90,'08:45:00',0,4)
,(80,'08:45:00',3,5)
,(50,'08:45:00',1,6)
,(100,'08:45:00',0,1)
,(100,'08:45:00',0,2)
,(120,'08:45:00',2,3)
,(70,'08:45:00',0,4)
,(70,'08:45:00',0,5)
,(90,'08:45:00',0,6)
,(90,'08:45:00',0,6)
,(50,'09:00:00',0,1)
,(50,'09:15:00',0,1)
,(50,'09:30:00',0,1)
,(50,'09:45:00',0,1)
,(100,'10:00:00',0,1)
,(50,'10:15:00',0,1)
,(50,'10:30:00',0,1)
,(50,'06:00:00',0,2)
,(50,'06:08:00',0,2)
,(50,'06:16:00',0,2)
,(50,'06:24:00',0,2)
,(50,'06:32:00',0,2)
,(50,'06:40:00',0,2)
,(50,'06:48:00',0,2)
,(50,'06:56:00',0,2)
,(50,'07:04:00',0,2)
,(50,'07:12:00',0,2)
,(50,'07:20:00',0,2)
,(50,'07:28:00',0,2)
,(50,'07:36:00',0,2)
,(50,'07:44:00',0,2)
,(50,'07:52:00',0,2)
,(50,'08:00:00',0,2)
,(50,'09:00:00',3,3)
,(50,'10:00:00',3,3)
,(50,'11:00:00',3,3)
,(50,'12:00:00',3,3)
,(50,'09:00:00',1,4)
,(50,'10:00:00',1,4)
,(50,'11:00:00',1,4)
,(50,'12:00:00',1,4)
,(50,'08:00:00',1,5)
,(50,'09:00:00',1,5)
,(50,'10:00:00',1,5)
,(50,'11:00:00',1,5)
,(50,'12:00:00',1,5)
,(50,'13:00:00',1,5)
,(50,'14:00:00',1,5)
,(50,'15:00:00',1,5)
,(50,'16:00:00',1,5)
,(50,'17:00:00',1,5)
,(50,'11:00:00',0,1)
,(50,'11:30:00',0,1)
,(50,'12:00:00',0,1)
,(50,'12:30:00',0,1)
,(50,'13:00:00',0,1)
,(50,'13:30:00',0,1)
,(50,'14:00:00',0,1)
,(50,'14:30:00',0,1)
,(50,'15:00:00',0,1)
,(50,'15:30:00',0,1)
,(50,'16:00:00',0,1)
,(50,'16:30:00',0,1)
,(50,'17:00:00',0,1)
,(50,'17:30:00',0,1)
,(50,'18:00:00',0,1)
,(50,'18:30:00',0,1)
,(50,'19:00:00',0,1)
,(50,'19:30:00',0,1)
,(50,'20:00:00',0,1)
,(50,'20:30:00',0,1)
,(50,'21:00:00',0,1)
,(50,'21:30:00',0,1)
,(50,'22:00:00',0,1); 

INSERT INTO public.station_connection(station_start, station_end, duration, distance) VALUES
(1,5,5*60, 200)
,(5,7,2.2*60,300)
,(7,10,3*60,150)
,(10,8,1.5*60,175)
,(8,11,2.2*60,230)
,(11,15,2*60,250)
,(15,12,3*60,300)
,(12,4,2.5*60,340)
,(4,26,60,160)
,(26,27,60,175)
,(27,30,2*60,200)
,(30,9,2.5*60,150)
,(9,31,3.5*60,200)
,(31,42,2.1*60,180)
,(42,33,2.8*60,200)
,(33,41,2.5*60,250)
,(41,40,3*60,300)
,(40,35,2*60,220)
,(35,25,4*60,400)
,(25,36,1.2*60,170)
,(36,39,1.2*60,200)
,(39,28,3*60,350)
,(28,23,2*60,150)
,(23,33,3*60,190)
,(33,21,3*60,150)
,(21,18,4*60,300)
,(18,6,2*60,290)
,(6,5,3.8*60,400)
,(5,1,2.5*60,200)
,(4,7,3*60,200)
,(7,10,2*60,150)
,(10,31,2.3*60,210)
,(31,11,2.2*60,200)
,(11,32,2.1*60,190)
,(32,42,3.3*60,300)
,(42,30,3.2*60,180)
,(30,12,3*60,250)
,(12,13,3.3*60,380)
,(13,18,4*60,280)
,(18,15,3*60,300)
,(15,39,3*60,250)
,(39,29,4*60,400)
,(29,35,4*60,600)
,(35,25,3.6*60,400)
,(25,41,3.1*60,260)
,(41,14,2.4*60,270)
,(14,40,4*60,250)
,(2,20,3*60,300)
,(20,10,3*60,250)
,(10,24,3*60,290)
,(24,14,3.1*60,310)
,(14,21,2.5*60,300)
,(21,23,2.8*60,270)
,(23,11,2.8*60,310)
,(11,13,3.1*60,300)
,(13,30,2.9*60,285)
,(30,40,2.9*60,305)
,(40,41,3*60,290)
,(41,31,3.5*60,300)
,(31,33,2.8*60,280)
,(33,20,3*60,345)
,(20,21,3*60,300)
,(21,12,3.1*60,280)
,(12,18,2.4*60,260)
,(18,11,2.9*60,360)
,(11,40,3.8*60,380)
,(40,6,3.8*60,400)
,(6,42,4*60,390)
,(42,30,3*60,280)
,(3,42,2.5*60,290)
,(42,32,2.5*60,350)
,(32,40,3*60,360)
,(40,35,3*60,240)
,(35,8,2.8*60,285)
,(8,30,2.8*60,305)
,(30,9,3*60,315)
,(9,20,3*60,300)
,(20,41,3.1*60,295)
,(41,21,3.5*60,350)
,(21,36,2.8*60,280)
,(36,22,2.9*60,300)
,(22,14,3.5*60,305)
,(14,10,3.5*60,315);


INSERT INTO public.path_station(station_connection_id, position_on_path, path_id) VALUES
 (1,1,1)
,(2,2,1)
,(3,3,1)
,(4,4,1)
,(5,5,1)
,(6,6,1)
,(7,7,1)
,(8,8,1)
,(9,9,1)
,(10,10,1)
,(11,11,1)
,(12,12,1)
,(13,13,1)
,(14,14,1)
,(15,15,1)
,(16,16,1)
,(17,17,1)
,(18,18,1)
,(19,19,1)
,(20,1,2)
,(21,2,2)
,(22,3,2)
,(23,4,2)
,(24,5,2)
,(25,6,2)
,(26,7,2)
,(27,8,2)
,(28,9,2)
,(29,10,2)
,(30,1,3)
,(31,2,3)
,(32,3,3)
,(33,4,3)
,(34,5,3)
,(35,6,3)
,(36,7,3)
,(37,8,3)
,(37,9,3)
,(38,10,3)
,(39,11,3)
,(40,12,3)
,(41,13,3)
,(42,14,3)
,(43,15,3)
,(44,16,3)
,(45,17,3)
,(46,18,3)
,(47,1,4)
,(48,2,4)
,(49,3,4)
,(50,4,4)
,(51,5,4)
,(52,6,4)
,(53,7,4)
,(54,8,4)
,(55,9,4)
,(56,10,4)
,(57,11,4)
,(58,12,4)
,(59,1,5)
,(60,2,5)
,(61,3,5)
,(62,4,5)
,(63,5,5)
,(64,6,5)
,(65,7,5)
,(66,8,5)
,(67,9,5)
,(68,10,5)
,(69,1,6)
,(70,2,6)
,(71,3,6)
,(72,4,6)
,(73,5,6)
,(74,6,6)
,(75,7,6)
,(75,8,6)
,(76,9,6)
,(77,10,6)
,(78,11,6)
,(79,12,6)
,(80,13,6)
,(81,14,6);

INSERT INTO public.driver VALUES(DEFAULT, 'Stadtstrasse 10', '1999-09-09', 'frank.schmitz@gmx.at', 0, 'Franz', 'Schmitz', 'KlasseDE', '0664123456789');
INSERT INTO public.driver VALUES(DEFAULT, 'Koenigstrasse 84 Teistungen', '1973-11-14', 'swen.seiler@kbus.com', '1', 'Swen', 'Seiler', 'KlasseDE', '0360716998');
INSERT INTO public.driver VALUES(DEFAULT, 'Lange Strasse 25 München', '1991-04-28', 'lukas.wirtz@kbus.com', '1', 'Lukas', 'Wirtz', 'KlasseDE', '08801599438');
INSERT INTO public.driver VALUES(DEFAULT, 'Hardenbergstraße 48', '1980-01-04', 'andreas.finke@kbus.com', '1', 'Andreas', 'Finke', 'KlasseDE', '02604703932');
INSERT INTO public.driver VALUES(DEFAULT, 'Stuttgarter Platz 54 Sulzbach', '1976-09-29', 'jens.schaefer@kbus.com', '1', 'Jens', 'Schaefer', 'KlasseDE', '07721380119');
INSERT INTO public.driver VALUES(DEFAULT, 'Schönhauser Allee 87 Rietheim', '1964-08-22', 'johannes.drechsler@kbus.com', '1', 'Johannes', 'Drechsler', 'KlasseDE', '08633751615');
INSERT INTO public.driver VALUES(DEFAULT, 'Kieler Strasse 77 Tüsling', '1969-01-20', 'steffen.herman@kbus.com', '1', 'Steffen', 'Herman', 'KlasseDE', '09526632935');
INSERT INTO public.driver VALUES(DEFAULT, 'Luebeckerstrasse 28 Schonungen', '1977-07-02', 'markus.drescher@kbus.com', '1', 'Markus', 'Drescher', 'KlasseDE', '0431936231');
INSERT INTO public.driver VALUES(DEFAULT, 'Genterstrasse 94 Kiel', '1994-05-26', 'maximilian.friedmann@kbus.com', '1', 'Maximilian', 'Freidmann', 'KlasseDE', '02133296831');
INSERT INTO public.driver VALUES(DEFAULT, 'Am Borsigturm 15 Dromagen', '1987-06-02', 'kevin.eiffel@kbus.com', '1', 'Kevin', 'Eiffel', 'KlasseDE', '0211750746');
INSERT INTO public.driver VALUES(DEFAULT, 'Parkstrasse 3 Düsseldorf', '1975-02-16', 'matthias.borgler@kbus.com', '1', 'Matthias', 'Borgler', 'KlasseDE', '0318643813');


INSERT INTO public.absence(driver_id,date_from,date_to) VALUES (2, '2019-07-15','2019-07-30')
,(1, '2019-07-29', '2019-08-03')
,(2, '2019-08-09', '2019-08-09')
,(2, '2019-12-24', '2020-01-01')
,(3, '2019-07-15', '2019-07-30')
,(3, '2019-08-08', '2019-08-15')
,(4, '2019-07-01', '2019-07-09')
,(4, '2019-08-10', '2019-08-11')
,(4, '2019-09-15', '2019-09-19')
,(5, '2019-07-19', '2019-07-19')
,(5, '2019-08-15', '2019-08-29')
,(8, '2019-07-15', '2019-07-30');

INSERT INTO public.web_driver VALUES(DEFAULT, 1, 'frank.schmitz', '{noop}passwordFrank', 'ADMIN');
INSERT INTO public.web_driver VALUES(DEFAULT, 2, 'swen.seiler', '{noop}passwordSwen', 'USER');
INSERT INTO public.web_driver VALUES(DEFAULT, 3, 'lukas.wirtz', '{noop}passwordLukas', 'USER');
INSERT INTO public.web_driver VALUES(DEFAULT, 4, 'andreas.finke', '{noop}passwordAndreas', 'USER');
INSERT INTO public.web_driver VALUES(DEFAULT, 5, 'jens.schaefer', '{noop}passwordJens', 'USER');


INSERT INTO public.holiday_wish VALUES(DEFAULT, 2, '2019-07-09', '2019-07-21');
INSERT INTO public.holiday_wish VALUES(DEFAULT, 2, '2019-12-10', '2019-12-21');
INSERT INTO public.holiday_wish VALUES(DEFAULT, 4, '2019-09-10', '2019-09-10', 1);

INSERT INTO public.shift_wish VALUES(DEFAULT, 2, '2019-09-10', true, null, 1);
INSERT INTO public.shift_wish VALUES(DEFAULT, 2, '2019-09-11', false, 'Doctor', 2);
INSERT INTO public.shift_wish VALUES(DEFAULT, 2, '2019-09-12', true, null, 3, 1);


INSERT INTO operation (date) VALUES 
('2019-07-09'),
('2019-07-09'),
('2019-07-09'),
('2019-07-09'),
('2019-07-10'),
('2019-07-10'),
('2019-07-10'),
('2019-07-10'),
('2019-07-08'),
('2019-07-08'),
('2019-07-08'),
('2019-07-08'),
('2019-07-11'),
('2019-07-11'),
('2019-07-11'),
('2019-07-11'),
('2019-07-12'),
('2019-07-12'),
('2019-07-12'),
('2019-07-12'),
('2019-07-13'),
('2019-07-13'),
('2019-07-14'),
('2019-07-14'),
('2019-07-01'),
('2019-07-01'),
('2019-07-01'),
('2019-07-01'),
('2019-07-02'),
('2019-07-02'),
('2019-07-02'),
('2019-07-02'),
('2019-07-03'),
('2019-07-03'),
('2019-07-03'),
('2019-07-03'),
('2019-07-04'),
('2019-07-04'),
('2019-07-04'),
('2019-07-04'),
('2019-07-05'),
('2019-07-05'),
('2019-07-05'),
('2019-07-05'),
('2019-07-06'),
('2019-07-06'),
('2019-07-07'),
('2019-07-07');



INSERT INTO operation_shift(date, start_time, end_time, operation_id, driver_id, bus_id) VALUES ('2019-07-09', '8:00:00', '9:00:00', 1, NULL, 1)
,('2019-07-09', '9:00:00', '9:00:00', 2, NULL, 2)
,('2019-07-09', '8:00:00', '9:00:00', 3, NULL, 3)
,('2019-07-09', '8:00:00', '9:00:00', 4, NULL, 4);

INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 27);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 31);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 35);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 39);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 28);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 32);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 34);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 38);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 40);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 42);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 29);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 33);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 36);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 27);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 31);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 35);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 39);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 28);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 32);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 34);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 38);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 40);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 42);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 29);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 33);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 36);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 27);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 31);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 35);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 39);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 28);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 32);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 34);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 38);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 40);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 42);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 29);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 33);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 36);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 27);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 31);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 35);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 39);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 28);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 32);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 34);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 38);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 40);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 42);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 29);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 33);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 36);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 27);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 31);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 35);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 39);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 28);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 32);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 34);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 38);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 40);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 42);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 29);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 33);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 36);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 47);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 48);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 50);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 50);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 51);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 51);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 53);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 54);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 55);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 56);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 57);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 58);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 59);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 60);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 47);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 48);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 50);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 50);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 51);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 51);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 53);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 54);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 55);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 56);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 57);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 58);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 59);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 60);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 27);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 31);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 35);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 39);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 28);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 32);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 34);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 38);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 40);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 42);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 29);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 33);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 36);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 27);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 31);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 35);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 39);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 28);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 32);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 34);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 38);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 40);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 42);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 29);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 33);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 36);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 27);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 31);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 35);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 39);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 28);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 32);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 34);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 38);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 40);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 42);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 29);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 33);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 36);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 27);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 31);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 35);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 39);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 28);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 32);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 34);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 38);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 40);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 42);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 29);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 33);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 36);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 27);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 31);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 35);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 39);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 28);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 32);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 34);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 38);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 40);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 42);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 29);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 33);
INSERT INTO public.route_ride VALUES (DEFAULT, 5, 36);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 1);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 20);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 21);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 22);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 23);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 25);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 26);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 47);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 48);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 50);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 50);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 51);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 51);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 53);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 54);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 55);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 56);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 57);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 58);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 59);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 60);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 47);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 48);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 50);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 50);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 51);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 51);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 53);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 54);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 55);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 56);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 57);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 58);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 59);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 60);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 2);
INSERT INTO public.route_ride VALUES (DEFAULT, 9, 8);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 50);
INSERT INTO public.route_ride VALUES (DEFAULT, 7, 48);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 44);
INSERT INTO public.route_ride VALUES (DEFAULT, 3, 10);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 61);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 62);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 63);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 64);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 65);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 66);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 67);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 68);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 69);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 70);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 71);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 72);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 73);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 74);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 75);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 76);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 77);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 78);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 79);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 80);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 81);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 82);
INSERT INTO public.route_ride VALUES (DEFAULT, 1, 83);

     INSERT INTO public.operation_ride VALUES (DEFAULT, 1, 1);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 1, 2);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 1, 3);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 1, 4);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 1, 5);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 1, 6);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 1, 7);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 2, 8);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 2, 9);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 2, 10);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 2, 11);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 2, 12);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 2, 13);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 2, 14);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 2, 15);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 2, 16);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 3, 17);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 3, 18);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 3, 19);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 3, 20);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 4, 21);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 4, 22);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 4, 23);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 4, 24);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 4, 25);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 4, 26);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 4, 27);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 5, 28);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 5, 29);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 5, 30);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 5, 31);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 5, 32);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 5, 33);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 5, 34);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 6, 35);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 6, 36);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 6, 37);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 6, 38);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 6, 39);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 6, 40);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 6, 41);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 6, 42);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 6, 43);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 7, 44);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 7, 45);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 7, 46);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 7, 47);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 8, 48);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 8, 49);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 8, 50);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 8, 51);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 8, 52);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 8, 53);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 8, 54);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 9, 55);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 9, 56);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 9, 57);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 9, 58);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 9, 59);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 9, 60);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 9, 61);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 10, 62);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 10, 63);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 10, 64);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 10, 65);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 10, 66);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 10, 67);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 10, 68);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 10, 69);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 10, 70);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 11, 71);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 11, 72);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 11, 73);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 11, 74);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 12, 75);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 12, 76);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 12, 77);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 12, 78);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 12, 79);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 12, 80);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 12, 81);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 13, 82);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 13, 83);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 13, 84);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 13, 85);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 13, 86);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 13, 87);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 13, 88);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 14, 89);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 14, 90);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 14, 91);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 14, 92);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 14, 93);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 14, 94);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 14, 95);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 14, 96);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 14, 97);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 15, 98);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 15, 99);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 15, 100);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 15, 101);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 16, 102);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 16, 103);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 16, 104);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 16, 105);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 16, 106);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 16, 107);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 16, 108);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 17, 109);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 17, 110);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 17, 111);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 17, 112);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 17, 113);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 17, 114);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 17, 115);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 18, 116);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 18, 117);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 18, 118);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 18, 119);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 18, 120);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 18, 121);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 18, 122);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 18, 123);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 18, 124);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 19, 125);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 19, 126);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 19, 127);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 19, 128);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 20, 129);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 20, 130);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 20, 131);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 20, 132);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 20, 133);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 20, 134);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 20, 135);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 21, 136);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 21, 137);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 21, 138);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 21, 139);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 22, 140);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 22, 141);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 22, 142);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 22, 143);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 22, 144);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 22, 145);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 22, 146);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 22, 147);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 22, 148);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 22, 149);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 23, 150);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 23, 151);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 23, 152);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 23, 153);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 24, 154);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 24, 155);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 24, 156);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 24, 157);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 24, 158);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 24, 159);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 24, 160);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 24, 161);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 24, 162);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 24, 163);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 25, 164);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 25, 165);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 25, 166);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 25, 167);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 25, 168);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 25, 169);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 25, 170);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 26, 171);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 26, 172);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 26, 173);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 26, 174);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 26, 175);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 26, 176);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 26, 177);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 26, 178);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 26, 179);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 27, 180);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 27, 181);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 27, 182);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 27, 183);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 27, 184);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 28, 185);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 28, 186);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 28, 187);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 28, 188);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 28, 189);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 28, 190);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 29, 191);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 29, 192);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 29, 193);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 29, 194);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 29, 195);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 29, 196);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 29, 197);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 30, 198);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 30, 199);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 30, 200);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 30, 201);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 30, 202);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 30, 203);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 30, 204);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 30, 205);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 30, 206);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 31, 207);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 31, 208);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 31, 209);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 31, 210);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 32, 211);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 32, 212);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 32, 213);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 32, 214);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 32, 215);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 32, 216);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 32, 217);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 33, 218);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 33, 219);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 33, 220);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 33, 221);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 33, 222);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 33, 223);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 33, 224);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 34, 225);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 34, 226);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 34, 227);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 34, 228);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 34, 229);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 34, 230);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 34, 231);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 34, 232);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 34, 233);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 35, 234);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 35, 235);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 35, 236);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 35, 237);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 36, 238);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 36, 239);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 36, 240);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 36, 241);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 36, 242);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 36, 243);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 36, 244);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 37, 245);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 37, 246);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 37, 247);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 37, 248);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 37, 249);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 37, 250);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 37, 251);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 38, 252);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 38, 253);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 38, 254);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 38, 255);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 38, 256);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 38, 257);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 38, 258);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 38, 259);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 38, 260);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 39, 261);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 39, 262);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 39, 263);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 39, 264);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 40, 265);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 40, 266);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 40, 267);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 40, 268);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 40, 269);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 40, 270);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 40, 271);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 41, 272);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 41, 273);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 41, 274);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 41, 275);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 41, 276);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 41, 277);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 41, 278);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 42, 279);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 42, 280);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 42, 281);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 42, 282);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 42, 283);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 42, 284);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 42, 285);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 42, 286);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 42, 287);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 43, 288);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 43, 289);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 43, 290);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 43, 291);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 44, 292);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 44, 293);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 44, 294);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 44, 295);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 44, 296);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 44, 297);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 44, 298);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 45, 299);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 45, 300);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 45, 301);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 45, 302);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 46, 303);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 46, 304);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 46, 305);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 46, 306);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 46, 307);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 46, 308);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 46, 309);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 46, 310);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 46, 311);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 46, 312);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 47, 313);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 47, 314);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 47, 315);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 47, 316);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 48, 317);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 48, 318);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 48, 319);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 48, 320);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 48, 321);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 48, 322);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 48, 323);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 48, 324);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 48, 325);
        INSERT INTO public.operation_ride VALUES (DEFAULT, 48, 326);

INSERT INTO operation_shift_ride(operation_shift_id, operation_ride_id) VALUES (1, 1),
(1, 2)
,(1, 3)
,(1, 4)
,(1, 5)
,(1, 6)
,(1, 7)
,(2, 8)
,(2, 9)
,(2, 10)
,(2, 11)
,(2, 12)
,(2, 13)
,(2, 14)
,(2, 15)
,(2, 16)
,(3, 17)
,(3, 18)
,(3, 19)
,(3, 20)
,(4, 21)
,(4, 22)
,(4, 23)
,(4, 24)
,(4, 25)
,(4, 26)
,(4, 27);