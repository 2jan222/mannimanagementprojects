function reloadHolidayWishes(data) {
    document.getElementById("holidayWishes").innerHTML = data;
}

function delHolidayWish(id) {
    console.log("DELETE " + id);
    fetch('/user/holidayWish/delete?id=' + id).then(function (response) {
        return response.json();
    }).then(function (a) {
        console.log(a);
        reloadHolidayWishes(a.data);
    })
}

function reloadShiftWishes(data) {
    document.getElementById("shiftWishes").innerHTML = data;
}

function delShiftWish(id) {
    console.log("DELETE " + id);
    fetch('/user/shiftWish/delete?id=' + id).then(function (response) {
        return response.json();
    }).then(function (a) {
        console.log(a);
        reloadShiftWishes(a.data);
    })
}