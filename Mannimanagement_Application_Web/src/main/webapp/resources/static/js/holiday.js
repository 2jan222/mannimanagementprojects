var callByName = new Map();

class ResponseParser {
    static add(keyword, callback) {
        callByName.set(keyword, callback);
    }

    static parse(response) {
        if (response.errors.length > 0) {
            response.errors.forEach(ResponseParser.evaluate);
        }
        if (response.warnings.length > 0) {
            response.warnings.forEach(ResponseParser.evaluate);
        }
        if (response.information.length > 0) {
            response.information.forEach(ResponseParser.evaluate);
        }
    }

    static evaluate(val) {
        let call = callByName.get(val);
        if (call !== undefined) {
            call();
        }
    }
}

function handlers() {
    let dayInPastError = function () {
        alert("Date in Past !!")
    };
    let dateParseError = function () {
        alert("Date could not be parsed")
    };
    let dateOrderError = function () {
        alert("Start Date is after End Date")
    };
    let success = function () {
        let div = document.getElementById("alert");
        if (div.style.display === 'none') {
            div.style.display = '';
        } else {
            div.style.display = 'none';
        }
    };
    let overlapping = function () {
        alert("You already have a request in this time frame. We suggest to delete one request")
    };
    ResponseParser.add("DATE_OF_PAST_DAY", dayInPastError);
    ResponseParser.add("DATE_PARSE_EX", dateParseError);
    ResponseParser.add("DATES_ORDER_REVERSED", dateOrderError);
    ResponseParser.add("SUCCESS", success);
    ResponseParser.add("HOLIDAY_REQUEST_OVERLAPS_WITH_EXISTING_REQUEST", overlapping)
}

handlers();

function showForDate() {
    let startDate = document.getElementById("startDate");
    let endDate = document.getElementById("endDate");

    function showResponse(data) {
        console.log(data);
        let contentContainer = document.getElementById("content");
        contentContainer.innerHTML = data;
    }

    if (startDate !== undefined && startDate.value !== undefined && startDate.value !== "") {
        //console.log("Start " + startDate.value);
        if (endDate !== undefined && endDate.value !== undefined && endDate.value !== "") {
            //console.log("End " + endDate.value);
            if (startDate.value <= endDate.value) {
                //console.log("HERE");
                let url = `/user/holidayWishForm/requestHoliday?start=${startDate.value}&end=${endDate.value}`;
                //console.log("URL " + url);
                fetch(url).then(function (response) {
                    return response.json();
                }).then(function (responseBody) {
                    console.log(`FETCHED Response for Holiday Request[start=${startDate} , end=${endDate}]`);
                    console.log(responseBody);
                    ResponseParser.parse(responseBody);
                    showResponse(responseBody.data);
                });
            }
        } else {
            alert("End Date not defined");
        }
    } else {
        alert("Start Date not defined");
    }
}
