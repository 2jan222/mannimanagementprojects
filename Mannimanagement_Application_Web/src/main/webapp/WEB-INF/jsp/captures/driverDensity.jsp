<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div>
    <jsp:useBean id="map" scope="request" type="java.util.HashMap<java.lang.String, java.lang.Object>"/>
    <table>
        <thead>
        <tr>
            <th>
                Date
            </th>
            <th>
                Enough Drivers ?
            </th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${map.keySet()}" var="key">
            <tr>
                <td>
                        ${key}
                </td>
                <td>
                    <c:choose>
                        <c:when test="${map.get(key) == false}">
                            Yes
                        </c:when>
                        <c:otherwise>
                            No
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
