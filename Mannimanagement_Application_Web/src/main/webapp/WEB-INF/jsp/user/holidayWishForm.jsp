<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Vacation Preferences</title>
    <jsp:include page="../fragments/style.jsp"/>
    <script type="text/javascript" src="<c:url value="/resources/static/js/holiday.js" />"></script>
    <script>
        function alertShow() {
            showForDate();
        }
    </script>
</head>
<body>
<header>
    <h1 class="headerall">Vacation Preferences</h1>
</header>
<aside>
    <nav id="nav">
        <jsp:include page="../fragments/menu.jsp"/>
    </nav>
</aside>
<main>
    <div class="divForm" id="form">
        <form>
            <label for=startDate> Start: </label>
            <input type="date" id="startDate" name="startDate"  dataformatas="dd-MM-yyyy"><br/>
            <label for="endDate"> End: </label>
            <br>
            <input type="date" id="endDate" name="endDate" dataformatas="dd-MM-yyyy"><br/>
            <input class="send" type="button" value="SEND"  onclick="alertShow()">
        </form>
    </div>
    <div id="content">

    </div>
    <div class="alert" id="alert" style="display:none">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        REQUEST SENT
    </div>
</main>
<footer>
    <jsp:include page="../fragments/footer.jsp"/>
</footer>
</body>
</html>
