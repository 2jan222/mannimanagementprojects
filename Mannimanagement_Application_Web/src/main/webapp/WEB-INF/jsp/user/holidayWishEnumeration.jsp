<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="driver" scope="request"
             type="at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.WebDriverEntity"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--<link href="/resources/static/css/main.css" type="text/css" rel="stylesheet"/>-->
<jsp:include page="../fragments/style.jsp"/>
<div>
    <script>
        function deleteWindowHoliday(bean) {
            var r = confirm("You really want to delete your wish?")
            if (r === true){
                delHolidayWish(bean)
            }else {

            }
        }
    </script>

    <table class="table">
        <thead>
        <col width="">
        <col width="">
        <col width="150">
        <col width="300">
        <tr>
            <th>
                Start
            </th>
            <th>
                End
            </th>
            <th>
                Status
            </th>
            <th>
                Delete
            </th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${driver.holidayWishes}" var="holidayWish">
            <tr>
                <td>
                        ${holidayWish.startDate}
                </td>
                <td>
                        ${holidayWish.endDate}
                </td>
                <td>
                    <c:choose>
                        <c:when test="${holidayWish.stateOrdinal == 0}">
                            Open
                        </c:when>
                        <c:when test="${holidayWish.stateOrdinal == 1}">
                            Accepted
                        </c:when>
                        <c:when test="${holidayWish.stateOrdinal == 2}">
                            Denied
                        </c:when>
                    </c:choose>

                </td>
                <td>
                    <c:choose>
                        <c:when test="${holidayWish.stateOrdinal == 0}">
                            <button class="button" onclick="deleteWindowHoliday(${holidayWish.holidayWishId})">Delete</button>
                        </c:when>
                        <c:otherwise>

                        </c:otherwise>
                    </c:choose>

                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
