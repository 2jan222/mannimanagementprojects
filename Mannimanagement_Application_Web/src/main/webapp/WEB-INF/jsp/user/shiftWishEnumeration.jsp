<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="driver" scope="request" type="at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.WebDriverEntity"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div>
    <script>
        function deleteWindowShift(bean) {
            var r = confirm("You really want to delete your wish?")
            if (r === true){
                delShiftWish(bean)
            }else {

            }
        }
    </script>
    <table class="table">
        <thead>
        <col width="">
        <col width="">
        <col width="">
        <col width="150">
        <col width="150">
        <col width="300">
        <tr>
            <th>
                Date
            </th>
            <th>
                Type of Preference
            </th>
            <th>
                Given Cause
            </th>
            <th>
                Shift Type
            </th>
            <th>
                Status
            </th>
            <th>
                Delete
            </th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${driver.shiftWishes}" var="shiftWish" varStatus="idIterator">
            <tr id="shiftWishNr${idIterator.index}">
                <td>
                        ${shiftWish.date}
                </td>
                <td>
                    <c:choose>
                        <c:when test="${shiftWish.positive == true}">
                            positive
                        </c:when>
                        <c:otherwise>
                            negative
                        </c:otherwise>
                    </c:choose>
                </td>
                <td>
                        ${shiftWish.negativeCause}
                </td>
                <td>
                    <c:choose>
                        <c:when test="${shiftWish.shiftType == 1}">
                            Early
                        </c:when>
                        <c:when test="${shiftWish.shiftType == 2}">
                            Late
                        </c:when>
                        <c:when test="${shiftWish.shiftType == 0}">
                            Normal
                        </c:when>

                    </c:choose>


                </td>
                <td>
                    <c:choose>
                        <c:when test="${shiftWish.stateOrdinal == 0}">
                            Open
                        </c:when>
                        <c:when test="${shiftWish.stateOrdinal == 1}">
                            Accepted
                        </c:when>
                        <c:when test="${shiftWish.stateOrdinal == 2}">
                            Denied
                        </c:when>
                    </c:choose>

                </td>
                <td>
                    <c:choose>
                        <c:when test="${shiftWish.stateOrdinal == 0}">
                            <button class="button" onclick="deleteWindowShift(${shiftWish.shiftWishId})">Delete</button>
                        </c:when>
                        <c:otherwise>

                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
