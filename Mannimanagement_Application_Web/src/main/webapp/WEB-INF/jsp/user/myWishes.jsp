<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>My Preferences</title>
    <jsp:include page="../fragments/style.jsp"/>
    <script src="<c:url value="/resources/static/js/myWishes.js"/>"></script>
</head>
<body>
<header>
    <h1 class="headerall">Preferences for ${driver.username}</h1>
</header>
<aside>
    <nav id="nav">
        <jsp:include page="../fragments/menu.jsp"/>
    </nav>
</aside>
<main>
    <div id="content">
        <h1>Vacation Preferences</h1>
        <div id="holidayWishes">
            <jsp:include page="holidayWishEnumeration.jsp"/>
        </div>
        <hr/>
        <h1>Shift Preferences</h1>
        <div id="shiftWishes">
            <jsp:include page="shiftWishEnumeration.jsp"/>
        </div>
        <hr/>
    </div>
</main>
<footer>
    <jsp:include page="../fragments/footer.jsp"/>
</footer>
</body>
</html>
