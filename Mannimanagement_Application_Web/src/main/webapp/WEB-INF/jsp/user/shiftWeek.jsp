<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Tab links -->


<div class="tab">
    <button class="tablinks" onclick="openTab('forWeek')">Assign for week</button>
    <button class="tablinks" onclick="openTab('perDay')">Assign per day</button>
</div>

<!-- Tab content -->
<div id="forWeek" class="tabcontent">
    <jsp:include page="shiftWeekFormWeekAsOne.jsp"/>
</div>

<div id="perDay" class="tabcontent" style="display: none">
    <h1>Assignment per day for ${year}, W${weekNr}</h1>
        <br/>
    <ul>
        <li>
            <span class="day">${startDate.plusDays(0).format(formatter)}</span> <span class="long">Monday</span>

            <form id="formOfDayMo" onsubmit="return false;">
                <h3 class="shiftUeber">Select type of shift:</h3>
                <label class="labelRadio1" for="shiftTypePickerEarlyMo">Early</label><input id="shiftTypePickerEarlyMo" name="shiftTypeMo" value="Early" type="radio"/>
                <label class="labelRadio" for="shiftTypePickerNormalMo">Normal</label><input id="shiftTypePickerNormalMo" name="shiftTypeMo" value="Normal" type="radio"/>
                <label class="labelRadio" for="shiftTypePickerLateMo">Late</label><input id="shiftTypePickerLateMo" name="shiftTypeMo" value="Late" type="radio"/>
                <h3>Type of Preference:</h3>
                <label class="labelRadio" for="typePrefPosMo">positive</label><input id="typePrefPosMo" name="typePrefMo" value="positive" type="radio"/>
                <label class="labelRadio" for="typePrefNegMo">negative</label><input id="typePrefNegMo" name="typePrefMo" value="negative" type="radio"/>
                <h3>Cause:</h3>
                <label for="causeMo"></label><textarea id="causeMo" name="causeWeek" placeholder="A cause will turn this request into a negative wish" rows="5" cols = "50"></textarea><br/>
                <button class="sendRequest" onclick="alertShowShift1('${startDate.plusDays(0).format(formatterYYYYMMDD)}', 'Mo')">Send Request</button>
            </form>
            <div class="alert" id="Mo" style="display:none">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                REQUEST SENT
            </div>
            <hr/>
        </li>

        <li>
            <span class="day">${startDate.plusDays(1).format(formatter)}</span> <span
                class="long">Tuesday</span>
            <form id="formOfDayTu" onsubmit="return false;">
                <h3 class="shiftUeber">Select type of shift:</h3>
                <label class="labelRadio1" for="shiftTypePickerEarlyTu">Early</label><input id="shiftTypePickerEarlyTu" name="shiftTypeTu" value="Early" type="radio"/>
                <label class="labelRadio" for="shiftTypePickerNormalTu">Normal</label><input id="shiftTypePickerNormalTu" name="shiftTypeTu" value="Normal" type="radio"/>
                <label class="labelRadio" for="shiftTypePickerLateTu">Late</label><input id="shiftTypePickerLateTu" name="shiftTypeTu" value="Late" type="radio"/>
                <h3>Type of Preference:</h3>
                <label class="labelRadio" for="typePrefPosTu">positive</label><input id="typePrefPosTu" name="typePrefTu" value="positive" type="radio"/>
                <label class="labelRadio" for="typePrefNegTu">negative</label><input id="typePrefNegTu" name="typePrefTu" value="negative" type="radio"/>
                <h3>Cause:</h3>
                <label for="causeTu"></label><textarea id="causeTu" name="causeWeek" placeholder="A cause will turn this request into a negative wish" rows="5" cols = "50"></textarea><br/>
                <button class="sendRequest" onclick="alertShowShift1('${startDate.plusDays(1).format(formatterYYYYMMDD)}', 'Tu')">Send Request</button>
            </form>
            <div class="alert" id="Tu" style="display:none">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                REQUEST SENT
            </div>
            <hr/>
        </li>

        <li>
            <span class="day">${startDate.plusDays(2).format(formatter)}</span>
            <span class="long">Wednesday</span>
            <form id="formOfDayWe" onsubmit="return false;">
                <h3 class="shiftUeber">Select type of shift:</h3>
                <label class="labelRadio1" for="shiftTypePickerEarlyWe">Early</label><input id="shiftTypePickerEarlyWe" name="shiftTypeWe" value="Early" type="radio"/>
                <label class="labelRadio" for="shiftTypePickerNormalWe">Normal</label><input id="shiftTypePickerNormalWe" name="shiftTypeWe" value="Normal" type="radio"/>
                <label class="labelRadio" for="shiftTypePickerLateWe">Late</label><input id="shiftTypePickerLateWe" name="shiftTypeWe" value="Late" type="radio"/>
                <h3>Type of Preference:</h3>
                <label class="labelRadio" for="typePrefPosWe">positive</label><input id="typePrefPosWe" name="typePrefWe" value="positive" type="radio"/>
                <label class="labelRadio" for="typePrefNegWe">negative</label><input id="typePrefNegWe" name="typePrefWe" value="negative" type="radio"/>
                <h3>Cause:</h3>
                <label for="causeWe"></label><textarea id="causeWe" name="causeWeek" placeholder="A cause will turn this request into a negative wish" rows="5" cols = "50"></textarea><br/>
                <button class="sendRequest" onclick="alertShowShift1('${startDate.plusDays(2).format(formatterYYYYMMDD)}', 'We')">Send Request</button>
            </form>
            <div class="alert" id="We" style="display:none">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                REQUEST SENT
            </div>
            <hr/>
        </li>

        <li>
            <span class="day">${startDate.plusDays(3).format(formatter)}</span> <span class="long">Thursday</span>
            <form id="formOfDayTh" onsubmit="return false;">
                <h3 class="shiftUeber">Select type of shift:</h3>
                <label class="labelRadio1" for="shiftTypePickerEarlyTh">Early</label><input id="shiftTypePickerEarlyTh" name="shiftTypeTh" value="Early" type="radio"/>
                <label class="labelRadio" for="shiftTypePickerNormalTh">Normal</label><input id="shiftTypePickerNormalTh" name="shiftTypeTh" value="Normal" type="radio"/>
                <label class="labelRadio" for="shiftTypePickerLateTh">Late</label><input id="shiftTypePickerLateTh" name="shiftTypeTh" value="Late" type="radio"/>
                <h3>Type of Preference:</h3>
                <label class="labelRadio" for="typePrefPosTh">positive</label><input id="typePrefPosTh" name="typePrefTh" value="positive" type="radio"/>
                <label class="labelRadio" for="typePrefNegTh">negative</label><input id="typePrefNegTh" name="typePrefTh" value="negative" type="radio"/>
                <h3>Cause:</h3>
                <label for="causeTh"></label><textarea id="causeTh" name="causeWeek" placeholder="A cause will turn this request into a negative wish" rows="5" cols = "50"></textarea><br/>
                <button class="sendRequest" onclick="alertShowShift1('${startDate.plusDays(3).format(formatterYYYYMMDD)}', 'Th')">Send Request</button>
            </form>
            <div class="alert" id="Th" style="display:none">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                REQUEST SENT
            </div>
            <hr/>
        </li>

        <li>
            <span class="day">${startDate.plusDays(4).format(formatter)}</span> <span
                class="long">Friday</span>
            <form id="formOfDayFr" onsubmit="return false;">
                <h3 class="shiftUeber">Select type of shift:</h3>
                <label class="labelRadio1" for="shiftTypePickerEarlyFr">Early</label><input id="shiftTypePickerEarlyFr" name="shiftTypeFr" value="Early" type="radio"/>
                <label class="labelRadio" for="shiftTypePickerNormalFr">Normal</label><input id="shiftTypePickerNormalFr" name="shiftTypeFr" value="Normal" type="radio"/>
                <label class="labelRadio" for="shiftTypePickerLateFr">Late</label><input id="shiftTypePickerLateFr" name="shiftTypeFr" value="Late" type="radio"/>
                <h3>Type of Preference:</h3>
                <label class="labelRadio" for="typePrefPosFr">positive</label><input id="typePrefPosFr" name="typePrefFr" value="positive" type="radio"/>
                <label class="labelRadio" for="typePrefNegFr">negative</label><input id="typePrefNegFr" name="typePrefFr" value="negative" type="radio"/>
                <h3>Cause:</h3>
                <label for="causeFr"></label><textarea id="causeFr" name="causeWeek" placeholder="A cause will turn this request into a negative wish" rows="5" cols = "50"></textarea><br/>
                <button class="sendRequest" onclick="alertShowShift1('${startDate.plusDays(4).format(formatterYYYYMMDD)}', 'Fr')">Send Request</button>
            </form>
            <div class="alert" id="Fr" style="display:none">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                REQUEST SENT
            </div>
            <hr/>
        </li>

        <li>
            <span class="day">${startDate.plusDays(5).format(formatter)}</span> <span class="long">Saturday</span>

            <form id="formOfDaySa" onsubmit="return false;">
                <h3 class="shiftUeber">Select type of shift:</h3>
                <label class="labelRadio1" for="shiftTypePickerEarlySa">Early</label><input id="shiftTypePickerEarlySa" name="shiftTypeSa" value="Early" type="radio"/>
                <label class="labelRadio" for="shiftTypePickerNormalSa">Normal</label><input id="shiftTypePickerNormalSa" name="shiftTypeSa" value="Normal" type="radio"/>
                <label class="labelRadio" for="shiftTypePickerLateSa">Late</label><input id="shiftTypePickerLateSa" name="shiftTypeSa" value="Late" type="radio"/>
                <h3>Type of Preference:</h3>
                <label class="labelRadio" for="typePrefPosSa">positive</label><input id="typePrefPosSa" name="typePrefSa" value="positive" type="radio"/>
                <label class="labelRadio" for="typePrefNegSa">negative</label><input id="typePrefNegSa" name="typePrefSa" value="negative" type="radio"/>
                <h3>Cause:</h3>
                <label for="causeSa"></label><textarea id="causeSa" name="causeWeek" placeholder="A cause will turn this request into a negative wish" rows="5" cols = "50"></textarea><br/>
                <button class="sendRequest" onclick="alertShowShift1('${startDate.plusDays(5).format(formatterYYYYMMDD)}', 'Sa')">Send Request</button>
            </form>
            <div class="alert" id="Sa" style="display:none">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                REQUEST SENT
            </div>
            <hr/>
        </li>

        <li>
            <span class="day">${startDate.plusDays(6).format(formatter)}</span> <span
                class="long">Sunday</span>
            <form id="formOfDaySo" onsubmit="return false;">
                <h3 class="shiftUeber">Select type of shift:</h3>
                <label class="labelRadio1" for="shiftTypePickerEarlySo">Early</label><input id="shiftTypePickerEarlySo" name="shiftTypeSo" value="Early" type="radio"/>
                <label class="labelRadio" for="shiftTypePickerNormalSo">Normal</label><input id="shiftTypePickerNormalSo" name="shiftTypeSo" value="Normal" type="radio"/>
                <label class="labelRadio" for="shiftTypePickerLateSo">Late</label><input id="shiftTypePickerLateSo" name="shiftTypeSo" value="Late" type="radio"/>
                <h3>Type of Preference:</h3>
                <label class="labelRadio" for="typePrefPosSo">positive</label><input id="typePrefPosSo" name="typePrefSo" value="positive" type="radio"/>
                <label class="labelRadio" for="typePrefNegSo">negative</label><input id="typePrefNegSo" name="typePrefSo" value="negative" type="radio"/>
                <h3>Cause:</h3>
                <label for="causeSo"></label><textarea id="causeSo" name="causeWeek" placeholder="A cause will turn this request into a negative wish" rows="5" cols = "50"></textarea><br/>
                <button class="sendRequest" onclick="alertShowShift1('${startDate.plusDays(6).format(formatterYYYYMMDD)}', 'So')">Send Request</button>
            </form>
            <div class="alert" id="So" style="display:none">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                REQUEST SENT
            </div>
            <hr/>
        </li>
    </ul>
</div>


