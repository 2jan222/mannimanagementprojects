<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="../fragments/style.jsp"/>
    <title>Shift Preferences</title>
    <script>
        Date.prototype.addDays = function(days) {
            var date = new Date(this.valueOf());
            date.setDate(date.getDate() + days);
            return date;
        };
        Date.prototype.formDate = function () {
            const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
            let current_datetime = new Date();
            return current_datetime.getDate() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getFullYear();
        };
        // Returns a Date object for Monday at the
        // start of the specified week
        function dateFromWeekNumber(year, week) {
            var d = new Date(year, 0, 1);
            var dayNum = d.getDay();
            var diff = --week * 7;

            // If 1 Jan is Friday to Sunday, go to next week
            if (!dayNum || dayNum > 4) {
                diff += 7;
            }

            // Add required number of days
            d.setDate(d.getDate() - d.getDay() + ++diff);
            return d;
        }
        function showDaysOfWeek() {
            let date;
            let week;
            let year;
            var weekPicker = document.getElementById("week");
            if (weekPicker !== undefined && weekPicker.value !== undefined) {
                console.log(weekPicker.value);
                var strings = weekPicker.value.split("-");
                year = strings[0];
                week = strings[1].substr(1);
                var startDateOfWeek = getStartDateOfWeek(week, year);
                console.log(startDateOfWeek);
                date = dateFromWeekNumber(year, week);
                console.log(date);
                let dateString = date.getFullYear() + "-" + ((date.getMonth() > 9) ? date.getMonth():"0" + date.getMonth())  + "-" + date.getDate() ;
                fetch("/user/shiftWishForm/getShiftWeek?startDate=" + dateString + "&week=" + week).then(function (response) {
                    return response.text();
                }).then(function (a) {
                    document.getElementById("content").innerHTML = a;
                })
            }
        }

        function showForWeek() {
            console.log("Execute");
            var startDate = document.getElementById("week");
            if (startDate !== undefined && startDate.value !== undefined && startDate.value !== "") {
                console.log("Start " + startDate.value);

                if (true || startDate.value > endDate.value) {
                    console.log("HERE");
                    var url = "/user/shiftWishForm/getWeek?start=" + startDate.value + "&end=" + endDate.value;
                    console.log("URL " + url);
                    fetch(url).then(function (response) {
                        return response.json();
                    }).then(function (j) {
                        console.log("Output");
                        console.log(j)
                    });
                }

            }

        }

        function getStartDateOfWeek(week, year) {
            var d = ((week - 1) * 7); // 1st of January + 7 days for each week

            return new Date(year, 0, d);
        }
    </script>
    <script>
        function subWeek() {
            let date;
            let week;
            let year;
            let dateString;
            var weekPicker = document.getElementById("week");
            if (weekPicker !== undefined && weekPicker.value !== undefined) {
                console.log(weekPicker.value);
                var strings = weekPicker.value.split("-");
                year = strings[0];
                week = strings[1].substr(1);
                var startDateOfWeek = getStartDateOfWeek(week, year);
                console.log(startDateOfWeek);
                date = dateFromWeekNumber(year, week);
                console.log(date);
                dateString = date.getFullYear() + "-" + ((date.getMonth() > 9) ? date.getMonth() : "0" + date.getMonth()) + "-" + date.getDate();
            }
            var val;
            let shiftTypeElements = document.getElementsByName("shiftType");
            for (var e in shiftTypeElements) {
                let el = shiftTypeElements[e];
                if (el.checked) {
                    val = el;
                    break;
                }
            }
            var pos;
            let prefTypeElements = document.getElementsByName("typePref");
            for (var f in prefTypeElements) {
                let el = prefTypeElements[f];
                if (el.checked) {
                    pos = el;
                    break;
                }
            }
            let cause = document.getElementsByName("causeWeek")[0].value;
            console.log(val.value);
            console.log(cause);
            let causeString = (cause !== undefined) ? "&cause=" + cause:"";//TODO ADD typePref to request and do the same for day request
            fetch("/user/shiftWishForm/updateForWeek?startDate=" + dateString + "&week=" + week + "&shiftType=" + val.value + "&isPos=" + pos.value + causeString).then(function (response) {
                return response.text();
            }).then(function (a) {
                console.log(a);
            })
        };

        function subDay(dateS, qual) {
            var val;
            let shiftTypeElements = document.getElementsByName("shiftType" + qual);
            for (var e in shiftTypeElements) {
                let el = shiftTypeElements[e];
                if (el.checked) {
                    val = el;
                    break;
                }
            }
            var pos;
            let prefTypeElements = document.getElementsByName("typePref" + qual);
            for (var f in prefTypeElements) {
                let el = prefTypeElements[f];
                if (el.checked) {
                    pos = el;
                    break;
                }
            }
            let cause = document.getElementById("cause" + qual).value;
            console.log(val.value);
            console.log(cause);
            let causeString = (cause !== undefined) ? "&cause=" + cause:"";
            //let date = new Date().parse(dateS);
            //let dateString = date.getFullYear() + "-" + ((date.getMonth() > 9) ? date.getMonth() : "0" + date.getMonth()) + "-" + date.getDate();
            fetch("/user/shiftWishForm/updateForDay?startDate=" + dateS + "&shiftType=" + val.value + "&isPos=" + pos.value + causeString).then(function (response) {
                return response.text();
            }).then(function (a) {
                console.log(a);
            })
        }

        function openTab(id) {
            let newById = document.getElementById(id);
            newById.style.display = "block";
            if (id === "forWeek") {
                let selfById = document.getElementById("perDay");
                selfById.style.display = "none";
            } else {
                let selfById = document.getElementById("forWeek");
                selfById.style.display = "none";
            }
        }

        function alertShowShift1(shift, day) {
            subDay(shift, day);
            var div = document.getElementById(day);
            if (div.style.display === 'none') {
                div.style.display = '';
            } else {
                div.style.display = 'none';
            }
        }

        function alertShowWeek() {
            subWeek();
            var div = document.getElementById("alert");
            if (div.style.display === 'none') {
                div.style.display = '';
            } else {
                div.style.display = 'none';
            }
        }
    </script>
</head>
<body>
<header>
    <h1 class="headerall">Shift Preferences</h1>
</header>
<aside>
    <nav id="nav">
        <jsp:include page="../fragments/menu.jsp"/>
    </nav>
</aside>
<main>
    <div class="divForm" id="form">
        <form>
            <label for=week> Start: </label>
            <input type="week" id="week" name="week" placeholder="Your desired week...">
            <input type="button" value="Select" onclick="showDaysOfWeek()">
            <hr/>
        </form>
    </div>
    <div id="content">

    </div>
</main>
<footer>
    <jsp:include page="../fragments/footer.jsp"/>
</footer>
</body>
</html>
