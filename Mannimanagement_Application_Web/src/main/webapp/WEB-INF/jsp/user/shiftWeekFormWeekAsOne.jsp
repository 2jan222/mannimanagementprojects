<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="divForm">
    <form id="weekShiftWishForm" onsubmit="return false;">
        <h3 class="shiftUeber">Select type of shift:</h3>
        <label class="labelRadio1" for="shiftTypePickerEarly">Early</label><input id="shiftTypePickerEarly" name="shiftType" value="Early" type="radio"/>
        <label class="labelRadio" for="shiftTypePickerNormal">Normal</label><input id="shiftTypePickerNormal" name="shiftType" value="Normal" type="radio"/>
        <label class="labelRadio" for="shiftTypePickerLate">Late</label><input id="shiftTypePickerLate" name="shiftType" value="Late" type="radio"/>
        <h3>Type of Preference:</h3>
        <label class="labelRadio" for="typePrefPos">positive</label><input id="typePrefPos" name="typePref" value="positive" type="radio"/>
        <label class="labelRadio" for="typePrefNeg">negative</label><input id="typePrefNeg" name="typePref" value="negative" type="radio"/>
        <h3>Cause:</h3>
        <label for="cause"></label><textarea id="cause" name="causeWeek" placeholder="A cause will turn this request into a negative wish" rows="5" cols = "50"></textarea><br/>
        <button class="sendRequest" onclick="alertShowWeek()">Send Request</button>
    </form>
</div>
<div class="alert" id="alert" style="display:none">
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    REQUEST SENT
</div>
