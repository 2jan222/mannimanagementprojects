<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../fragments/style.jsp"/>
<div id="menu">
    <form action="${pageContext.request.contextPath}/user/holidayWishForm">
        <button class="menu-button" type="submit"><span>Vacation Preferences</span></button>
    </form>

    <form action="${pageContext.request.contextPath}/user/shiftWishForm">
        <button class="menu-button" type="submit"><span>Shift Preferences</span></button>
    </form>

    <form action="${pageContext.request.contextPath}/user/myWishes">
        <button class="menu-button" type="submit"><span>My Preferences</span></button>
    </form>

</div>
