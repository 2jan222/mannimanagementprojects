<%--
  Created by IntelliJ IDEA.
  User: Joshi
  Date: 03.06.2019
  Time: 13:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<style>

header{
    width: 100%;
    background: #626E7E;
    height: 60px;
    display: block;
}
.headerall{
    padding: 15px;
    color: white;
    text-align: center;
    text-decoration: underline;
}
h1{
    color: black;
    font-family: Arial, Algerian, "Footlight MT Light";
}

aside{
    width: 20%;
    height: 100%;
    background-color: #626E7E;
    float: left;
    border-right: red;
}

main{
    float: left;
    width: 75%;
    margin-left: 20px;
}

footer{
    position: absolute;
    bottom: 0;
    width: 100%;
    height: 50px;
}
/*
#menu ul{
    list-style: none;
}

#menu li{
    margin: .5em;
}

#menu a{
    text-decoration: none;
}

#nav a:focus,
#nav a:hover,
#nav a:active{
    text-decoration: underline;
    font-weight: bold;
}
*/
table{
        color: #333;
        font-family: Arial,Helvetica, sans-serif;
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;

    }
    td, th {
        border: 1px solid transparent;
        height: 50px;
        transition: all 0.3s;
    }

    th {
        background: gray;
        color: black;
        font-weight: bold;
        text-shadow: white;
        font-size: 18px;
    }

    td {
        background: #FAFAFA;
        text-align: center;
        font-weight: bold;
    }

    tr:nth-child(even) td {background: #F1F1F1;}
    tr:nth-child(odd) td {background: #FEFEFE;}
    tr:hover { background: black; color: #4CAF50; font-weight: bold }

    .button {
        background: darkred;
        border: 1px solid darkred;
        color: white;
        font-weight: bold;
        text-align: center;
        font-size: 16px;
        width: 100%;
        height: 100%;
        display: block;

    }

    .button:hover {background-color: red}

    .button:active {
        background-color: #3e8e41;
        box-shadow: 0 5px #666;
        transform: translateY(4px);
    }

.menu-button {
    display: inline-block;
    border-radius: 4px;
    border: 3px #4CAF50 solid;
    background-color: #4CAF50;
    color: white;
    text-align: center;
    font-size: 20px;
    padding: 20px;
    width: 100%;
    transition: all 0.5s;
    cursor: pointer;
}

.menu-button span {
    cursor: pointer;
    display: inline-block;
    position: relative;
    transition: 0.5s;
}

.menu-button span:after {
    content: '\00bb';
    position: absolute;
    opacity: 0;
    top: 0;
    right: -20px;
    transition: 0.5s;
}

.menu-button:hover span {
    padding-right: 25px;
}

.menu-button:hover span:after {
    opacity: 1;
    right: 0;
}

input[type=week]{
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

input[type=date]{
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

label{
    font-family: Arial, Helvetica, sans-serif;
    font-size: 20px;
    color: black;
    font-weight: bold;
}

input[type=button] {
    width: 100%;
    height: 45px;
    background-color: #4CAF50;
    color: white;
    font-size: 18px;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

input[type=button]:hover {
    background-color: #45a049;
}

    .divForm{
        padding: 20px;
    }
    body, html{
        margin: 0 auto;
        padding: 0;
        height: 100%;
        border: 0;
        outline: 0;
    }

    .loginButton{
        background-color: #4CAF50;
        color: white;
        font-size: 18px;
        width: 60px;
        height: 30px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }
    .loginButton:hover{
        background-color: #45a049;
    }
    .tablinks{
        width: 48%;
        background-color: #37b080;
        color: white;
        padding: 12px 20px;
        margin: 8px 0;
        font-size: 18px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
        height: 45px;
    }
    .tablinks:hover{
        background-color: #2d7b55;
    }

    .labelRadio{
        margin-left: 20%;
        font-size: 15px;
        font-family: Arial, Helvetica, sans-serif;
    }
.labelRadio1{

    font-size: 15px;
    font-family: "Century Gothic",Arial,sans-serif;
}
.shiftUeber{
    font-family: "Century Gothic",Arial,sans-serif;
    text-decoration: underline;

}
    h3{
        font-family: "Century Gothic",Arial,sans-serif;
        text-decoration: underline;
    }
    .sendRequest{
        background-color: #4CAF50;
        display: ;
        color: white;
        height: 45px;
        width: 150px;
        font-size: 18px;
        margin-top: 10px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }

.sendRequest:hover{
    background-color: #45a049;
}

    .day{
        font-family: "Century Gothic",Arial,sans-serif;
        font-size: 18px;
    }
    .long{
        font-family: "Century Gothic",Arial,sans-serif;
        font-size: 18px;
    }
.tabcontent{
    margin-left: 20px;
}
.loginl{
    margin: 0;
}
    .tab{
        margin-left: 20px;
    }

.alert {
    padding: 20px;
    background-color: green;
    color: white;
}

.closebtn {
    margin-left: 15px;
    color: white;
    font-weight: bold;
    float: right;
    font-size: 22px;
    line-height: 20px;
    cursor: pointer;
    transition: 0.3s;

}

.closebtn:hover {
    color: black;
}
</style>
