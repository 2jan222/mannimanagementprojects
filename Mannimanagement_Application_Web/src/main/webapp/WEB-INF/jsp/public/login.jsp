<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
    <style>
        .count-particles{
            background: #000022;
            position: absolute;
            top: 48px;
            left: 0;
            width: 80px;
            color: #7CFC00;
            font-size: .8em;
            text-align: left;
            text-indent: 4px;
            line-height: 14px;
            padding-bottom: 2px;
            font-family: Helvetica, Arial, sans-serif;
            font-weight: bold;
        }

        .js-count-particles{
            font-size: 1.1em;
        }

        #stats,
        .count-particles{
            -webkit-user-select: none;
            margin-top: 5px;
            margin-left: 5px;
        }

        #stats{
            border-radius: 3px 3px 0 0;
            overflow: hidden;
        }

        .count-particles{
            border-radius: 0 0 3px 3px;
        }


        /* ---- particles.js container ---- */

        #particles-js{
            width: 100%;
            height: 100%;
            background-color: black;
            background-size: cover;
            background-position: 50% 50%;
            background-repeat: no-repeat;
        }

        #login{
            background:#fff;
            opacity:0.9;
            padding:2em;
            border:#ccc 2px solid;
            position:absolute;
            top:35%;
            left:35%;
        }

        #login input[type="text"], #login input[type="password"]{
            padding:7px;
            margin-bottom:10px;
            border:1px #ccc solid;
        }
    </style>
    <jsp:include page="../fragments/style.jsp"/>
</head>
<body>
<div id="particles-js">
    <div id="login">
        <form action="/login" method="post">
            <h3 class="login1">Login:</h3>
            <label for="username"></label><input type="text" name="username" id="username" placeholder="username" required="true" autofocus="true"><br/>
            <label for="password"></label><input type="password" name="password" id="password" placeholder="password" required="true"><br/>
            <input class="loginButton" type="submit" value="Send" style="float: right;">
        </form>
    </div>
</div>
<script>
    particlesJS.load('particles-js', 'particles.json', function(){
        console.log('particles.json loaded...');
    });
</script>
</body>
</html>
