<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>HOME</title>
    <jsp:include page="../fragments/style.jsp"/>
</head>
<body>
<h1>HOME</h1>
<jsp:include page="../fragments/menu.jsp"/>
</body>
</html>
