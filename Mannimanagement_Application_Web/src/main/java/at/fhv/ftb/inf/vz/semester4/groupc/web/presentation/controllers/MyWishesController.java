package at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.controllers;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MyWishesController {
    @GetMapping("/user/myWishes")
    public String myWishes(@NotNull Model model) {
        model.addAttribute("driver", MainRoutingController.userAssociatedDriver());
        return "/user/myWishes";
    }

}
