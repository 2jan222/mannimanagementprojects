package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade.DatabaseEntityMarker;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "station", schema = "public", catalog = "kmobil")
public class FlatStationEntity implements DatabaseEntityMarker {
    private int stationId;
    private String stationName;
    private String shortName;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "station_id")
    public int getStationId() {
        return stationId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    @Basic
    @Column(name = "station_name")
    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    @Basic
    @Column(name = "short_name")
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FlatStationEntity)) return false;
        FlatStationEntity that = (FlatStationEntity) o;
        return stationId == that.stationId &&
                stationName.equals(that.stationName) &&
                shortName.equals(that.shortName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stationId, stationName, shortName);
    }

    @Override
    public String toString() {
        return "FlatStationEntity{" +
                "stationId=" + stationId +
                ", stationName='" + stationName + '\'' +
                ", shortName='" + shortName + '\'' +
                '}';
    }
}
