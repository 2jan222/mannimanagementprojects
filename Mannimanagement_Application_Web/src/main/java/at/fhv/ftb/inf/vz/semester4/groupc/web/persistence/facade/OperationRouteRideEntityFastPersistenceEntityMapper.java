package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.OperationRouteRideEntityFast;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class OperationRouteRideEntityFastPersistenceEntityMapper extends PersistenceEntityMapper<OperationRouteRideEntityFast> {
    @Override
    public OperationRouteRideEntityFast read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(OperationRouteRideEntityFast.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("OperationRouteRideEntityFast", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<OperationRouteRideEntityFast> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<OperationRouteRideEntityFast> query = session.createQuery("from OperationRouteRideEntityFast ", OperationRouteRideEntityFast.class);
            return query.getResultList();
        }
    }

    @Override
    public List<OperationRouteRideEntityFast> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<OperationRouteRideEntityFast> query = session.createQuery("from OperationRouteRideEntityFast " + whereQueryFragment, OperationRouteRideEntityFast.class);
            return query.getResultList();
        }
    }
}
