package at.fhv.ftb.inf.vz.semester4.groupc.web.application;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade.PersistenceFacade;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.AbsenceEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.DriverEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.HolidayWishEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.WebDriverEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.controllers.CustomResponseBody;
import de.jollyday.HolidayCalendar;
import de.jollyday.HolidayManager;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.controllers.CustomResponseBody.ApplicationError.DATES_ORDER_REVERSED;
import static at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.controllers.CustomResponseBody.ApplicationError.DATE_OF_PAST_DAY;
import static at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.controllers.CustomResponseBody.ApplicationWarning.HOLIDAY_REQUEST_OVERLAPS_WITH_EXISTING_REQUEST;
import static at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.controllers.CustomResponseBody.ApplicationWarning.NOT_ENOUGH_PERSONAL;


public class HolidayRequest {
    public static final Integer WEEKDAY_NEEDED_DRIVERS = 9;
    public static final Integer SATURDAY_NEEDED_DRIVERS = 5;
    public static final Integer SUN_AND_HOLIDAY_NEEDED_DRIVERS = 4;

    public HashMap<LocalDate, Boolean> requestHoliday(@NotNull WebDriverEntity user, @NotNull LocalDate start, LocalDate end, CustomResponseBody customResponseBody) {
        HashMap<LocalDate, Boolean> map = new HashMap<>();
        Set<HolidayWishEntity> existingWishes = user.getHolidayWishes();
        HolidayWishEntity wishNew = new HolidayWishEntity(user.getDriverId(), start, end);
        if (start.isBefore(end) || start.equals(end)) {         //Start vor Ende
            if (start.isBefore(LocalDate.now())) {              //Start vor Heute
                customResponseBody.addError(DATE_OF_PAST_DAY);
            } else {
                map = createMapForDays(start, end);
                boolean driverHolidayOverlapping = checkDriversHolidayRequestsOverlapping(existingWishes, wishNew);
                if (!driverHolidayOverlapping) {                //Request überlappt mit altem Request
                    customResponseBody.addWarning(HOLIDAY_REQUEST_OVERLAPS_WITH_EXISTING_REQUEST);
                }
                boolean enoughDriversBoolean = enoughDrivers(map, user.getDriver());    //Genügend Fahrer
                if (!enoughDriversBoolean) {
                    customResponseBody.addWarning(NOT_ENOUGH_PERSONAL);
                }
                PersistenceFacade.getInstance().create(wishNew);
                user.getHolidayWishes().add(wishNew);
            }
        } else {
            customResponseBody.addError(DATES_ORDER_REVERSED);
        }

        /*DUMMY DATA
        int i = 0;
        while (!start.isAfter(end)) {
            map.put(start, (i & 1) != 0);
            i++;
            start = start.plusDays(1);
        }*/

        return map;
    }

    @Contract(pure = true)
    private boolean enoughDrivers(@NotNull HashMap<LocalDate, Boolean> days, DriverEntity driver) {
        Set<LocalDate> localDates = days.keySet();
        boolean b = true;
        boolean rb;
        @SuppressWarnings({"SqlResolve"})
        int count = ((java.math.BigInteger) PersistenceFacade.getInstance().executeTransaction(session -> session.createSQLQuery("SELECT COUNT(*) FROM driver").uniqueResult())).intValue();
        for (LocalDate localDate : localDates) {
            rb = checkEnoughDriversPerDay(localDate, count);
            boolean finalRb = rb;
            days.computeIfPresent(localDate, (date, aBoolean) -> finalRb);
            b = rb & b;
        }
        return b;
    }

    @Contract(pure = true)
    private boolean checkEnoughDriversPerDay(LocalDate localDate, int totalDrivers) {
        int needDrivers = getNeededDriversForDate(localDate);
        List<AbsenceEntity> allWhere = PersistenceFacade.getInstance().getAllWhere(AbsenceEntity.class, "where '" + localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' BETWEEN dateFrom AND dateTo");
        boolean result = (totalDrivers - allWhere.size() <= needDrivers);
        System.out.println("NEEDED DRIVERS: " + needDrivers + " Absent: " + allWhere.size() + " TOTAL: " + totalDrivers + " Result: " + result);
        return result;
        /*
        Random random = new Random();
        return random.nextBoolean();
        */
    }

    private HashMap<LocalDate, Boolean> createMapForDays(@NotNull LocalDate start, @NotNull LocalDate end) {
        HashMap<LocalDate, Boolean> map = new HashMap<>();
        while (!start.isAfter(end)) {
            map.put(start, true);
            start = start.plusDays(1);
        }
        return map;
    }

    private boolean checkDriversHolidayRequestsOverlapping(Set<HolidayWishEntity> existingWishes, HolidayWishEntity wishNew) {
        LinkedList<HolidayWishEntity> list = new LinkedList<>(existingWishes);
        for (HolidayWishEntity e : list) {
            if (!((e.getStartDate().isAfter(wishNew.getEndDate()) || e.getStartDate().equals(wishNew.getEndDate())) || (e.getEndDate().isBefore(wishNew.getStartDate()) || e.getEndDate().equals(wishNew.getStartDate())))) {
                return false;
            }
        }
        return true;
    }

    public static int getNeededDriversForDate(@NotNull LocalDate day) {
        Set<LocalDate> _holidays = new HashSet<>();
        HolidayManager hm = HolidayManager.getInstance(HolidayCalendar.AUSTRIA);
        hm.getHolidays(LocalDate.now().getYear()).forEach(holiday -> _holidays.add(holiday.getDate()));
        int type = WEEKDAY_NEEDED_DRIVERS;
        if (day.getDayOfWeek() == DayOfWeek.SUNDAY || _holidays.contains(day)) {
            type = SUN_AND_HOLIDAY_NEEDED_DRIVERS;
        } else if (day.getDayOfWeek() == DayOfWeek.SATURDAY) {
            type = SATURDAY_NEEDED_DRIVERS;
        }
        return type;
    }

    public boolean deleteRequest(int id, @NotNull WebDriverEntity webDriverEntity) {
        HolidayWishEntity toDelete = webDriverEntity.getHolidayWishes().stream().filter(e -> e.getHolidayWishId() == id).collect(Collectors.toCollection(LinkedList::new)).getFirst();
        if (toDelete.getStateOrdinal() == 0) {
            PersistenceFacade.getInstance().delete(toDelete);
            webDriverEntity.getHolidayWishes().remove(toDelete);
            return true;
        }
        return false;
    }
}
