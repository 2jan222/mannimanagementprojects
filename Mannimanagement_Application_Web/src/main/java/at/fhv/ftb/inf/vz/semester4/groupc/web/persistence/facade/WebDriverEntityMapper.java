package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.WebDriverEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class WebDriverEntityMapper extends PersistenceEntityMapper<WebDriverEntity> {
    @Override
    public WebDriverEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(WebDriverEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("WebDriverEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<WebDriverEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<WebDriverEntity> query = session.createQuery("from WebDriverEntity ", WebDriverEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<WebDriverEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<WebDriverEntity> query = session.createQuery("from WebDriverEntity " + whereQueryFragment, WebDriverEntity.class);
            return query.getResultList();
        }
    }
}
