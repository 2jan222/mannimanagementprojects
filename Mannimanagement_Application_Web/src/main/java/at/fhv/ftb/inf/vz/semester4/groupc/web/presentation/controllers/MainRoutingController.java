package at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.controllers;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.WebDriverEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.web.application.WebUserRepo;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MainRoutingController {
    @GetMapping("/")
    public String home1() {
        return "/public/home";
    }

    @GetMapping("/home")
    public String home() {
        return "/public/home";
    }

    @GetMapping("/admin")
    public String admin() {
        return "/admin/admin";
    }

    @GetMapping("/user/user")
    public String user() {
        return "/user/user";
    }

    @GetMapping("/about")
    public String about() {
        return "/public/about";
    }

    @GetMapping("/login")
    public String login() {
        return "/public/login";
    }

    @GetMapping("/403")
    public String error403() {
        return "/error/403";
    }

    @GetMapping("/error")
    public String error() {
        return "/error/error";
    }

    @GetMapping("/resCss")
    public String res(@RequestParam(name = "file") String file) {
        return "/resources/static/css/" + file;
    }

    @GetMapping("/particles.json")
    @ResponseBody
    public String particleJson() {
        return "{\n" +
                "  \"particles\":{\n" +
                "    \"number\":{\n" +
                "      \"value\":100\n" +
                "    },\n" +
                "    \"color\":{\n" +
                "      \"value\":\"#7CFC00\"\n" +
                "    },\n" +
                "    \"shape\":{\n" +
                "      \"type\":\"circle\",\n" +
                "      \"stroke\":{\n" +
                "        \"width\":1,\n" +
                "        \"color\":\"#ccc\"\n" +
                "      },\n" +
                "      \"image\":{\n" +
                "        \"src\":\"http://www.iconsdb.com/icons/preview/white/contacts-xxl.png\"\n" +
                "      }\n" +
                "    },\n" +
                "    \"opacity\":{\n" +
                "      \"value\":0.5,\n" +
                "      \"random\":true,\n" +
                "      \"anim\":{\n" +
                "        \"enable\":false,\n" +
                "        \"speed\":1\n" +
                "      }\n" +
                "    },\n" +
                "    \"size\":{\n" +
                "      \"value\": 5,\n" +
                "      \"random\":false,\n" +
                "      \"anim\":{\n" +
                "        \"enable\": false,\n" +
                "        \"speed\":30\n" +
                "      }\n" +
                "    },\n" +
                "    \"line_linked\":{\n" +
                "      \"enable\": true,\n" +
                "      \"distance\": 120,\n" +
                "      \"color\":\"#fff\",\n" +
                "      \"width\":1\n" +
                "    },\n" +
                "    \"move\":{\n" +
                "      \"enable\":true,\n" +
                "      \"speed\":2,\n" +
                "      \"direction\":\"none\",\n" +
                "      \"straight\":false\n" +
                "    }\n" +
                "  },\n" +
                "  \"interactivity\":{\n" +
                "    \"events\":{\n" +
                "      \"onhover\":{\n" +
                "        \"enable\":true,\n" +
                "        \"mode\":\"repulse\"\n" +
                "      },\n" +
                "      \"onclick\":{\n" +
                "        \"enable\": true,\n" +
                "        \"mode\":\"push\"\n" +
                "      }\n" +
                "    },\n" +
                "    \"modes\":{\n" +
                "      \"repulse\":{\n" +
                "        \"distance\":50,\n" +
                "        \"duration\":0.4\n" +
                "      },\n" +
                "      \"bubble\":{\n" +
                "        \"distance\":100,\n" +
                "        \"size\":10\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";
    }

    public static WebDriverEntity userAssociatedDriver() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User principal = (User) auth.getPrincipal();
        return WebUserRepo.search(principal.getUsername());
    }
}
