package at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.util.jsp_renderer;


import org.springframework.http.HttpMethod;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.DispatcherType;

public class MockIncludedHttpServletRequest extends MockHttpServletRequest {

    public MockIncludedHttpServletRequest() {
        super();
        this.setMethod(HttpMethod.GET.name());
    }

    public DispatcherType getDispatcherType() {
        return DispatcherType.INCLUDE;
    }

    public boolean isAsyncSupported() {
        return false;
    }
}
