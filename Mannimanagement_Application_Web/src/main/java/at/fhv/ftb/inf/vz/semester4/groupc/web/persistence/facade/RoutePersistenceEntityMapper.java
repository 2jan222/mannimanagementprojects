package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.RouteEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class RoutePersistenceEntityMapper extends PersistenceEntityMapper<RouteEntity> {
    @Override
    public RouteEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(RouteEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("RouteEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<RouteEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<RouteEntity> query = session.createQuery("from RouteEntity ", RouteEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<RouteEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<RouteEntity> query = session.createQuery("from RouteEntity " + whereQueryFragment, RouteEntity.class);
            return query.getResultList();
        }
    }
}
