package at.fhv.ftb.inf.vz.semester4.groupc.web;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade.PersistenceFacade;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;



public class DatabaseReset {
    public static void main(String[] args) {
        PersistenceFacade.getInstance().executeTransaction(session -> {
            File file = new File("src/main/resources/sql/BusMasterDBSkriptWebV2_Absences.sql");
            System.out.println(file.getAbsolutePath());
            try {
                FileInputStream fis = new FileInputStream(file);
                byte[] data = new byte[(int) file.length()];
                fis.read(data);
                fis.close();
                String str = new String(data, StandardCharsets.UTF_8);
                //System.out.println(str);
                session.createSQLQuery(str);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        });
    }
}
