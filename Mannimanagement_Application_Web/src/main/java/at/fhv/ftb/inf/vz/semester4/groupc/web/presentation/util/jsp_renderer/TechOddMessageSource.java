package at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.util.jsp_renderer;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("messageSource")
public class TechOddMessageSource extends ReloadableResourceBundleMessageSource {

    @PostConstruct
    public void init() {
        setBasename("classpath:/messages/messages");
    }

}
