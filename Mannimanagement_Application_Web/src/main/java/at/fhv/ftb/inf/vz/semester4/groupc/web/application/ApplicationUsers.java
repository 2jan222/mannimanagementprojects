package at.fhv.ftb.inf.vz.semester4.groupc.web.application;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade.PersistenceFacade;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.WebDriverEntity;

import java.util.List;

public class ApplicationUsers {
    public static List<WebDriverEntity> getAllUsers() {
        PersistenceFacade pf = PersistenceFacade.getInstance();
        List<WebDriverEntity> all = pf.getAll(WebDriverEntity.class);
        all.forEach(WebUserRepo::add);
        return all;
    }
}
