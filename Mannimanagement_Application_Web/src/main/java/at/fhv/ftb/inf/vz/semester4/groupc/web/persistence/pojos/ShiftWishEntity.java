package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade.DatabaseEntityMarker;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "shift_wish", schema = "public", catalog = "kmobil")
public class ShiftWishEntity implements DatabaseEntityMarker {
    private Integer shiftWishId;
    private Integer driverId;
    private LocalDate date;
    private boolean isPositive;
    private String negativeCause;
    private Integer shiftType;
    private Integer stateOrdinal;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shift_wish_id")
    public Integer getShiftWishId() {
        return shiftWishId;
    }

    public void setShiftWishId(Integer shiftWishId) {
        this.shiftWishId = shiftWishId;
    }

    @Basic
    @Column(name = "driver_id")
    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    @Basic
    @Column(name = "date")
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate startDate) {
        this.date = startDate;
    }

    @Basic
    @Column(name = "is_positive")
    public boolean getPositive() {
        return isPositive;
    }

    public void setPositive(boolean positive) {
        isPositive = positive;
    }

    @Basic
    @Column(name = "negative_cause")
    public String getNegativeCause() {
        return negativeCause;
    }

    public void setNegativeCause(String negativeCause) {
        this.negativeCause = negativeCause;
    }

    @Basic
    @Column(name = "shift_type")
    public Integer getShiftType() {
        return shiftType;
    }

    public void setShiftType(Integer shiftType) {
        this.shiftType = shiftType;
    }

    @Basic
    @Column(name = "state_ordinal")
    public Integer getStateOrdinal() {
        return stateOrdinal;
    }

    public void setStateOrdinal(Integer stateOrdinal) {
        this.stateOrdinal = stateOrdinal;
    }
}
