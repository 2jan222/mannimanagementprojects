package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade.DatabaseEntityMarker;
import org.jetbrains.annotations.TestOnly;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "driver", schema = "public", catalog = "kmobil")
public class DriverEntity implements DatabaseEntityMarker {
    private Integer driverId;
    private String address;
    private LocalDate birthday;
    private String email;
    private Integer employmentType;
    private String firstname;
    private String lastname;
    private String jobDescription;
    private String telephonenumber;
    //private Set<OperationShiftEntity> operationShiftsByDriverId;
    private Set<AbsenceEntity> absenceEntities;

    public DriverEntity() {
    }

    @TestOnly
    public DriverEntity(Integer driverId, String address, LocalDate birthday, String email, Integer employmentType, String firstname, String lastname, String jobDescription, String telephonenumber) {
        this.driverId = driverId;
        this.address = address;
        this.birthday = birthday;
        this.email = email;
        this.employmentType = employmentType;
        this.firstname = firstname;
        this.lastname = lastname;
        this.jobDescription = jobDescription;
        this.telephonenumber = telephonenumber;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "driver_id")
    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    @Basic
    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "birthday")
    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "employment_type")
    public Integer getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(Integer employmentType) {
        this.employmentType = employmentType;
    }

    @Basic
    @Column(name = "firstname")
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Basic
    @Column(name = "lastname")
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Basic
    @Column(name = "job_description")
    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    @Basic
    @Column(name = "telephonenumber")
    public String getTelephonenumber() {
        return telephonenumber;
    }

    public void setTelephonenumber(String telephonenumber) {
        this.telephonenumber = telephonenumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DriverEntity that = (DriverEntity) o;
        return driverId == that.driverId &&
                Objects.equals(address, that.address) &&
                Objects.equals(birthday, that.birthday) &&
                Objects.equals(email, that.email) &&
                Objects.equals(employmentType, that.employmentType) &&
                Objects.equals(firstname, that.firstname) &&
                Objects.equals(lastname, that.lastname) &&
                Objects.equals(jobDescription, that.jobDescription) &&
                Objects.equals(telephonenumber, that.telephonenumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(driverId, address, birthday, email, employmentType, firstname, lastname, jobDescription, telephonenumber);
    }

    @Override
    public String toString() {
        return "DriverEntity{" +
                "driverId=" + driverId +
                ", address='" + address + '\'' +
                ", birthday=" + birthday +
                ", email='" + email + '\'' +
                ", employmentType=" + employmentType +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", jobDescription='" + jobDescription + '\'' +
                ", telephonenumber='" + telephonenumber + '\'' +
                '}';
    }

    /*
    @OneToMany(mappedBy = "driverByDriverId", fetch = FetchType.EAGER)
    public Set<OperationShiftEntity> getOperationShiftsByDriverId() {
        return operationShiftsByDriverId;
    }

    public void setOperationShiftsByDriverId(Set<OperationShiftEntity> operationShiftsByDriverId) {
        this.operationShiftsByDriverId = operationShiftsByDriverId;
    }
     */

    @OneToMany(mappedBy = "driver", fetch = FetchType.EAGER)
    public Set<AbsenceEntity> getAbsenceEntities() {
        return absenceEntities;
    }

    public void setAbsenceEntities(Set<AbsenceEntity> absenceEntities) {
        this.absenceEntities = absenceEntities;
    }
}
