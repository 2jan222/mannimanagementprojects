package at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.controllers;

import at.fhv.ftb.inf.vz.semester4.groupc.web.application.ShiftRequest;
import at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.util.jsp_renderer.SwallowingJspRenderer;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.controllers.CustomResponseBody.ApplicationError.RENDER_ERROR;
import static at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.controllers.CustomResponseBody.ApplicationError.WISH_STATE_PROHIBITS_DELETION;
import static at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.controllers.CustomResponseBody.ApplicationInfo.SUCCESS;

@Controller
public class UserShiftWishController {
    private final SwallowingJspRenderer jspRenderer;
    private static final DateTimeFormatter yyyyMMdd = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final DateTimeFormatter ddMM = DateTimeFormatter.ofPattern("dd.MM");
    private ShiftRequest shiftRequestModel = new ShiftRequest();

    public UserShiftWishController(SwallowingJspRenderer jspRenderer) {
        this.jspRenderer = jspRenderer;
    }

    @GetMapping("/user/shiftWishForm")
    public String shiftWishes() {
        return "/user/shiftWishForm";
    }

    @GetMapping("/user/shiftWishForm/getShiftWeek")
    public String getShiftWeek(@NotNull Model model, @RequestParam(value = "startDate") String startDateString, @RequestParam(value = "week") String week) {
        try {
            LocalDate startDate = LocalDate.parse(startDateString, yyyyMMdd);
            model.addAttribute("year", startDate.getYear());
            model.addAttribute("weekNr", week);
            model.addAttribute("startDate", startDate);
            model.addAttribute("formatter", ddMM);
            model.addAttribute("formatterYYYYMMDD", yyyyMMdd);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            //TODO REDIRECT TO ERROR PAGE ? OR ERROR RESPONSE
        }
        return "/user/shiftWeek";
    }

    @RequestMapping(path = "/user/shiftWish/delete", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public CustomResponseBody deleteShiftRequest(@RequestParam(value = "id") String delete) {
        System.out.println("Shift to delete = " + delete);
        int parseInt = Integer.parseInt(delete);
        CustomResponseBody customResponseBody = new CustomResponseBody();
        boolean deleteSuccess = shiftRequestModel.deleteRequest(parseInt, MainRoutingController.userAssociatedDriver());
        if (deleteSuccess) {
            customResponseBody.addInformation(SUCCESS);
        } else {
            customResponseBody.addError(WISH_STATE_PROHIBITS_DELETION);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("driver", MainRoutingController.userAssociatedDriver());
        try {
            customResponseBody.setData(jspRender(map, "/user/shiftWishEnumeration"));
        } catch (Exception e) {
            e.printStackTrace();
            customResponseBody.addError(RENDER_ERROR);
        }
        return customResponseBody;
    }

    @GetMapping("/user/shiftWishForm/updateForWeek")
    @ResponseBody
    public String updateForWeek(@NotNull Model model, @RequestParam(value = "startDate") String startDateString, @RequestParam(value = "week") String week, @RequestParam(value = "shiftType") String shiftType, @NotNull @RequestParam(value = "isPos") String isPos, @RequestParam(value = "cause", required = false) String cause) {
        try {
            LocalDate date = LocalDate.parse(startDateString, yyyyMMdd);
            boolean isPositive = isPos.matches("positive");
            for (int i = 0; i < 7; i++) {
                shiftRequestModel.createWishForDate(date.plusDays(i), shiftType, isPositive, cause, MainRoutingController.userAssociatedDriver());
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return "Success";
    }

    @GetMapping("/user/shiftWishForm/updateForDay")
    @ResponseBody
    public String updateForWeek(@NotNull Model model, @RequestParam(value = "startDate") String startDateString, @RequestParam(value = "shiftType") String type, @NotNull @RequestParam(value = "isPos") String isPos, @RequestParam(value = "cause", required = false) String cause) {
        try {
            LocalDate date = LocalDate.parse(startDateString, yyyyMMdd);
            boolean isPositive = isPos.matches("positive");
            shiftRequestModel.createWishForDate(date, type, isPositive, cause, MainRoutingController.userAssociatedDriver());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return "Success";
    }

    private String jspRender(Map<String, Object> jspMap, String fsPath) throws Exception {
        Locale locale = Locale.getDefault();
        return jspRenderer.render(fsPath, jspMap, locale);
    }
}
