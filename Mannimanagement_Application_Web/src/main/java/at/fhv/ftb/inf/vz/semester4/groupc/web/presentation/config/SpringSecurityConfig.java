package at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.config;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.WebDriverEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.web.application.ApplicationUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.AccessDeniedHandler;

import java.util.List;

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AccessDeniedHandler accessDeniedHandler;


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "/home", "/about", "/favicon.ico", "resources/static/css/main.css", "resources/static/css/home.css" , "resources/static/css/**", "/particles.json").permitAll()
                .antMatchers("/admin/**").hasAnyRole("ADMIN")
                .antMatchers("/user/**").hasAnyRole("USER", "ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .and()
                .exceptionHandling().accessDeniedHandler(accessDeniedHandler);
    }

    // create two users, admin and user
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        List<WebDriverEntity> all = ApplicationUsers.getAllUsers();
        /*
        auth.inMemoryAuthentication()
                .withUser("user").password("{noop}password").roles("USER")
                .and()
                .withUser("admin").password("{noop}password").roles("ADMIN");
         */
        for (WebDriverEntity driver : all) {
            System.out.println(driver);
            auth.inMemoryAuthentication().withUser(driver.getUsername()).password(driver.getPasswordString()).roles(driver.getRole());
        }

    }
}
