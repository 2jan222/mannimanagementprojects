package at.fhv.ftb.inf.vz.semester4.groupc.web.application;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.WebDriverEntity;

import java.util.HashMap;

public class WebUserRepo {
    private static HashMap<String, WebDriverEntity> map = new HashMap<>();

    public static void add(WebDriverEntity driver) {
        map.put(driver.getUsername(), driver);
    }

    public static WebDriverEntity search(String username) {
        return map.get(username);
    }
}
