package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade.DatabaseEntityMarker;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Objects;
import java.util.Set;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Table(name = "path", schema = "public", catalog = "kmobil")
public class PathEntity implements DatabaseEntityMarker {
    private int pathId;
    private String pathDescription;
    private int routeId;
    private boolean isRetour = false;
    private RouteEntity routeByRouteId;
    private Set<PathStationEntity> pathStationsByPathId;
    private Set<StartTimeEntity> startTimesByPathId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "path_id")
    public int getPathId() {
        return pathId;
    }

    public void setPathId(int pathId) {
        this.pathId = pathId;
    }

    @Basic
    @Column(name = "path_description")
    public String getPathDescription() {
        return pathDescription;
    }

    public void setPathDescription(String pathDescription) {
        this.pathDescription = pathDescription;
    }

    @Basic
    @Column(name = "route_id")
    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PathEntity that = (PathEntity) o;
        return pathId == that.pathId &&
                routeId == that.routeId &&
                Objects.equals(pathDescription, that.pathDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pathId, pathDescription, routeId);
    }

    @ManyToOne
    @JoinColumn(name = "route_id", referencedColumnName = "route_id", nullable = false, insertable = false, updatable = false)
    public RouteEntity getRouteByRouteId() {
        return routeByRouteId;
    }

    public void setRouteByRouteId(RouteEntity routeByRouteId) {
        this.routeByRouteId = routeByRouteId;
    }

    @OneToMany(mappedBy = "pathByPathId", fetch = FetchType.EAGER)
    public Set<PathStationEntity> getPathStationsByPathId() {
        return pathStationsByPathId;
    }

    public void setPathStationsByPathId(Set<PathStationEntity> pathStationsByPathId) {
        this.pathStationsByPathId = pathStationsByPathId;
    }

    @OneToMany(mappedBy = "pathByPathId", fetch = FetchType.EAGER)
    public Set<StartTimeEntity> getStartTimesByPathId() {
        return startTimesByPathId;
    }

    public void setStartTimesByPathId(Set<StartTimeEntity> startTimesByPathId) {
        this.startTimesByPathId = startTimesByPathId;
    }

    @Override
    public String toString() {
        return "PathEntity{" +
                "pathId=" + pathId +
                ", pathDescription='" + pathDescription + '\'' +
                ", routeId=" + routeId +
                ", routeByRouteId=" + routeByRouteId +
                ", pathStationsByPathIdAmount=" + pathStationsByPathId.size() +
                ", startTimesByPathIdAmount=" + startTimesByPathId.size() +
                '}';
    }

    @Transient
    public boolean getRetour() {
        return isRetour;
    }

    public void setRetour(boolean retour) {
        isRetour = retour;
    }
}
