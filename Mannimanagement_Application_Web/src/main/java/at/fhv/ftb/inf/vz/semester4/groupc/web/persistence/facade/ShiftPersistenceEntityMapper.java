package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.ShiftEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class ShiftPersistenceEntityMapper extends PersistenceEntityMapper<ShiftEntity> {
    @Override
    public ShiftEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(ShiftEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("ShiftEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<ShiftEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<ShiftEntity> query = session.createQuery("from ShiftEntity ", ShiftEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<ShiftEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<ShiftEntity> query = session.createQuery("from ShiftEntity " + whereQueryFragment, ShiftEntity.class);
            return query.getResultList();
        }
    }
}
