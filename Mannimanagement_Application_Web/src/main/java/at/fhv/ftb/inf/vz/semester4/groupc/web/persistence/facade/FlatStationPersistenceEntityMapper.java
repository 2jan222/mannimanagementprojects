package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.FlatStationEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class FlatStationPersistenceEntityMapper extends PersistenceEntityMapper<FlatStationEntity> {

    @Override
    public FlatStationEntity read(Integer id) {
        return read(FlatStationEntity.class, id);
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("FlatStationEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<FlatStationEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<FlatStationEntity> query = session.createQuery("from FlatStationEntity ", FlatStationEntity.class);
            return query.getResultList();
        }
    }


    @Override
    public List<FlatStationEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<FlatStationEntity> query = session.createQuery("from FlatStationEntity " + whereQueryFragment, FlatStationEntity.class);
            return query.getResultList();
        }
    }
}
