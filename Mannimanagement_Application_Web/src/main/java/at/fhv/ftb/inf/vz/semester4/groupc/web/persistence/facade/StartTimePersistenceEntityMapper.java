package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.StartTimeEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class StartTimePersistenceEntityMapper extends PersistenceEntityMapper<StartTimeEntity> {
    @Override
    public StartTimeEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(StartTimeEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("StartTimeEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<StartTimeEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<StartTimeEntity> query = session.createQuery("from StartTimeEntity ", StartTimeEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<StartTimeEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<StartTimeEntity> query = session.createQuery("from StartTimeEntity " + whereQueryFragment, StartTimeEntity.class);
            return query.getResultList();
        }
    }
}
