package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.BusEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class BusPersistenceEntityMapper extends PersistenceEntityMapper<BusEntity> {
    @Override
    public BusEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(BusEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("BusEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<BusEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<BusEntity> query = session.createQuery("from BusEntity ", BusEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<BusEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<BusEntity> query = session.createQuery("from BusEntity " + whereQueryFragment, BusEntity.class);
            return query.getResultList();
        }
    }
}
