package at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.controllers;

import at.fhv.ftb.inf.vz.semester4.groupc.web.application.HolidayRequest;
import at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.util.jsp_renderer.SwallowingJspRenderer;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.controllers.CustomResponseBody.ApplicationError.*;
import static at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.controllers.CustomResponseBody.ApplicationInfo.SUCCESS;

@Controller
public class HolidayWishController {

    private final SwallowingJspRenderer jspRenderer;

    private static final DateTimeFormatter yyyyMMdd = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final DateTimeFormatter ddMM = DateTimeFormatter.ofPattern("dd.MM");

    private static final HolidayRequest holidayRequestModel = new HolidayRequest();

    public HolidayWishController(SwallowingJspRenderer jspRenderer) {
        this.jspRenderer = jspRenderer;
    }

    @GetMapping("/user/holidayWishForm")
    public String holidayWishes() {
        return "/user/holidayWishForm";
    }



    @RequestMapping(path = "/user/holidayWishForm/requestHoliday", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public CustomResponseBody req(@RequestParam(value = "start") String start, @RequestParam(value = "end") String end) {
        System.out.println("start = " + start);
        System.out.println("end = " + end);
        CustomResponseBody customResponseBody = new CustomResponseBody();
        customResponseBody.setData(getRequestHolidayResult(start, end, customResponseBody));
        if (customResponseBody.getErrors().isEmpty()) {
            customResponseBody.addInformation(SUCCESS);
        }
        return customResponseBody;
    }

    @RequestMapping(path = "/user/holidayWish/delete", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public CustomResponseBody deleteHolidayRequest(@RequestParam(value = "id") String delete) {
        System.out.println("delete = " + delete);
        int parseInt = Integer.parseInt(delete);
        CustomResponseBody customResponseBody = new CustomResponseBody();
        boolean deleteSuccess = holidayRequestModel.deleteRequest(parseInt, MainRoutingController.userAssociatedDriver());
        if (deleteSuccess) {
            customResponseBody.addInformation(SUCCESS);
        } else {
            customResponseBody.addError(WISH_STATE_PROHIBITS_DELETION);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("driver", MainRoutingController.userAssociatedDriver());
        try {
            customResponseBody.setData(jspRender(map, "/user/holidayWishEnumeration"));
        } catch (Exception e) {
            e.printStackTrace();
            customResponseBody.addError(RENDER_ERROR);
        }
        return customResponseBody;
    }

    @Nullable
    private String getRequestHolidayResult(String start, String end, CustomResponseBody customResponseBody) {
        try {
            HashMap<LocalDate, Boolean> map;
            try {
                LocalDate startDate = LocalDate.parse(start, yyyyMMdd);
                LocalDate endDate = LocalDate.parse(end, yyyyMMdd);
                map = holidayRequestModel.requestHoliday(MainRoutingController.userAssociatedDriver(), startDate, endDate, customResponseBody);
            } catch (DateTimeParseException e) {
                customResponseBody.addError(DATE_PARSE_EX);
                return null;
            }
            Map<String, Object> jspMap = new HashMap<>();
            jspMap.put("map", map);
            return jspRender(jspMap, "captures/driverDensity");
        } catch (Exception e) {
            e.printStackTrace();
            customResponseBody.addError(RENDER_ERROR);
            return null;
        }
    }

    private String jspRender(Map<String, Object> jspMap, String fsPath) throws Exception {
        Locale locale = Locale.getDefault();
        return jspRenderer.render(fsPath, jspMap, locale);
    }
}
