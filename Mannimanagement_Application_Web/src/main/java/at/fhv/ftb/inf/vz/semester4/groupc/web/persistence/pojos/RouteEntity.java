package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade.DatabaseEntityMarker;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Table(name = "route", schema = "public", catalog = "kmobil")
public class RouteEntity implements DatabaseEntityMarker {
    private int routeId;
    private int routeNumber;
    private LocalDate validFrom;
    private LocalDate validTo;
    private String variation;
    private Set<PathEntity> pathsByRouteId;
    private Set<RouteRideEntity> routeRidesByRouteId;

    public RouteEntity() {
    }

    public RouteEntity(int routeId, int routeNumber, LocalDate validFrom, LocalDate validTo, String variation, Set<PathEntity> pathsByRouteId, Set<RouteRideEntity> routeRidesByRouteId) {
        this.routeId = routeId;
        this.routeNumber = routeNumber;
        this.validFrom = validFrom;
        this.validTo = validTo;
        this.variation = variation;
        this.pathsByRouteId = pathsByRouteId;
        this.routeRidesByRouteId = routeRidesByRouteId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "route_id")
    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    @Basic
    @Column(name = "route_number")
    public int getRouteNumber() {
        return routeNumber;
    }

    public void setRouteNumber(int routeNumber) {
        this.routeNumber = routeNumber;
    }

    @Basic
    @Column(name = "valid_from")
    public LocalDate getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDate validFrom) {
        this.validFrom = validFrom;
    }

    @Basic
    @Column(name = "valid_to")
    public LocalDate getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDate validTo) {
        this.validTo = validTo;
    }

    @Basic
    @Column(name = "variation")
    public String getVariation() {
        return variation;
    }

    public void setVariation(String variation) {
        this.variation = variation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RouteEntity that = (RouteEntity) o;
        return routeId == that.routeId &&
                routeNumber == that.routeNumber &&
                Objects.equals(validFrom, that.validFrom) &&
                Objects.equals(validTo, that.validTo) &&
                Objects.equals(variation, that.variation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(routeId, routeNumber, validFrom, validTo, variation);
    }

    @OneToMany(mappedBy = "routeByRouteId", fetch = FetchType.EAGER)
    public Set<PathEntity> getPathsByRouteId() {
        return pathsByRouteId;
    }

    public void setPathsByRouteId(Set<PathEntity> pathsByRouteId) {
        this.pathsByRouteId = pathsByRouteId;
    }

    @OneToMany(mappedBy = "routeByRouteId", fetch = FetchType.EAGER)
    public Set<RouteRideEntity> getRouteRidesByRouteId() {
        return routeRidesByRouteId;
    }

    public void setRouteRidesByRouteId(Set<RouteRideEntity> routeRidesByRouteId) {
        this.routeRidesByRouteId = routeRidesByRouteId;
    }

    @Override
    public String toString() {
        return "RouteEntity{" +
                "routeId=" + routeId +
                ", routeNumber=" + routeNumber +
                ", validFrom=" + validFrom +
                ", validTo=" + validTo +
                ", variation='" + variation + '\'' +
                ", pathsByRouteIdAmount=" + pathsByRouteId.size() +
                ", routeRidesByRouteIdAmount=" + routeRidesByRouteId.size() +
                '}';
    }
}
