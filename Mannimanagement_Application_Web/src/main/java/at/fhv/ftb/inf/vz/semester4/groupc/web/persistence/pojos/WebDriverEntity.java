package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade.DatabaseEntityMarker;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "web_driver", schema = "public", catalog = "kmobil")

public class WebDriverEntity implements Serializable, DatabaseEntityMarker {

    private Integer webDriverId;
    private Integer driverId;
    private DriverEntity driver;
    private String username;
    private String passwordString;
    private String role;
    private Set<HolidayWishEntity> holidayWishes;
    private Set<ShiftWishEntity> shiftWishes;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "web_driver_id")
    public Integer getWebDriverId() {
        return webDriverId;
    }

    public void setWebDriverId(Integer webDriverId) {
        this.webDriverId = webDriverId;
    }

    @Basic
    @Column(name = "driver_id")
    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password_string")
    public String getPasswordString() {
        return passwordString;
    }

    public void setPasswordString(String passwordString) {
        this.passwordString = passwordString;
    }

    @Basic
    @Column(name = "role")
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "WebDriverEntity{" +
                "webDriverId=" + webDriverId +
                ", driverId=" + driverId +
                ", username='" + username + '\'' +
                ", passwordString='" + passwordString + '\'' +
                ", role='" + role + '\'' +
                '}';
    }

    @OneToOne
    @JoinColumn(name = "driver_id", referencedColumnName = "driver_id", insertable = false, updatable = false)
    public DriverEntity getDriver() {
        return driver;
    }

    public void setDriver(DriverEntity driver) {
        this.driver = driver;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "driver_id", referencedColumnName = "driver_id", insertable = false, updatable = false)
    public Set<HolidayWishEntity> getHolidayWishes() {
        return holidayWishes;
    }

    public void setHolidayWishes(Set<HolidayWishEntity> holidayWishes) {
        this.holidayWishes = holidayWishes;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "driver_id", referencedColumnName = "driver_id", insertable = false, updatable = false)
    public Set<ShiftWishEntity> getShiftWishes() {
        return shiftWishes;
    }

    public void setShiftWishes(Set<ShiftWishEntity> shiftWishes) {
        this.shiftWishes = shiftWishes;
    }
}
