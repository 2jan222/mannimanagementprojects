package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade.DatabaseEntityMarker;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalTime;
import java.util.Objects;
import java.util.Set;


@Entity
@Table(name = "start_time", schema = "public", catalog = "kmobil")
public class StartTimeEntity implements DatabaseEntityMarker {
    private Integer startTimeId;
    private Integer pathId;
    private Integer requiredCapacity;
    private LocalTime startTime;
    private Integer startTimeType;
    private Set<RouteRideEntity> routeRidesByStartTimeId;
    private PathEntity pathByPathId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "start_time_id")
    public Integer getStartTimeId() {
        return startTimeId;
    }

    public void setStartTimeId(Integer startTimeId) {
        this.startTimeId = startTimeId;
    }

    @Basic
    @Column(name = "path_id")
    public Integer getPathId() {
        return pathId;
    }

    public void setPathId(Integer pathId) {
        this.pathId = pathId;
    }

    @Basic
    @Column(name = "required_capacity")
    public Integer getRequiredCapacity() {
        return requiredCapacity;
    }

    public void setRequiredCapacity(Integer requiredCapacity) {
        this.requiredCapacity = requiredCapacity;
    }

    @Basic
    @Column(name = "start_time")
    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "start_time_type")
    public Integer getStartTimeType() {
        return startTimeType;
    }

    public void setStartTimeType(Integer startTimeType) {
        this.startTimeType = startTimeType;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StartTimeEntity that = (StartTimeEntity) o;
        return Objects.equals(startTimeId, that.startTimeId) &&
                Objects.equals(pathId, that.pathId) &&
                Objects.equals(requiredCapacity, that.requiredCapacity) &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(startTimeType, that.startTimeType) &&
                Objects.equals(routeRidesByStartTimeId, that.routeRidesByStartTimeId) &&
                Objects.equals(pathByPathId, that.pathByPathId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startTimeId, pathId, requiredCapacity, startTime, startTimeType, routeRidesByStartTimeId, pathByPathId);
    }

    @OneToMany(mappedBy = "startTimeByStartTimeId", fetch = FetchType.EAGER)
    public Set<RouteRideEntity> getRouteRidesByStartTimeId() {
        return routeRidesByStartTimeId;
    }

    public void setRouteRidesByStartTimeId(Set<RouteRideEntity> routeRidesByStartTimeId) {
        this.routeRidesByStartTimeId = routeRidesByStartTimeId;
    }

    @ManyToOne
    @JoinColumn(name = "path_id", referencedColumnName = "path_id", nullable = false, insertable = false, updatable = false)
    public PathEntity getPathByPathId() {
        return pathByPathId;
    }

    public void setPathByPathId(PathEntity pathByPathId) {
        this.pathByPathId = pathByPathId;
    }

    @Override
    public String toString() {
        return "StartTimeEntity{" +
                "startTimeId=" + startTimeId +
                ", pathId=" + pathId +
                ", requiredCapacity=" + requiredCapacity +
                ", startTime=" + startTime +
                ", startTimeType=" + startTimeType +
                ", routeRidesByStartTimeIdAmount=" + routeRidesByStartTimeId.size() +
                ", pathByPathId=" + pathByPathId +
                '}';
    }
}
