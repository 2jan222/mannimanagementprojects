package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade.DatabaseEntityMarker;
import org.jetbrains.annotations.Contract;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "holiday_wish", schema = "public", catalog = "kmobil")
public class HolidayWishEntity implements DatabaseEntityMarker {
    private Integer holidayWishId;
    private Integer driverId;
    private LocalDate startDate;
    private LocalDate endDate;
    private Integer stateOrdinal;

    @Contract(pure = true)
    public HolidayWishEntity() {
    }

    @Contract(pure = true)
    public HolidayWishEntity(Integer driverId, LocalDate startDate, LocalDate endDate) {
        this.driverId = driverId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.stateOrdinal = 0;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "holiday_wish_id")
    public Integer getHolidayWishId() {
        return holidayWishId;
    }

    public void setHolidayWishId(Integer holidayWishId) {
        this.holidayWishId = holidayWishId;
    }

    @Basic
    @Column(name = "driver_id")
    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    @Basic
    @Column(name = "start_date")
    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "end_date")
    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "state_ordinal")
    public Integer getStateOrdinal() {
        return stateOrdinal;
    }

    public void setStateOrdinal(Integer stateOrdinal) {
        this.stateOrdinal = stateOrdinal;
    }

    @Override
    public String toString() {
        return "HolidayWishEntity{" +
                "holidayWishId=" + holidayWishId +
                ", driverId=" + driverId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", stateOrdinal=" + stateOrdinal +
                '}';
    }
}
