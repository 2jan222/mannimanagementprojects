package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade.DatabaseEntityMarker;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "driving_licenses", schema = "public", catalog = "kmobil")
public class DrivingLicensesEntity implements DatabaseEntityMarker {
    private int driverId;
    private String drivingLicenses;

    @Id
    @Column(name = "driver_id")
    public int getDriverId() {
        return driverId;
    }

    public void setDriverId(int driverId) {
        this.driverId = driverId;
    }

    @Basic
    @Column(name = "driving_licenses")
    public String getDrivingLicenses() {
        return drivingLicenses;
    }

    public void setDrivingLicenses(String drivingLicenses) {
        this.drivingLicenses = drivingLicenses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DrivingLicensesEntity that = (DrivingLicensesEntity) o;
        return driverId == that.driverId &&
                Objects.equals(drivingLicenses, that.drivingLicenses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(driverId, drivingLicenses);
    }

    @Override
    public String toString() {
        return "DrivingLicensesEntity{" +
                "driverId=" + driverId +
                ", drivingLicenses='" + drivingLicenses + '\'' +
                '}';
    }
}
