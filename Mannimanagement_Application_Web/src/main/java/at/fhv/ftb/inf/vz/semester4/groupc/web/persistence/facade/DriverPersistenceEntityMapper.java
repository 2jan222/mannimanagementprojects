package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.DriverEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class DriverPersistenceEntityMapper extends PersistenceEntityMapper<DriverEntity> {
    @Override
    public DriverEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(DriverEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("DriverEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<DriverEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<DriverEntity> query = session.createQuery("from DriverEntity ", DriverEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<DriverEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<DriverEntity> query = session.createQuery("from DriverEntity " + whereQueryFragment, DriverEntity.class);
            return query.getResultList();
        }
    }
}
