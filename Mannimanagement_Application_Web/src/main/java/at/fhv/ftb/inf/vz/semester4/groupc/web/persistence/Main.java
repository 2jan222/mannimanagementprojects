package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.StationEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Default main. Auto generated.
 */
@Deprecated
public class Main {

    private static final SessionFactory ourSessionFactory;

    static {
        try {
            ourSessionFactory = new Configuration().
                    configure("hibernate.cfg.xml").
                    buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }


    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("manniPersUnit");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        StationEntity se = em.find(StationEntity.class, 217);
        em.remove(se);
        em.getTransaction().commit();
    }

}
