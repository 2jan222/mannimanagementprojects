package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade.DatabaseEntityMarker;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "station_connection")
public class StationConnectorEntity implements DatabaseEntityMarker {

    private int stationConnectorId;
    private int startStationId;
    private int endStationId;
    private Integer duration;
    private Integer distance;
    private StationEntity startStation;
    private StationEntity endStation;
    private PathStationEntity pathStationEntity;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "station_connection_id")
    public int getStationConnectorId() {
        return stationConnectorId;
    }

    public void setStationConnectorId(int stationConnectorId) {
        this.stationConnectorId = stationConnectorId;
    }


    @Column(name = "station_start")
    public Integer getStartStationId() {
        return startStationId;
    }

    public void setStartStationId(Integer startStationId) {
        this.startStationId = startStationId;
    }

    @Column(name = "station_end")
    public Integer getEndStationId() {
        return endStationId;
    }

    public void setEndStationId(Integer endStationId) {
        this.endStationId = endStationId;
    }

    @Column(name = "duration")
    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Column(name = "distance")
    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    @ManyToOne
    @JoinColumn(name = "station_start", referencedColumnName = "station_id", nullable = false, insertable = false, updatable = false)
    public StationEntity getStartStation() {
        return startStation;
    }

    public void setStartStation(StationEntity startStation) {
        this.startStation = startStation;
    }

    @ManyToOne
    @JoinColumn(name = "station_end", referencedColumnName = "station_id", nullable = false, insertable = false, updatable = false)
    public StationEntity getEndStation() {
        return endStation;
    }

    public void setEndStation(StationEntity endStation) {
        this.endStation = endStation;
    }

    @OneToOne(mappedBy = "stationConnector")
    public PathStationEntity getPathStationEntity() {
        return pathStationEntity;
    }

    public void setPathStationEntity(PathStationEntity pathStationEntity) {
        this.pathStationEntity = pathStationEntity;
    }

    @Override
    public String toString() {
        return "StationConnectorEntity{" +
                "stationConnectorId=" + stationConnectorId +
                ", startStationId=" + startStationId +
                ", endStationId=" + endStationId +
                ", duration=" + duration +
                ", distance=" + distance +
                ", startStation=" + startStation +
                ", endStation=" + endStation +
                //", pathStationEntity=" + pathStationEntity +
                '}';
    }
}
