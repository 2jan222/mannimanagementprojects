package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.StationEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class StationPersistenceEntityMapper extends PersistenceEntityMapper<StationEntity> {

    @Override
    public StationEntity read(Integer id) {

        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(StationEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("StationEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<StationEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<StationEntity> query = session.createQuery("from StationEntity ", StationEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<StationEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<StationEntity> query = session.createQuery("from StationEntity " + whereQueryFragment, StationEntity.class);
            return query.getResultList();
        }
    }
}
