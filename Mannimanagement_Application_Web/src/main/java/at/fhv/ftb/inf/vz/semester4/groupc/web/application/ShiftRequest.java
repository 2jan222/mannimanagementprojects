package at.fhv.ftb.inf.vz.semester4.groupc.web.application;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade.PersistenceFacade;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.ShiftWishEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.WebDriverEntity;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class ShiftRequest {
    public boolean deleteRequest(int id, @NotNull WebDriverEntity webDriverEntity) {
        ShiftWishEntity toDelete = webDriverEntity.getShiftWishes().stream().filter(e -> e.getShiftWishId() == id).collect(Collectors.toCollection(LinkedList::new)).getFirst();
        if (toDelete.getStateOrdinal() == 0) {
            PersistenceFacade.getInstance().delete(toDelete);
            webDriverEntity.getShiftWishes().remove(toDelete);
            return true;
        }
        return false;
    }

    public void createWishForDate(LocalDate date, String shiftType, boolean isPositive, String cause, @NotNull WebDriverEntity webDriverEntity) {
        ShiftWishEntity entity = new ShiftWishEntity();
        entity.setDate(date);
        entity.setDriverId(webDriverEntity.getDriverId());
        entity.setStateOrdinal(0);
        entity.setPositive(isPositive);
        if (!(cause == null || cause.length() < 1)) {
            entity.setNegativeCause(cause);
        }
        switch (shiftType.toLowerCase()) {
            case "early": entity.setShiftType(1); break;
            case "late": entity.setShiftType(2); break;
            default: entity.setShiftType(0); break;
        }
        PersistenceFacade.getInstance().update(entity);
        webDriverEntity.getShiftWishes().add(entity);
    }
}
