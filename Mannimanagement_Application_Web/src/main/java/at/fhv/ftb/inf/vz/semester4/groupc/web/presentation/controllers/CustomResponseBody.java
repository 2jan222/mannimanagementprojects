package at.fhv.ftb.inf.vz.semester4.groupc.web.presentation.controllers;

import org.jetbrains.annotations.Contract;

import java.util.LinkedList;

public class CustomResponseBody {
    private LinkedList<ApplicationError> errors = new LinkedList<>();
    private LinkedList<ApplicationWarning> warnings = new LinkedList<>();
    private LinkedList<ApplicationInfo> information = new LinkedList<>();
    private Object data;


    @Contract(pure = true)
    public CustomResponseBody(Object data) {
        this.data = data;
    }

    public CustomResponseBody() {
        this.data = null;
    }

    public LinkedList<ApplicationError> getErrors() {
        return errors;
    }

    public LinkedList<ApplicationWarning> getWarnings() {
        return warnings;
    }

    public LinkedList<ApplicationInfo> getInformation() {
        return information;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public CustomResponseBody addError(ApplicationError error) {
        this.errors.add(error);
        return this;
    }

    public CustomResponseBody addWarning(ApplicationWarning warning) {
        this.warnings.add(warning);
        return this;
    }

    public CustomResponseBody addInformation(ApplicationInfo information) {
        this.information.add(information);
        return this;
    }

    public enum ApplicationError {
        DRIVER_NOT_FOUND,
        DATABASE_NO_CONNECTION,
        DATE_PARSE_EX,
        DATES_ORDER_REVERSED,
        DATE_OF_PAST_DAY,
        RENDER_ERROR, WISH_STATE_PROHIBITS_DELETION;
    }

    public enum ApplicationWarning {
        NOT_ENOUGH_PERSONAL, HOLIDAY_REQUEST_OVERLAPS_WITH_EXISTING_REQUEST
    }

    public enum ApplicationInfo
    {
        SUCCESS
    }
}
