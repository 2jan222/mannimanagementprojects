package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.HolidayWishEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class HolidayWishEntityMapper extends PersistenceEntityMapper<HolidayWishEntity> {
    @Override
    public HolidayWishEntity read(Integer id) {
        return read(HolidayWishEntity.class, id);
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("HolidayWishEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<HolidayWishEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<HolidayWishEntity> query = session.createQuery("from HolidayWishEntity ", HolidayWishEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<HolidayWishEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<HolidayWishEntity> query = session.createQuery("from HolidayWishEntity " + whereQueryFragment, HolidayWishEntity.class);
            return query.getResultList();
        }
    }
}
