package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade;

import java.util.List;

public interface DBMapper<T> {

    void create(DatabaseEntityMarker value);

    T read(Integer id);

    void update(DatabaseEntityMarker value);

    void update(String setQueryFragment, String whereQueryFragment);

    void delete(DatabaseEntityMarker value);

    List<T> getAll();

    List<T> getAllWhere(String whereQueryFragment);

}
