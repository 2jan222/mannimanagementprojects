package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos.StationConnectorEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class StationConnectorPersistenceEntityMapper extends PersistenceEntityMapper<StationConnectorEntity> {
    @Override
    public StationConnectorEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return sess.get(StationConnectorEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        updateHelper("StationConnectorEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<StationConnectorEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<StationConnectorEntity> query = session.createQuery("from StationConnectorEntity ", StationConnectorEntity.class);
            return query.getResultList();
        }
    }

    @Override
    public List<StationConnectorEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query<StationConnectorEntity> query = session.createQuery("from StationConnectorEntity " + whereQueryFragment, StationConnectorEntity.class);
            return query.getResultList();
        }
    }
}
