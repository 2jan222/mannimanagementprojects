package at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.web.persistence.facade.DatabaseEntityMarker;
import org.jetbrains.annotations.TestOnly;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "bus", schema = "public", catalog = "kmobil")
public class BusEntity implements DatabaseEntityMarker {
    private int busId;
    private int maintenanceKm;
    private String licenceNumber;
    private String make;
    private String model;
    private String note;
    private LocalDate registrationDate;
    private int seatPlaces;
    private int standPlaces;
    private Set<RouteRideEntity> routeRidesByBusId;
    private Set<SuspendedEntity> suspendedsByBusId;
    private Set<OperationEntity> operationsByBusId;

    public BusEntity() {
    }

    @TestOnly
    public BusEntity(int busId, int maintenanceKm, String licenceNumber, String make, String model, String note, LocalDate registrationDate, int seatPlaces, int standPlaces, Set<RouteRideEntity> routeRidesByBusId, Set<SuspendedEntity> suspendedsByBusId, Set<OperationEntity> operationsByBusId) {
        this.busId = busId;
        this.maintenanceKm = maintenanceKm;
        this.licenceNumber = licenceNumber;
        this.make = make;
        this.model = model;
        this.note = note;
        this.registrationDate = registrationDate;
        this.seatPlaces = seatPlaces;
        this.standPlaces = standPlaces;
        this.routeRidesByBusId = routeRidesByBusId;
        this.suspendedsByBusId = suspendedsByBusId;
        this.operationsByBusId = operationsByBusId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bus_id")
    public int getBusId() {
        return busId;
    }

    public void setBusId(int busId) {
        this.busId = busId;
    }

    @Basic
    @Column(name = "maintenance_km")
    public int getMaintenanceKm() {
        return maintenanceKm;
    }

    public void setMaintenanceKm(int maintenanceKm) {
        this.maintenanceKm = maintenanceKm;
    }

    @Basic
    @Column(name = "licence_number")
    public String getLicenceNumber() {
        return licenceNumber;
    }

    public void setLicenceNumber(String licenceNumber) {
        this.licenceNumber = licenceNumber;
    }

    @Basic
    @Column(name = "make")
    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    @Basic
    @Column(name = "application")
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Basic
    @Column(name = "note")
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Basic
    @Column(name = "registration_date")
    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Basic
    @Column(name = "seat_places")
    public int getSeatPlaces() {
        return seatPlaces;
    }

    public void setSeatPlaces(int seatPlaces) {
        this.seatPlaces = seatPlaces;
    }

    @Basic
    @Column(name = "stand_places")
    public int getStandPlaces() {
        return standPlaces;
    }

    public void setStandPlaces(int standPlaces) {
        this.standPlaces = standPlaces;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BusEntity busEntity = (BusEntity) o;
        return busId == busEntity.busId &&
                maintenanceKm == busEntity.maintenanceKm &&
                seatPlaces == busEntity.seatPlaces &&
                standPlaces == busEntity.standPlaces &&
                Objects.equals(licenceNumber, busEntity.licenceNumber) &&
                Objects.equals(make, busEntity.make) &&
                Objects.equals(model, busEntity.model) &&
                Objects.equals(note, busEntity.note) &&
                Objects.equals(registrationDate, busEntity.registrationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(busId, maintenanceKm, licenceNumber, make, model, note, registrationDate, seatPlaces, standPlaces);
    }

    @OneToMany(mappedBy = "busByBusId", fetch = FetchType.EAGER)
    public Set<RouteRideEntity> getRouteRidesByBusId() {
        return routeRidesByBusId;
    }

    public void setRouteRidesByBusId(Set<RouteRideEntity> routeRidesByBusId) {
        this.routeRidesByBusId = routeRidesByBusId;
    }

    @OneToMany(mappedBy = "busByBusId", fetch = FetchType.EAGER)
    public Set<SuspendedEntity> getSuspendedsByBusId() {
        return suspendedsByBusId;
    }

    public void setSuspendedsByBusId(Set<SuspendedEntity> suspendedsByBusId) {
        this.suspendedsByBusId = suspendedsByBusId;
    }

    @Override
    public String toString() {
        return "BusEntity{" +
                "busId=" + busId +
                ", maintenanceKm=" + maintenanceKm +
                ", licenceNumber='" + licenceNumber + '\'' +
                ", make='" + make + '\'' +
                ", application='" + model + '\'' +
                ", note='" + note + '\'' +
                ", registrationDate=" + registrationDate +
                ", seatPlaces=" + seatPlaces +
                ", standPlaces=" + standPlaces +
                ", routeRidesByBusIdAmount=" + routeRidesByBusId.size() +
                ", suspendedsByBusIdAmount=" + suspendedsByBusId.size() +
                '}';
    }

    @OneToMany(mappedBy = "busByBusId", fetch = FetchType.EAGER)
    public Set<OperationEntity> getOperationsByBusId() {
        return operationsByBusId;
    }

    public void setOperationsByBusId(Set<OperationEntity> operationsByBusId) {
        this.operationsByBusId = operationsByBusId;
    }
}
